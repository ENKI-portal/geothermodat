"""
This module handles input/ouput for thermodynamic databases.

"""
import json
import numpy as np
import pandas as pd
from os import path
import os
import pathlib
import matplotlib.pyplot as plt
from IPython.display import display
import re
import copy

import openpyxl
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment

import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from collections import OrderedDict

__all__ = ['extract_table_data', 'extract_unstructured_data',
           'read_json_data', 'read_xls_data', 'json_to_xls', 'xls_to_json',
           'APP_DATA_DIR']


APP_DATA_DIR = '_app_data/'

OXIDE_ORDER = [
    'SiO2','TiO2','Al2O3','Fe2O3','Cr2O3','FeO','MnO','MgO','NiO',
    'CoO','CaO','Na2O','K2O','P2O5','H2O','CO2']
PERIODIC_ORDER = [
    'H', 'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na',
    'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V',
    'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se',
    'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh',
    'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La',
    'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm',
    'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl',
    'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U' ]

class DataUpdate:
    _TEMPLATE_FILENAME='_app_data/phase-rxn-data-base-template.xlsx'
    _RESULT_OPTS = ['+', '-', '=', '+?', '-?']

    def __init__(self, orig_filename, data_dir_name ='ENKI-data',
                 use_home_dir=True, update_dir_name='update-data',
                 ignore_empty_rxn_phases=True):

        self._init_data_dirs(data_dir_name, update_dir_name, use_home_dir)
        self._load_orig_file(orig_filename)
        self._init_update_file()

    def _init_data_dirs(self, data_dir_name, update_dir_name, use_home_dir):
        if use_home_dir:
            home_dir = str(pathlib.Path.home())+'/'
        else:
            home_dir = ''

        input_dir = os.path.join(home_dir, data_dir_name)
        output_dir = os.path.join(home_dir, data_dir_name, update_dir_name)

        if not os.path.exists(input_dir):
            os.makedirs(input_dir)

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        self._input_dir_name = input_dir
        self._output_dir_name = output_dir

    @property
    def data_update(self):
        return self._data_update

    @property
    def data_orig(self):
        return self._data_orig

    @property
    def wb_update(self):
        return self._wb_update

    def update(self, validate=True, write_file=True):
        data_orig = self._data_orig
        data_update = self._data_update
        self._data_update = self._copy_orig_data(data_orig, data_update)

        if validate:
            self.validate()


        if write_file:
            self.write_update()

    def validate(self):
        data_update = self._data_update

        rxn = data_update['rxn']
        results = rxn['results'].copy()

        RESULT_OPTS = self._RESULT_OPTS

        for exp_ind, iresult in enumerate(results[1:]):
            iresult_list = [istr.replace(' ','') for istr
                            in iresult.split(';')]

            obs_not_valid = False

            for ind_result, iobs in enumerate(iresult_list):
                opt_found = any([iobs.endswith(opt) for opt in RESULT_OPTS])
                if not opt_found:
                    obs_not_valid = True
                    # opt_begin = any([iobs.startswith(opt) for opt in RESULT_OPTS])

                # iresult_list[ind_result] = '???'

            if obs_not_valid:
                iresult_list.insert(0, '???')

            iresult_valid = '; '.join(iresult_list)
            results[exp_ind+1] = iresult_valid

            # print(iresult_valid)
            # print('----')

        data_update['rxn']['results'] = results
        self._data_update = data_update

    def write_update(self):
        update_filename = self._update_filename
        data_update = self._data_update

        wb_update = openpyxl.load_workbook(update_filename)
        wb_update = self._update_workbook(data_update, wb_update)

        wb_update.save(update_filename)
        self._wb_update = wb_update

    def _load_orig_file(self, orig_filename):
        input_dir = self._input_dir_name
        output_dir = self._output_dir_name

        orig_file_path = os.path.join(input_dir, orig_filename)

        data_orig = pd.read_excel(orig_file_path, sheet_name=None)
        basename_orig = path.basename(orig_file_path)
        update_filename = path.join(
            output_dir, path.splitext(basename_orig)[0]+'-update.xlsx')

        self._data_orig = data_orig
        self._update_filename = update_filename

    def _init_update_file(self):
        data_orig = self._data_orig
        update_filename = self._update_filename

        rxn_phases = self._get_rxn_phases(data_orig)
        components = self._get_chem_components(data_orig)
        update_template = self._write_data_template(components, rxn_phases, update_filename)
        data_update = self._init_data_update(data_orig, update_template)

        self._data_update = data_update

    # Determine chem_components
    def _get_chem_components(self, data_orig):
        chem_cols = data_orig['starter_phase'].columns
        components = []
        ind=1
        while True:
            icomponent = chem_cols[-4*ind]
            if chem_cols[-4*ind+1].startswith(icomponent):
                components.append(icomponent)

            else:
                break

            ind = ind+1

        components = components[::-1]
        return components

    def _get_rxn_phases(self, data_orig, ignore_empty=True):
        rxn_phases = []
        sheets = list(data_orig.keys())
        ind=1
        while True:
            isheet = sheets[-ind]
            if isheet == 'rxn':
                break
            else:
                rxn_phases.append(isheet)

            ind += 1

        rxn_phases = rxn_phases[::-1]

        if ignore_empty:
            all_rxn_phases = rxn_phases
            rxn_phases = []
            for isheet_name in rxn_phases:
                isheet = data_orig[isheet_name]
                idata_rows = isheet.shape[0] - 1
                if idata_rows > 0:
                    rxn_phases.append(isheet_name)

        return rxn_phases

    def _write_data_template(self, components, rxn_phases, update_filename):
        std_sheets, chem_sheet, units_lookup, rxn_phase_sheet = (
            read_rxn_base_template(self._TEMPLATE_FILENAME))

        data_info = extract_data_info(
            components, rxn_phases,
            std_sheets, chem_sheet, units_lookup, rxn_phase_sheet)

        wb = write_excel_workbook(update_filename, data_info)
        wb.close()

        update_template = pd.read_excel(update_filename, sheet_name=None)
        return update_template

    def _init_data_update(self, data_orig, update_template):
        data_update = OrderedDict()
        for isheet_name in update_template.keys():
            if isheet_name in data_orig.keys():
                # print(isheet_name, ' present')
                isheet_orig = data_orig[isheet_name]
                isheet_update = update_template[isheet_name]
                if isheet_name in ['reference', 'notes', 'phase_symbols']:
                    iNdata_row = isheet_orig.shape[0]
                else:
                    iNdata_row = isheet_orig.shape[0]-1

                iNdata_col = isheet_update.shape[1]
                empty_data_rows = pd.DataFrame(
                    np.nan*np.ones((iNdata_row, iNdata_col)),
                    columns=isheet_update.columns)
                isheet_update = isheet_update.append(empty_data_rows, ignore_index=True)
                data_update[isheet_name] = isheet_update

            elif isheet_name=='exp_details':
                # print(isheet_name, ' absent')
                isheet_name_orig = 'exp_conditions'
                isheet_name_update = 'exp_details'

                isheet_orig = data_orig[isheet_name_orig]
                isheet_update = update_template[isheet_name_update]
                iNdata_row = isheet_orig.shape[0]-1
                iNdata_col = isheet_update.shape[1]

                empty_data_rows = pd.DataFrame(
                    np.nan*np.ones((iNdata_row, iNdata_col)),
                    columns=isheet_update.columns)
                isheet_update = isheet_update.append(empty_data_rows, ignore_index=True)
                data_update[isheet_name_update] = isheet_update

        return data_update

    def _copy_orig_data(self, data_orig, data_update):
        for isheet_name in data_update:
            isheet_update = data_update[isheet_name]

            orig_sheet_names = []
            orig_sheet_names.append(isheet_name)

            if isheet_name=='exp_details':
                orig_sheet_names.append('exp_conditions')

            elif isheet_name=='sample_material':
                orig_sheet_names.append('rxn')

            # print(orig_sheet_names)

            if isheet_name in ['reference', 'notes', 'phase_symbols']:
                header_lines = 1
            else:
                header_lines = 2

            col_range = range(isheet_update.shape[1])

            for icol_name in isheet_update.columns:
                isheet_update.loc[header_lines-1:, icol_name] = '???'
                # isheet_update[icol_name][header_lines-1:] = '???'

                for orig_sheet_name in orig_sheet_names:
                    try:
                        isheet_orig = data_orig[orig_sheet_name]
                        if icol_name in isheet_orig:
                            isheet_update.loc[:, icol_name] = isheet_orig[icol_name]
                            break
                    except:
                        # sheet not present in original
                        continue

            data_update[isheet_name] = isheet_update

        return data_update

    def _update_workbook(self, data, wb):
        all_col_widths = self._get_col_widths(data)

        for isheet_name in data:
            isheet, iws = data[isheet_name], wb[isheet_name]
            icol_widths = all_col_widths[isheet_name]

            iws = self._fix_data_validations(iws)
            iws = self._adjust_comments(iws)
            iws = self._paste_wb_values(isheet_name, isheet, iws)
            iws = self._adjust_column_format(isheet_name, isheet, iws,
                                             icol_widths)

        return wb

    def _adjust_comments(self, ws, width=400, height=500):
        for col in ws.iter_cols():
            cell = col[0]
            comment = cell.comment
            if comment is not None:
                comment.height = height
                comment.width = width

        return ws

    def _get_col_widths(self, data, shift=1):
        col_widths = OrderedDict()
        for sheet_name in data:
            sheet = data[sheet_name]
            col_width = pd.Series()
            for col_name in sheet:
                entry_max_len = sheet[col_name].astype(str).map(len).max()
                title_len = len(col_name)
                if col_name=='notes':
                    max_chars = 20
                else:
                    max_chars = max(entry_max_len, title_len)

                col_width[col_name] = max_chars + shift

            col_widths[sheet_name] = col_width

        return col_widths

    def _get_header_rows(self, sheet_name):
        title_rows = 1
        unit_rows = 1
        if sheet_name in ['reference', 'notes', 'phase_symbols']:
            unit_rows = 0

        header_rows = title_rows+unit_rows
        return header_rows, title_rows

    def _paste_wb_values(self, sheet_name, sheet, ws):
        header_rows, title_rows = self._get_header_rows(sheet_name)
        # row_range = range(sheet.shape[0])[header_rows:]
        row_range = range(sheet.shape[0])
        col_range = range(sheet.shape[1])
        for irow in row_range:
            for icol in col_range:
                ws.cell(row=irow+title_rows+1,
                         column=icol+1).value = sheet.iloc[irow,icol]

        return ws

    def _fix_data_validations(self, ws):
        dv_list = ws.data_validations.dataValidation
        for idv in dv_list:
            idv.showErrorMessage = False

        return ws

    def _adjust_column_format(self, sheet_name, sheet, ws, col_widths):
        header_rows, title_rows = self._get_header_rows(sheet_name)
        # row_range = range(sheet.shape[0])[header_rows:]

        row_range = range(sheet.shape[0])
        for ind, icol_name in enumerate(sheet):
            if icol_name == 'notes':
                # print(sheet_name, ', ', icol_name)
                for irow in row_range:
                    # print(irow+header_rows+1, ind+1)
                    icell = ws.cell(row=irow+header_rows+1, column=ind+1)
                    icell.alignment = Alignment(wrapText=True)

            icol_num = col_widths.iloc[ind]
            col_id = openpyxl.utils.get_column_letter(ind+1)
            ws.column_dimensions[col_id].width = icol_num

        return ws

class RxnObs:
    _RXN_DIR_OPTS = ('FWD','FWD?','REV','REV?','NC','VOID','NONE')
    _OBS_OPTS = ['-','-?','=','+?','+']
    _HASH_CONST = 1.234567e-6

    def __init__(self, rxn_str, result_str=None, rxn_bnd_dir_lookup=None):
        rxn_orig = rxn_str
        result_orig = result_str

        phases_orig, coefs_orig = self._parse_rxn(rxn_orig)

        rxn_std, phases_std, coefs_std = self._standardize_rxn(
            rxn_orig, phases_orig, coefs_orig)

        rxn_dir, other_phase_obs, rxn_phase_obs_matrix = self._parse_result_str(
            result_str, phases_std, coefs_std)

        high_T_dir, high_P_dir = self._init_rxn_bnd_dir(rxn_std, rxn_bnd_dir_lookup)

        self._rxn = rxn_std
        self._rxn_dir = rxn_dir

        self._high_T_dir = high_T_dir
        self._high_P_dir = high_P_dir

        self._rxn_orig = rxn_orig
        self._result_orig = result_str

        self._phases = phases_std
        self._coefs = coefs_std
        self._other_phases = other_phase_obs
        self._rxn_phase_obs_matrix = rxn_phase_obs_matrix
        self._result_str = rxn_phase_obs_matrix

    @property
    def rxn(self):
        return self._rxn

    @property
    def rxn_dir(self):
        return self._rxn_dir

    @property
    def phases(self):
        return self._phases

    @property
    def coefs(self):
        return self._coefs

    @property
    def other_phases(self):
        return self._other_phases

    @property
    def rxn_phase_obs_matrix(self):
        return self._rxn_phase_obs_matrix

    @property
    def rxn_orig(self):
        return self._rxn_orig

    @property
    def result_orig(self):
        return self._result_orig

    @property
    def high_T_dir(self):
        return self._high_T_dir

    @property
    def high_P_dir(self):
        return self._high_P_dir

    def __repr__(self):
        return f"RxnObs('{self.rxn}', result_str='{self.rxn_dir}')"

    def __hash__(self):
        phases_hash = hash(tuple(self.phases))
        coefs_sign_hash = hash(tuple(np.sign(self.coefs + self._HASH_CONST)))
        rxn_dir_hash = hash(self.rxn_dir)
        return hash((phases_hash, coefs_sign_hash, rxn_dir_hash))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __lt__(self, other):
        return hash(self) < hash(other)

    def __gt__(self, other):
        return hash(self) > hash(other)


        rxn_bnd_dir = self._init_rxn_bnd_dir(rxn_std, rxn_bnd_dir_lookup)

    def _init_rxn_bnd_dir(self, rxn_std, rxn_bnd_dir_lookup):

        # def _parse_bnd_dir(dir_sign):
        #     if dir_sign > 0:
        #         return 'FWD'
        #     elif dir_sign < 0:
        #         return 'REV'
        #     else:
        #         return None

        if rxn_bnd_dir_lookup is None:
            raise NotImplementedError('Rxn_bnd_dir_lookup must be provided. '
                                      'Auto detection not yet implemented')

        try:
            rxn_bnd_dir = rxn_bnd_dir_lookup.loc[rxn_std]
            high_T_dir = np.sign(rxn_bnd_dir['high_T_dir'])
            high_P_dir = np.sign(rxn_bnd_dir['high_P_dir'])

        except:
            raise NotImplementedError('Rxn_bnd_dir_lookup must provide bnd dir for this rxn. '
                                      'Auto detection not yet implemented')

        return high_T_dir, high_P_dir

    def _parse_result_str(self, result_str, phases_orig, coefs_orig):

        rxn_phase_num = len(phases_orig)
        coefs_sign = np.sign(coefs_orig)

        rxn_dir = None
        other_phase_obs = {}
        rxn_phase_obs_matrix = np.zeros((rxn_phase_num, 5))

        if result_str is None:
            result_str = 'NONE'

        if result_str in self._RXN_DIR_OPTS:
            rxn_dir = result_str
            phase_results = []

        else:
            phase_results = [iresult.strip() for iresult in result_str.split(';')]
            for iresult in phase_results:
                iphs, iobs = self._parse_phase_obs(iresult)
                if iphs in phases_orig:
                    # rxn_phase_obs_matrix accounts for product vs reactant
                    isign = coefs_sign[np.array(phases_orig)==iphs]
                    ind_obs_0 = np.where([iobs==opt for opt in self._OBS_OPTS])[0]
                    ind_obs = int(isign*(ind_obs_0 - 2) + 2)
                    ind_phs = np.where([iphs==phs for phs in phases_orig])[0]

                    # print(iphs)
                    # print( ind_phs)
                    # print( ind_obs)
                    # print('---')
                    rxn_phase_obs_matrix[ind_phs, ind_obs] = 1

                else:
                    other_phase_obs[iphs] = iobs

            rxn_obs_tot = np.sum(rxn_phase_obs_matrix, axis=0)
            rxn_dir = self._eval_rxn_dir(rxn_obs_tot)

        return rxn_dir, other_phase_obs, rxn_phase_obs_matrix


    def _parse_phase_obs(self, result):
        matchstr = '[=+-]\??'
        match = re.search(matchstr, result)
        obs = match.group()
        tokens = re.split(matchstr, result)
        phs = tokens[0] if len(tokens[0])>0 else tokens[1]

        phs = phs.strip()
        obs = obs.strip()

        return phs, obs


    def _eval_rxn_dir(self, rxn_obs):
        RXN_DIR_OPTS = np.array([
            [  'NC', 'REV?', 'REV' ],
            ['FWD?',   'NC', 'REV?'],
            [ 'FWD', 'FWD?', 'VOID'],
        ])

        if rxn_obs[-1]>0:
            fwd_evidence = 2
        elif rxn_obs[-2]>0:
            fwd_evidence = 1
        else:
            fwd_evidence = 0

        if rxn_obs[0]>0:
            rev_evidence = 2
        elif rxn_obs[1]>0:
            rev_evidence = 1
        else:
            rev_evidence = 0

        rxn_dir = RXN_DIR_OPTS[fwd_evidence, rev_evidence]
        return rxn_dir




    def _standardize_rxn(self, rxn, phases, coefs):
        rxn_std = rxn
        # phases_std = phases
        # coefs_std = coefs

        rxn_phases = [(iphs, icoef) for iphs, icoef in zip(phases, coefs)]

        # Enforce alphabetical order (by phase symbol) for rxn direction
        first_phs_sign = np.sign(sorted(rxn_phases)[0][1])
        if first_phs_sign > 0:
            coefs = -1*coefs
            rxn_phases = [(iphs, icoef) for iphs, icoef in zip(phases, coefs)]

        # Group phases by reactants/products and sort alphabetical by symbol
        rxn_phases_sorted = sorted(rxn_phases, key=lambda x:(np.sign(x[1]), x[0]))

        phases_std = []
        coefs_std = []
        rxn_std = ''
        prod_found = False

        for ind, (iphs, icoef) in enumerate(rxn_phases_sorted):
            phases_std.append(iphs)
            coefs_std.append(icoef)

            if icoef>0 and not prod_found:
                prod_found = True
                rxn_std = rxn_std + ' ='
            else:
                if ind > 0:
                    rxn_std = rxn_std + ' +'

            icoef_abs = np.abs(icoef)
            if icoef_abs == int(icoef_abs):
                icoef_abs = int(icoef_abs)

            rxn_std = rxn_std + ' ' + str(icoef_abs) + iphs


        rxn_std = rxn_std.strip()

        coefs_std = np.array(coefs_std)

        # for icoef in coefs_std:




        return rxn_std, phases_std, coefs_std

    def _parse_rxn(self, rxn):
        rxn_combo0, rxn_combo1 = rxn.split('=')

        phases0, coefs0  = self._parse_rxn_combo(rxn_combo0)
        phases1, coefs1  = self._parse_rxn_combo(rxn_combo1)

        phases = phases0
        phases.extend(phases1)
        coefs = np.hstack((-np.array(coefs0), np.array(coefs1)))

        return phases, coefs



    def _parse_rxn_combo(self, rxn_combo):
        terms = [iphs.strip() for iphs in rxn_combo.split('+')]

        phases = []
        coefs = []

        for term in terms:
            phs, coef  = self._parse_phase_term(term)
            phases.append(phs)
            coefs.append(coef)

        return phases, coefs

    def _parse_phase_term(self, term):
        imatch = re.match(r'\d+', term)
        if imatch:
            coef = float(imatch.group())
            ipos_start = imatch.span()[-1]
            phs = term[ipos_start:].strip()
        else:
            coef = 1.0
            phs = term.strip()

        return phs, coef

    def _parse_rxn_bias_dir(self, rxn_bias_dir_str):

        # -1 rev bias, +1 fwd bias, 0 no bias
        # NaN bias present but unknown
        if rxn_bias_dir_str=='unknown':
            return np.nan

        if rxn_bias_dir_str in (np.nan, None, ''):
            return 0

        bias_phs = rxn_bias_dir_str[0:-1]
        phs_bias_dir = rxn_bias_dir_str[-1]

        phs_coef_sign = self.coefs[np.array(self.phases)==bias_phs]

        if phs_bias_dir=='+':
            rxn_bias = phs_coef_sign
        elif phs_bias_dir=='-':
            rxn_bias = -phs_coef_sign
        else:
            print(bias_phs)
            print(phs_bias_dir)
            assert False, 'That is not valid.'

        return rxn_bias

    def _parse_cond_bias_dir(self, cond_bias_dir_str):
        if cond_bias_dir_str in (np.nan, None, ''):
            return 0
        if cond_bias_dir_str == 'unknown':
            return np.nan

        cond = cond_bias_dir_str[0:-1]
        bias_dir = cond_bias_dir_str[-1]

        if cond=='P':
            high_dir = self.high_P_dir
        elif cond=='T':
            high_dir = self.high_T_dir
        else:
            high_dir = 0

        if bias_dir == '+':
            rxn_bias = -high_dir
        elif bias_dir == '-':
            rxn_bias = +high_dir
        else:
            print(cond)
            print(bias_dir)
            assert False, 'That is not valid.'

        return rxn_bias

    def net_rxn_bias(self, rxn_bias_dir_str, cond_bias_dir_str):
        ind_rxn_bias = self._parse_rxn_bias_dir(rxn_bias_dir_str)+1
        ind_cond_bias = self._parse_cond_bias_dir(cond_bias_dir_str)+1

        def add_count(row, ind_bias, bias_counts):
            if np.isnan(ind_bias):
                bias_counts[row] = np.nan
            else:
                ind = int(ind_bias)
                bias_counts[row, ind] = 1

            return

        bias_counts = np.zeros((2,3))
        add_count(0, ind_rxn_bias, bias_counts)
        add_count(1, ind_cond_bias, bias_counts)

        bias_tot = np.sum(bias_counts, axis=0)

        if (bias_tot[0]>0) and (bias_tot[2]>0):
            return 'ALL'
        elif bias_tot[0]>0:
            return 'REV'
        elif bias_tot[2]>0:
            return 'FWD'
        else:
            return 'NONE'

# extract primary raw data

def load_rxn_data(filename, data_dir_name='ENKI-data'):
    home_dir = str(pathlib.Path.home())+'/'
    data_dir_path = path.join(home_dir, data_dir_name)

    file_path = path.join(data_dir_path, filename)
    raw_data = pd.read_excel(file_path, sheet_name=None)

    return raw_data

def parse_rxn_data(data):
    data_summary = pd.DataFrame()

    Ndat = data['exp_conditions'].shape[0]
    pub_id = data['reference']['pub_id'].values[0]
    pub_id_series = pd.Series(np.tile(pub_id, Ndat))
    pub_id_series.drop(index=0, inplace=True)
    data_summary['pub_id'] = pub_id_series

    data_summary['index'] = data['exp_conditions'].loc[1:, 'index'].astype(int)
    data_summary['run_id'] = data['exp_conditions'].loc[1:, 'run_id'].astype(str)

    P_units = data['exp_conditions'].loc[0, 'P']
    T_units = data['exp_conditions'].loc[0, 'T']
    P = data['exp_conditions'].loc[1:, 'P']
    T = data['exp_conditions'].loc[1:, 'T']
    P_err = data['exp_conditions'].loc[1:, 'P_err']
    T_err = data['exp_conditions'].loc[1:, 'T_err']
    if data['exp_conditions'].loc[0, 'T_err'] == '%':
        T_err = T_err/100*T
    if data['exp_conditions'].loc[0, 'P_err'] == '%':
        P_err = P_err/100*P

    if P_units=='bar':
        P = P/1000
        P_err = P_err/1000

    if T_units=='K':
        T = T - 273.15

    data_summary['P'] = P
    data_summary['T'] = T
    data_summary['P_err'] = P_err
    data_summary['T_err'] = T_err

    data_summary['cond_bias'] = data['exp_conditions'].loc[1:, 'condition_bias']
    data_summary['cond_bias_dir'] = data['exp_conditions'].loc[1:, 'bias_direction']
    data_summary['results'] = data['rxn'].loc[1:, 'results']
    data_summary['rxns'] = data['rxn'].loc[1:, 'rxn_studied']
    data_summary['rxn_bias'] = data['rxn'].loc[1:, 'rxn_bias']
    data_summary['rxn_bias_dir'] = data['rxn'].loc[1:, 'bias_direction']


    data_summary.reset_index(inplace=True, drop=True)

    return data_summary

def process_rxn_data(data_summary):
    rxn_bnd_dir_lookup = pd.read_excel('_app_data/rxn_bnd_dir_lookup.xlsx', index_col='rxn')

    rxn_obs = data_summary.apply(
        lambda x: RxnObs(x['rxns'], x['results'], rxn_bnd_dir_lookup), axis=1)
    data_summary['rxn_dir'] = rxn_obs

    net_bias_dir = data_summary.apply(
        lambda x: x['rxn_dir'].net_rxn_bias(x['rxn_bias_dir'],
                                            x['cond_bias_dir']), axis=1)

    data_summary['net_bias_dir'] = net_bias_dir


    rxn_dir = data_summary.apply(lambda x: x['rxn_dir'].rxn_dir, axis=1)
    net_bias_dir = data_summary['net_bias_dir']

    valid = np.tile(False, rxn_dir.shape)
    valid[(net_bias_dir=='NONE').values] = True
    mask_fwd_bias = ((rxn_dir.apply(lambda x: x.startswith('REV'))) &
                     (net_bias_dir=='FWD')).values
    mask_rev_bias = ((rxn_dir.apply(lambda x: x.startswith('FWD'))) &
                     (net_bias_dir=='REV')).values
    valid[mask_fwd_bias] = True
    valid[mask_rev_bias] = True

    data_summary['valid'] = valid
    return data_summary

def load_processed_rxn_data(filename, data_summary=None):
    raw_data = load_rxn_data(filename)
    idata_summary = parse_rxn_data(raw_data)
    idata_summary = process_rxn_data(idata_summary)

    if data_summary is None:
        data_summary = idata_summary
    else:
        data_summary = data_summary.append(idata_summary)

    return data_summary

def sort_chem_comp(chem_comp):
    # print('in sort', chem_comp)
    comp = []
    for oxide in OXIDE_ORDER:
        if oxide in chem_comp:
            comp.append(oxide)

    for atom in PERIODIC_ORDER:
        if atom in chem_comp:
            comp.append(atom)

    for ichem in chem_comp:
        if ichem not in comp:
            comp.append(ichem)

    return comp

def _extract_data_units(df, unitless=True):
    if unitless:
        units = None
    else:
        fields = list(df.columns)
        units_list = list(df.iloc[0])
        units = OrderedDict()
        for field, unit in zip(fields, units_list):
            units[field] = unit

        df.drop(df.index[0], inplace=True) # drop units row after saving

    return units

def _extract_data_values(df):
    data_str = df.to_json(orient='records')
    json_str = '{"data": '+data_str+'}'
    data_tbl=json.loads(json_str, object_pairs_hook=OrderedDict)['data']
    return data_tbl

def extract_table_data(sheet, df_book, unitless=True):
    df = df_book[sheet].copy()

    fields = list(df.columns)
    units = _extract_data_units(df, unitless=unitless)
    data_tbl = _extract_data_values(df)

    data_dict = OrderedDict()
    data_dict['name'] = sheet
    data_dict['fields'] = fields
    data_dict['units'] = units
    data_dict['data'] = data_tbl
    return data_dict

def extract_unstructured_data(sheet, df_book):
    df = df_book[sheet].copy()

    data_str = df.to_json(orient='records')
    json_str = '{"data": '+data_str+'}'
    data_tbl=json.loads(json_str, object_pairs_hook=OrderedDict)['data']

    data_dict = OrderedDict()
    data_dict['name'] = sheet
    data_dict['data'] = data_tbl
    return data_dict

def read_json_data(filename):
    with open(filename,'r') as f:
        json_dat = json.load(f, object_pairs_hook=OrderedDict)

    return json_dat

def read_xls_data(filename):
    df_book = pd.read_excel(filename, sheet_name=None)
    sheetnames = list(df_book.keys())

    workbook_data = OrderedDict()
    for sheet in sheetnames:
        if sheet=='reference':
            data = extract_table_data(sheet, df_book, unitless=True)
        elif sheet=='notes':
            data = extract_unstructured_data(sheet, df_book)
        else:
            data = extract_table_data(sheet, df_book, unitless=False)

        workbook_data[sheet] = data

    return workbook_data

def json_to_xls(filename, xls_filename=None):

    if xls_filename is None:
        basename, extname = path.splitext(filename)
        xls_filename = basename+'.xlsx'

    json_data = read_json_data(filename)

    writer = pd.ExcelWriter(xls_filename)
    sheets = list(json_data.keys())
    for sheet in sheets:
        data = json_data[sheet]
        df = pd.DataFrame(data['data'])
        if 'fields' in data:
            fields = data['fields']
            units = data['units']
            if units is not None:
                # df = pd.concat([pd.DataFrame(units), df])
                df.loc[-1] = [units[key] for key in units]
                df.index = df.index + 1  # shifting index
                df.sort_index(inplace=True)


        df.to_excel(writer,sheet_name=sheet,index=False)
        worksheet = writer.sheets[sheet]
        # from IPython import embed; embed(); import ipbd; ipdb.set_trace()
        # worksheet.set_column(col_range,col_width)
        # worksheet.column_dimensions['A'].width=col_width
        adjust_worksheet_column_widths(worksheet)
        if sheet == 'reference':
            header_rows = 1
        elif sheet == 'notes':
            header_rows = 0
        else:
            header_rows = 2
        adjust_worksheet_row_colors(worksheet, header_rows=header_rows)

    writer.close()
    pass

def adjust_worksheet_column_widths(worksheet, fac=1.1):
    def as_text(value):
        if value is None:
            return ""
        return str(value)

    for column_cells in worksheet.columns:
        length = max(len(as_text(cell.value)) for cell in column_cells)
        length = np.ceil(fac*length)
        worksheet.column_dimensions[column_cells[0].column].width = length

    pass

def adjust_worksheet_row_colors(worksheet, header_rows=2):
    # redFill = PatternFill(start_color='FFFF0000', end_color='FFFF0000',
    #                       fill_type='solid')
    # ws['A1'].style = redFill
    #F9E2E2
    # my_red = openpyxl.styles.colors.Color(rgb='00FF0000')
    thin_border = Border(left=Side(style='thin'), right=Side(style='thin'),
                         top=Side(style='thin'), bottom=Side(style='thin'))


    my_red = openpyxl.styles.colors.Color(rgb='00F9E2E2')
    my_fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_red)
    for row_ind, row_cells in enumerate(worksheet.rows):
        if row_ind >= header_rows:
            break
        # worksheet.column_dimensions[column_cells[0].column].width = length
        for cell in row_cells:
            cell.fill = my_fill
            cell.border = thin_border

    pass

def xls_to_json(filename, json_filename=None):
    if json_filename is None:
        basename, extname = path.splitext(filename)
        json_filename = basename+'.json'

    workbook_data = read_xls_data(filename)
    s = json.dumps(workbook_data, indent=4)
    with open(json_filename,'w') as f:
        f.write(s)

    pass


#####################################################
def get_comp_list(type='major_elems'):
    if type=='major_oxides':
        comp = ['Al2O3', 'CO2', 'CaO', 'FeO', 'Fe2O3', 'H2O', 'K2O', 'MgO', 'MnO',
                'MnO2', 'Na2O', 'P2O5','SO3', 'SiO2', 'TiO2', 'Ti2O3']
    elif type=='all_oxides':
        comp = ['Al2O3', 'As2O3', 'Au2O', 'B2O3', 'BaO', 'BeO', 'CO2', 'CaO',
                'CeO2', 'Ce2O3', 'CoO', 'Cr2O3', 'Cs2O', 'CuO', 'Dy2O3', 'Er2O3',
                'EuO', 'Eu2O3', 'FeO', 'Fe2O3', 'Ga2O3', 'Gd2O3', 'GeO2', 'H2O',
                'HfO2', 'Ho2O3', 'K2O', 'La2O3', 'Li2O', 'Lu2O3', 'MgO', 'MnO',
                'MnO2', 'Mn3O4', 'MoO3', 'Na2O', 'Nb2O5', 'Nd2O3', 'NiO', 'P2O5',
                'PbO', 'Pr2O3', 'Rb2O', 'SO3', 'Sb2O3', 'Sc2O3', 'SiO2', 'Sm2O3',
                'SnO2', 'SrO', 'Ta2O5', 'Tb2O3', 'ThO2', 'TiO2', 'Ti2O3', 'Tm2O3',
                'UO2', 'U3O8', 'V2O5', 'WO3', 'Y2O3', 'Yb2O3', 'ZnO', 'ZrO2']
    elif type=='major_elems':
        comp = ['Si', 'Al', 'Ca', 'Mg', 'Na', 'K', 'Ti', 'Fe', 'Mn', 'P',
                'Cu', 'Zn', 'O']
    elif type=='minor_elems':
        comp = ['Ag','As', 'B', 'Ba', 'Bi', 'Cd', 'Co', 'Cr', 'Hg', 'Li',
                'Mn', 'Mo', 'Ni', 'Pb', 'Sb', 'Se', 'Sn', 'Sr', 'V', 'Zn']
    elif type=='all_elems':
        comp = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg',
                'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V',
                'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se',
                'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh',
                'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba',
                'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy',
                'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir',
                'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra',
                'Ac', 'Th', 'Pa', 'U']
    else:
        raise LookupError(type + ' is not a valid comp type. Try again.')

    return comp

def get_header_format(wb, header_color='F9E2E2'):
    header_fmt = wb.add_format({'bold': True, 'bg_color': header_color})
    return header_fmt

def get_unit_list(units):
    if units=='length':
        unit_list = ['inch','cm','mm','micron','ang']
    elif units=='volume':
        unit_list = ['cm3/mol','ang3/cell','cm3/g']
    elif units=='weight':
        unit_list = ['g','mg','microgram']
    elif units=='angle':
        unit_list = ['deg','radian']
    elif units=='amount':
        unit_list = [
            'wt%','mol%','vol%','mol','g','log10molal','wtppm','wtppb',
            'molppm','molppb','molal']
    elif units=='mode':
        unit_list = ['wt%','vol%','area%']
    elif units=='pressure':
        unit_list = ['kbar','GPa','bar','psi','Pa','TPa','Mbar']
    elif units=='temperature':
        unit_list = ['C','K','F']
    elif units=='fO2':
        unit_list = ['log10bar','fO2-IW','fO2-NNO','fO2-MMO','fO2-QFM','fO2-MW',
                     'fO2-GCH','fO2-HM','fO2-Cu+Cu2O','fO2-CCO']
    elif units=='time':
        unit_list = ['hr','s','min','day']
    elif units=='fraction':
        unit_list = ['%','decimal']
    elif units=='error':
        unit_list = ['absolute','%']
    elif units=='empty':
        unit_list = []
    else:
        unit_list = ['-']

    return unit_list

def add_column(ws, header_fmt, title, units=None, units_row=True,
               key_options=None,
               col_comment=None, Nrows=300, column=0, added_units=[]):

    if key_options is not None:
        _add_column_options(ws, key_options, Nrows=Nrows, column=column)

    column = _add_column_header(ws, header_fmt, title, units=units,
                                units_row=units_row, col_comment=col_comment,
                                column=column, added_units=added_units)
    return column

def _add_column_options(ws, key_options, Nrows=300, column=0):
    for ientry in range(Nrows):
        # ws.write(ientry+2, column, header_fmt)
        unit_cell = xl_rowcol_to_cell(ientry+2, column)
        ws.data_validation(unit_cell, {'validate': 'list',
                                       'source': key_options,
                                       'show_error': False})

    pass

def _add_column_header(ws, header_fmt, title, units=None, units_row=True,
                       col_comment=None, column=0, added_units=[]):
    ws.write(0, column, title, header_fmt)

    if col_comment is not None:
        ws.write_comment(xl_rowcol_to_cell(0, column), col_comment,
                         {'y_scale':3.0})

    if units_row:
        if units is None:
            ws.write(1, column, None, header_fmt)
        else:
            unit_list = get_unit_list(units)
            if not (not added_units):
                unit_list.extend(added_units)

            ws.write(1, column, unit_list[0], header_fmt)
            if len(unit_list)>1:
                ws.write(1, column, unit_list[0], header_fmt)
                unit_cell = xl_rowcol_to_cell(1, column)
                ws.data_validation(unit_cell, {'validate': 'list',
                                               'source': unit_list,
                                               'show_error': False})
    else:
        pass # No units row

    return column+1

def add_chem_headers(ws, chem_cols, header_fmt, data_options,
                     column=0, Nmax=50):

    for ichem in chem_cols :

        # icomp_cell = chem_sheet+'!'+xl_rowcol_to_cell(1+irow, 1)
        # itot_cell = chem_sheet+'!'+xl_rowcol_to_cell(1+irow, 2)

        # ititle = [
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)",'+
        #      '{0}))').format(icomp_cell,itot_cell),
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)_err",'+
        #      '{0}&"_err"))').format(icomp_cell,itot_cell),
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)_method",'+
        #      '{0}&"_method"))').format(icomp_cell,itot_cell),
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)_std",'+
        #      '{0}&"_std"))').format(icomp_cell, itot_cell)
        #     ]
        ititle = [
            (ichem),
            (ichem+'_err'),
            (ichem+'_method'),
            (ichem+'_std'),
        ]
        # unit_cond = '=IF(ISBLANK('+icomp_cell+'),"",{0})'
        column = add_column(ws, header_fmt, ichem,
                            units='amount', column=column)
        column = add_column(ws, header_fmt, ichem+'_err',
                            units='error', column=column)
        column = add_column(ws, header_fmt, ichem+'_method', column=column,
                            key_options=data_options['chem_method_opts'],
                            col_comment=data_options['chem_method_comment'])
        column = add_column(ws, header_fmt, ichem+'_lab_id', column=column,
                            col_comment=data_options['chem_lab_id_comment'])

    return column+1

#####################################################
def get_data_options():
    data_options = {}
    data_options['rxn_method_opts']  = [
        'xrd_peak_ratio','weightdiff','DTA','Optical','no_info']
    data_options['rxn_metric_units'] = ['mol%','ratio','microgram','area%']
    data_options['status_opts'] = ['Yes','No','Maybe']
    data_options['flux_opts'] = ['water','none','water+CO2',
                                 'molten_salt','oxalic_acid','no_info']
    data_options['contact_geom'] = ['mixed_grains','planar','single_xtal']
    data_options['press_medium'] = ['water','Ar_gas','Ne_gas',
                                    'He_gas','pyrex_glass','talc_sleeve',
                                    'none','no_info']
    data_options['P_meas_method'] = ['resist','tube_gauge',
                                     'xrd_vol_eos','ambient','no_info']
    data_options['T_meas_method'] = ['TC_D','TC_S','TC_K','power','no_info']
    data_options['T_meas_comment'] = (
        "TC_D: Thermocouple Type-D (W/Re)\n"+
        "TC_S: Thermocouple Type-S (PtRh10%-Pt) \n"+
        "TC_K: Thermocouple Type-K (Chromel/Alumel)\n"+
        "power: Heater Power-Temperature calibration curve\n"+
        "no_info: No information provided")

    data_options['T_meas_samp_loc'] = ['touching','adjacent','separate',
                                       'no_info']
    data_options['T_meas_heat_loc'] = ['center','axial','off_axis','aligned', 'no_info']

    # "log10bar: Absolute units log10(bars)\n"+
    # ADD ambient fO2 !
    data_options['fO2_buffer_comment'] = (
        "IW: Iron-wustite buffer\n"+
        "NNO: Nickel-nickel oxide buffer\n"+
        "MMO: MnO-Mn3O4 buffer\n"+
        "QFM: Quartz-fayalite-magnetite buffer\n"+
        "MW: Magnetite-wustite buffer\n"+
        "GCH: Graphite-methane buffer\n"+
        "HM: Hematite-magnetite buffer\n"+
        "Cu+Cu2O: Copper-copper oxide buffer\n"+
        "CCO: Graphite-carbon monoxide buffer\n"+
        "no_info: No information provided")

    data_options['fO2_buffer'] = ['IW','NNO','MMO','QFM','MW', 'GCH',
                                  'HM','Cu+Cu2O','CCO','no_info']

    data_options['fO2_method'] = ['unbuffered', 'buffer', 'capsule',
                                  'model']

    data_options['device_comment'] = (
        'no_info: No information provided\n'+
        'ambient: Ambient conditions measurement\n'+
        '1-atm: One atmosphere furnace\n'+
        'Belt: Belt apparatus\n'+
        'CSPV: Cold-seal pressure vessel\n'+
        'IHPV: Internally heated pressure vessel\n'+
        'MA: Multi-anvil apparatus\n'+
        'PC: Piston-cylinder apparatus\n'+
        'RQAC: Rapid quench autoclave\n'+
        'LHDAC: Laser heated diamond anvil cell\n'+
        'Gas: Gas-medium pressure apparatus\n'+
        'DAC300: 300K DAC')
    data_options['device'] = ['no_info', 'ambient', '1-atm', 'Belt', 'CSPV',
        'IHPV', 'MA', 'PC', 'RQAC', 'LHDAC', 'Gas', 'DAC300']

    data_options['container'] = ['Ag','AgPd','Au','AuPd','AuPd/Pt','C','Fe',
                                 'FePt','Mo','Oliv/Pt','Pt','PtC','PtRh',
                                 'Plag','Re']
    data_options['container_comment'] = (
        'Ag = Silver capsule\n'+
        'AgPd = Silver-palladium alloy capsule\n'+
        'Au = Gold capsule\n'+
        'AuPd = Gold-palladium alloy capsule\n'+
        'AuPd/Pt = Double capsule of platimun surrounding one of gold-palladium\n'+
        'C = Graphite capsule\n'+
        'Fe = Iron capsule\n'+
        'FePt = Platinum capsule "pretreated" with iron\n'+
        'Mo = Molybdenum capsule\n'+
        'Oliv/Pt = Double capsule of platinum surrounding one of olivine\n'+
        'Pt = Platinum capsule (or platinum wire at one atmosphere)\n'+
        'PtC = Double capsule of platinum surrounding one of graphite\n'+
        'PtRh = Platinum-rhodium alloy capsule\n'+
        'Plag=plagioclase container\n'+
        'Re = Rhenium capsule')

    data_options['certainty_opts'] = ['definite','probable','unresolved',
                                      'biased']

    data_options['xray_source_opts'] = ['W', 'Fe', 'Cu', 'synchrotron']

    data_options['xray_technique_opts'] = ['single_xtal','powder',
                                           'guinnier']

    data_options['xrd_peak_num_comment'] = (
        'Enter the number of xray diffraction peaks or independent single xtal orientations used to determine the crystal dimensions.')

    data_options['formula_comment'] = (
        "Report nominal chemical formula using Berman format. For example, Forsterite is 'Mg(2)Si(1)O(4)', Brucite is 'Mg(1)O(2)H(2)', Dolomite is Ca(1)Mg(1)C(2)O(6)."
    )

    data_options['sample_mix_comment'] = (
        'Define which starter phases samples are mixed together in the run. You must use the sample_id values found in column A of the starter_phase tab. Be sure to choose the best units (e.g. mol, mol%, wt%, etc.) Define how much of each phase using numbers e.g.: 50Ky1 + 50And1  or 1Sil1 + 1And1.'
    )

    data_options['chem_method_opts'] = ['AAS','synth','EMP',]
    data_options['chem_method_comment'] = (
        "Stoichiometry: Calculated from stoichiometry of measured species\n"+
        "Added: Amount added to experimental charge\n"+
        "Difference: Quantity determined by difference from an assumed total\n"+
        "no_info: No information provided\n"+
        "AAS: Atomic Absorption Spectroscopy\n"+
        "WDS: Wavelength dispersive X-ray analysis on an electron microprobe\n"+
        "EDS: Energy dispersive X-ray analysis on a scanning electron microscope or electron microprobe\n"+
        "LA_ICP_MS: Laser Inducticely-coupled plasma mass spectroscopy\n"
        "SIMS: Secondary ionization mass spectroscopy (ion microprobe)\n"+
        "WET: Wet chemical analysis\n"+
        "LECO: Total carbon determined by a LECO carbon analyser\n"+
        "Manometry: Water content determined by manometry\n"+
        "GC: Gas chromatography\n"+
        "LOI: Loss on ignition\n"+
        "Penfield: Water content determined using the Penfield method\n"+
        "TGA: Thermogravimetric analysis"
    )
    data_options['chem_method_opts'] = [
        "Stoichiometry", "Added", "Difference", "no_info", "AAS",
        "WDS", "EDS", "LA_ICP_MS", "SIMS", "WET", "LECO", "Manometry",
        "GC", "LOI", "Penfield", "TGA"]

    data_options['flux_amt'] = ['###','no_info','None','trace','excess']

    data_options['chem_lab_id_comment'] = (
        "ID of location where chemical analysis was performed. Lookup on lab_id sheet."
    )
    data_options['lab_id_comment'] = (
        "ID of location where experiment was performed. Lookup on lab_id sheet."
    )
    data_options['source_pubid_comment'] = (
        "For data that is actually from a previous publication, enter the pub_id of the source publication."
    )
    data_options['pubid_comment'] = (
        "Official Publication id for the publication. Look it up on the wiki."
    )
    return data_options

def write_generic_datasheets(wb, data_options, exp_chem):
    write_sheet_ref(wb, data_options)
    write_sheet_notes(wb, data_options)
    write_sheet_phase_symbols(wb, exp_chem)
    write_sheet_start_phs(wb, exp_chem, data_options)
    write_sheet_exp_cond(wb, data_options)
    pass

def write_eos_template(filename, exp_chem):
    data_options = get_data_options()
    wb = xlsxwriter.Workbook(filename)

    write_generic_datasheets(wb, data_options, exp_chem)
    write_sheet_eos_phases(wb, exp_chem, data_options)
    wb.close()

def parse_units_lookup(units_sheet):
    units_lookup = OrderedDict()

    for col_name in units_sheet.columns[1:]:
        unit_col = units_sheet[col_name]

        if unit_col.isnull()[0]:
            comment = None
        else:
            comment = unit_col[0]

        # unit_data = list(unit_col.dropna().values)

        # comment = unit_data[0]
        options = list(unit_col[1:].dropna().values)

        # options = list(options.dropna().values)
        if len(options)==0:
            options = None

        units_lookup[col_name] = {
            'comment': comment,
            'options': options
        }

    return units_lookup

def read_rxn_base_template(filename, units_sheet_name='<units>',
                           rxn_phase_sheet_name='<rxn_phase>',
                           chem_sheet_name='<chem_info>'):

    base_template = pd.read_excel(filename, sheet_name=None)


    units_sheet = base_template.pop(units_sheet_name)
    rxn_phase_sheet = base_template.pop(rxn_phase_sheet_name)
    chem_sheet = base_template.pop(chem_sheet_name)
    std_sheets = base_template


    units_lookup = parse_units_lookup(units_sheet)
    # all_sheet_names = np.array(list(base_template.keys()))
    # mask = [not str.startswith(name, '<') for name in all_sheet_names]
    # std_sheet_names = all_sheet_names[mask]

    return std_sheets, chem_sheet, units_lookup, rxn_phase_sheet

def extract_data_sheet(data_sheet, units_lookup=None):
    col_info = []
    col_names = data_sheet.columns[1:]

    has_unit_row = 'units' in data_sheet['columns'].values

    for title in col_names:
        assert not title.startswith('Unnamed'), 'All columns in data template must be named'

        col_data = data_sheet[title]

        if col_data.isnull()[0]:
            col_comment = None
        else:
            col_comment = col_data.values[0]

        units = None
        units_comment = None
        units_options = None

        if has_unit_row:
            # unitval = col_data.values[1]
            # units = [unitval]
            units = col_data.values[1]

            # print('unit type = ', type(units), '; units = ', units)
            if not isinstance(units, str):
                assert not np.isnan(units), (
                    'Units cannot be left blank in data template. '
                    'Instead use "-" character.'
                    )

            if units[0]=='<' and units[-1]=='>' and (
                units_lookup is not None):
                unit_entry = units_lookup[units]
                units_comment = unit_entry['comment']
                # units_options = unit_entry['options']
                # units = units_options[0]
                units = unit_entry['options']
                # print(units)
                # print(units_lookup[units])


            cell_options = col_data[2:]
        else:
            # units = None
            cell_options = col_data[1:]

        # assert units is not np.nan, 'Units must be provided'

        cell_options = list(cell_options.dropna().values)
        if len(cell_options)==0:
            cell_options = None

        icol_info = OrderedDict()
        icol_info['title'] = title
        icol_info['units'] = units
        # icol_info['units_options'] = units_options
        icol_info['units_comment'] = units_comment
        icol_info['col_comment'] = col_comment
        icol_info['cell_options'] = cell_options

        col_info.append(icol_info)

    return col_info

def gen_chem_info_block(chem_components, chem_sheet, units_lookup):
    chem_var = '<chem>'

    chem_info_block = []
    for chem_type in chem_components:
        chem_info_group = extract_data_sheet(chem_sheet, units_lookup=units_lookup)

        for chem_info in chem_info_group:
            title = chem_info['title']
            title = title.replace(chem_var, chem_type)
            chem_info['title'] = title

        chem_info_block.extend(chem_info_group)

    return chem_info_block

def write_chem_info(chem_components, chem_sheet,
                    data_info_template, units_lookup):
    chem_info_id = '<chem_info>'

    chem_info_block = gen_chem_info_block(chem_components, chem_sheet, units_lookup)

    # data_info = copy.deepcopy(data_info_template)
    data_info = OrderedDict()

    for sheet_name in data_info_template:
        all_col_info = []
        for col_info in data_info_template[sheet_name]:
            title = col_info['title']
            if title == chem_info_id:
                ichem_info_block = copy.deepcopy(chem_info_block)
                all_col_info.extend(ichem_info_block)
            else:
                all_col_info.append(col_info)

        data_info[sheet_name] = all_col_info

    return data_info

def extract_data_info(chem_components, rxn_phases, std_sheets,
                      chem_sheet, units_lookup, rxn_phase_sheet):
    data_info = OrderedDict()
    for sheet_name in std_sheets:
        data_sheet = std_sheets[sheet_name]
        data_info[sheet_name] = extract_data_sheet(
            data_sheet, units_lookup=units_lookup)

    for rxn_phase in rxn_phases:
        data_info[rxn_phase] = extract_data_sheet(
            rxn_phase_sheet, units_lookup=units_lookup)

    data_info = write_chem_info(chem_components, chem_sheet, data_info, units_lookup)

    return data_info

def add_excel_column(ws, column, title, header_fmt,
               col_comment=None, cell_options=None, units=None,
               units_comment=None,
               Nrows=300, col_width=20):
    """
    Add data column to excel spreadsheet with custom attributes

    """

    # Detect if units defined
    if units is None:
        header = 1
        unit_row = None
    else:
        header = 2
        unit_row = 1

    # Write column title
    ws.write(0, column, title, header_fmt)

    # Write column comment if present
    if col_comment is not None:
        ws.write_comment(xl_rowcol_to_cell(0, column), col_comment,
                         {'y_scale':3.0})

    # Write units if defined
    if unit_row is not None:
        if isinstance(units, str):
            unit_default = units
        else:
            unit_default = units[0]
            unit_cell = xl_rowcol_to_cell(unit_row, column)
            ws.data_validation(unit_cell, {'validate': 'list',
                                           'source': units,
                                           'show_error': False})

            if units_comment is not None:
                ws.write_comment(unit_cell, units_comment,
                                 {'y_scale':3.0})


        ws.write(unit_row, column, unit_default, header_fmt)

    # Write cell_options if present
    if cell_options is not None:
        for ientry in range(Nrows):
            unit_cell = xl_rowcol_to_cell(ientry+header, column)
            ws.data_validation(unit_cell, {'validate': 'list',
                                           'source': cell_options,
                                           'show_error': False})

    ws.set_column(0, column, col_width)

def write_excel_workbook(filename, data_info, header_color='F9E2E2'):
    wb = xlsxwriter.Workbook(filename)
    header_fmt = wb.add_format({'bold': True, 'bg_color': header_color})


    for sheet_name in data_info:
        ws = wb.add_worksheet(sheet_name)

        all_col_info = data_info[sheet_name]

        unit_free = []
        for column, col_info in enumerate(all_col_info):
            title = col_info['title']
            units = col_info['units']

            unit_free.append(units is None)

            # units_options = col_info['units_options']
            units_comment = col_info['units_comment']
            col_comment = col_info['col_comment']
            cell_options = col_info['cell_options']

            add_excel_column(ws, column, title, header_fmt,
                             col_comment=col_comment,
                             cell_options=cell_options,
                             units=units,
                             units_comment=units_comment)

        if np.all(unit_free):
            ws.freeze_panes('A2')

        elif sheet_name=='starter_phase':
            ws.freeze_panes('C3')

        else:
            ws.freeze_panes('C3')

    # wb.close()
    return wb

def get_col_num(sheet_name, col_name, data_info):
    assert sheet_name in data_info, (
        'That is not a valid sheet_name. Must select from: '+
        str([key for key in data_info])
    )

    data_info_sheet = data_info[sheet_name]

    columns = np.array([idata_info['title'] for idata_info in data_info_sheet])

    col_num = np.where(columns==col_name)[0]
    assert len(col_num) > 0, 'That col_name is not valid. Must select from: '+ str(columns)

    col_num = int(col_num)
    return col_num

def fill_phase_sample_info(phases, phase_symbols, sample_ids,
                           sample_names, data_info, wb):
    sheet_name = 'phase_symbols'
    ws = wb.get_worksheet_by_name(sheet_name)

    phs_col = get_col_num(sheet_name, 'phase', data_info)
    sym_col = get_col_num(sheet_name, 'phase_symbol', data_info)

    irows=1

    for phs, symbol in zip(phases, phase_symbols):
        ws.write(irows, phs_col, phs)
        ws.write(irows, sym_col, symbol)
        irows += 1

    sheet_name = 'starter_phase'
    ws = wb.get_worksheet_by_name(sheet_name)

    id_col = get_col_num(sheet_name, 'sample_id', data_info)
    name_col = get_col_num(sheet_name, 'sample_name', data_info)
    sym_col = get_col_num(sheet_name, 'phase_symbol', data_info)

    irows=2

    for samp_id, samp_name, symbol in zip(sample_ids, sample_names, phase_symbols):
        ws.write(irows, id_col, samp_id)
        ws.write(irows, name_col, samp_name)
        ws.write(irows, sym_col, symbol)
        irows += 1

def write_phase_rxn_template_new(
    filename, exp_chem,
    base_template='phase-rxn-data-base-template.xlsx'):

    chem_components = ['MgO','SiO2','Al2O3']
    rxn_phases = ['Liquid', 'Feldspar','Olivine']

    filename = 'test_workbook.xlsx'



    phases = np.array(exp_chem['phases'])
    phase_symbols = np.array(exp_chem['symbols'])
    chem_cols = sort_chem_comp(exp_chem['starter_chem_comps'])
    is_starter = np.array(exp_chem['is_starter_phase'])
    is_rxn_soln_phase = exp_chem['is_rxn_solution_phase']

    # phases = ['Liquid', 'Albite', 'Olivine']
    # phase_symbols = ['Liq', 'Ab', 'Ol']
    sample_ids = ['Liq1','Ab1','Ol1']
    sample_names = ['LiquidSample1', 'AlbiteSample1','OlivineSample1']

    std_sheets, chem_sheet, units_lookup, rxn_phase_sheet = (
        read_rxn_base_template(base_template))

    print('hello')
    print(rxn_phase_sheet)

    data_info = extract_data_info(
        chem_components, rxn_phases, std_sheets,
        chem_sheet, units_lookup, rxn_phase_sheet)

    wb = write_excel_workbook(filename, data_info)
    fill_phase_sample_info(phases, phase_symbols, sample_ids,
                           sample_names, wb)


    # write_generic_datasheets(wb, data_options, exp_chem)
    # write_sheet_samp_material(wb, data_options)
    # write_sheet_rxn(wb, data_options)
    # write_sheet_soln_phases(wb, exp_chem, data_options)
    wb.close()

def write_phase_rxn_template(filename, exp_chem):
    data_options = get_data_options()
    wb = xlsxwriter.Workbook(filename)

    write_generic_datasheets(wb, data_options, exp_chem)
    write_sheet_samp_material(wb, data_options)
    write_sheet_rxn(wb, data_options)
    write_sheet_soln_phases(wb, exp_chem, data_options)
    wb.close()

def write_sheet_ref(wb, data_options, header_color='F9E2E2', col_width=20):
    header_fmt = get_header_format(wb, header_color=header_color)

    ws = wb.add_worksheet('reference')

    column = 0
    column = add_column(ws, header_fmt, 'pub_id', units=None, units_row=False,
                        column=column,col_comment=data_options['pubid_comment'])
    column = add_column(ws, header_fmt, 'authors', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'date', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'title', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'journal', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'volume', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'issue', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'pages', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'doi', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'url', units=None, units_row=False,
                        column=column)
    column = add_column(ws, header_fmt, 'notes', units=None, units_row=False,
                        column=column)

    ws.set_column(0, column-1, col_width)
    pass

def write_sheet_notes(wb, data_options, header_color='F9E2E2', col_width=20):
    header_fmt = get_header_format(wb, header_color=header_color)

    ws = wb.add_worksheet('notes')
    ws.write('A1', 'notes', header_fmt)
    pass

def write_sheet_phase_symbols(wb, exp_chem, header_color='F9E2E2',
                              col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)


    ws = wb.add_worksheet('phase_symbols')

    column = 0
    (column, phs_col) = (add_column(ws, header_fmt, 'phase', column=column,
                                    units=None), column)

    (column, sym_col) = (add_column(ws, header_fmt, 'phase_symbol',
                                    column=column, units=None), column)

    ws.set_column(0, column-1, col_width)

    phases = np.array(exp_chem['phases'])
    phase_symbols = np.array(exp_chem['symbols'])

    irows=1
    for phs, symbol in zip(phases, phase_symbols):
        ws.write(irows, phs_col, phs)
        ws.write(irows, sym_col, symbol)
        irows += 1

    pass

def write_sheet_start_phs(wb, exp_chem, data_options,
                          header_color='F9E2E2', col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)

    ws = wb.add_worksheet('starter_phase')

    column = 0
    column, sampID_col = (add_column(ws, header_fmt, 'sample_id', units=None,
                                     column=column), column)

    column, sample_name_col = (add_column(ws, header_fmt, 'sample_name',
                                          units=None, column=column), column)
    column, phase_col = (add_column(ws, header_fmt, 'phase_symbol', units=None,
                                    column=column), column)

    column = add_column(ws, header_fmt, 'formula', units=None, column=column,
                        col_comment=data_options['formula_comment'])

    column = add_column(ws, header_fmt, 'source_pubid', column=column,
                        col_comment=data_options['source_pubid_comment'])
    column = add_column(ws, header_fmt, 'notes', units=None, column=column)
    column = add_column(ws, header_fmt, 'table_num', units=None, column=column)
    column = add_column(ws, header_fmt, 'figure_num', units=None, column=column)
    column = add_column(ws, header_fmt, 'page_num', units=None, column=column)

    column = add_column(ws, header_fmt, 'synthetic', units=None, column=column, key_options=data_options['status_opts'])
    column = add_column(ws, header_fmt, 'contaminant_phases', units='amount',
                        column=column)
    column = add_column(ws, header_fmt, 'Total', units='amount', column=column)


    # print('starter_chem_comps',exp_chem['starter_chem_comps'])
    chem_cols = sort_chem_comp(exp_chem['starter_chem_comps'])
    column = add_chem_headers(ws, chem_cols, header_fmt, data_options, column=column, Nmax=50)
    # column = add_column(ws, header_fmt, 'Total', units='amount', column=column)
    ws.set_column(0, column-1, col_width)

    # Add starter phase names to rows
    # write rows
    phases = np.array(exp_chem['phases'])
    phase_symbols = np.array(exp_chem['symbols'])
    is_starter = np.array(exp_chem['is_starter_phase'])
    # print(is_starter)
    starter_phases = phases[is_starter]
    starter_phase_symbols = phase_symbols[is_starter]
    # print(starter_phases)

    irows=2
    for ind,(phs, symbol) in enumerate(zip(
        starter_phases, starter_phase_symbols)):

        ws.write(irows, sampID_col, symbol+'1')
        ws.write(irows, sample_name_col, phs+'Sample1')
        ws.write(irows, phase_col, symbol)
        irows += 1

    pass

def write_sheet_exp_cond(wb, data_options, header_color='F9E2E2',
                         col_width=20):
    header_fmt = get_header_format(wb, header_color=header_color)
    ws = wb.add_worksheet('exp_conditions')

    column = 0
    column = add_column(ws, header_fmt, 'index', column=column)
    column = add_column(ws, header_fmt, 'run_id', column=column)
    column = add_column(ws, header_fmt, 'lab_id', column=column,
                        col_comment=data_options['lab_id_comment'])
    column = add_column(ws, header_fmt, 'source_pubid', column=column,
                        col_comment=data_options['source_pubid_comment'])
    column = add_column(ws, header_fmt, 'notes', column=column)
    column = add_column(ws, header_fmt, 'trust_conditions', column=column,
                        key_options=data_options['status_opts'])
    column = add_column(ws, header_fmt, 'table_num', column=column)
    column = add_column(ws, header_fmt, 'figure_num', column=column)
    column = add_column(ws, header_fmt, 'page_num', column=column)
    column = add_column(ws, header_fmt, 'device', column=column,
                        key_options=data_options['device'],
                        col_comment=data_options['device_comment'])
    column = add_column(ws, header_fmt, 'container', column=column,
                        key_options=data_options['container'],
                        col_comment=data_options['container_comment'])
    column = add_column(ws, header_fmt, 'container_width',
                        units='length', column=column)
    column = add_column(ws, header_fmt, 'container_length',
                        units='length', column=column)
    column = add_column(ws, header_fmt, 'press_medium', column=column, key_options=data_options['press_medium'])
    # column = add_column(ws, header_fmt, 'P_meas_method', column=column)
    column = add_column(ws, header_fmt, 'P_meas_method', column=column,
        key_options=data_options['P_meas_method'])
    column = add_column(ws, header_fmt, 'T_meas_method', column=column,
                        key_options=data_options['T_meas_method'],
                        col_comment=data_options['T_meas_comment'])
    column = add_column(ws, header_fmt, 'T_meas_samp_loc', column=column,
        key_options=data_options['T_meas_samp_loc'] )

    column = add_column(ws, header_fmt, 'T_meas_heat_loc', column=column,
        key_options=data_options['T_meas_heat_loc'] )
    column = add_column(ws, header_fmt, 'P', units='pressure',column=column)
    column = add_column(ws, header_fmt, 'P_err', units='error', column=column)
    column = add_column(ws, header_fmt, 'T', units='temperature', column=column)
    column = add_column(ws, header_fmt, 'T_err', units='error', column=column)
    column = add_column(ws, header_fmt, 'fO2_method', column=column,
                        key_options=data_options['fO2_method'])
    column = add_column(ws, header_fmt, 'fO2_ref_buffer', column=column,
                        col_comment=data_options['fO2_buffer_comment'],
                        key_options=data_options['fO2_buffer'])
    column = add_column(ws, header_fmt, 'fO2_offset', column=column)
    column = add_column(ws, header_fmt, 'fO2_err', column=column)
    column = add_column(ws, header_fmt, 'equil_time', units='time',
                        column=column)

    ws.set_column(0, column-1, col_width)
    # adjust_worksheet_column_widths(ws, fac=1.1)
    pass

def write_sheet_samp_material(wb, data_options, header_color='F9E2E2',
                              col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)
    ws = wb.add_worksheet('sample_material')

    column = 0
    column = add_column(ws, header_fmt, 'index', column=column)
    column = add_column(ws, header_fmt, 'run_id', column=column)
    column = add_column(ws, header_fmt, 'source_pubid', column=column,
                        col_comment=data_options['source_pubid_comment'])
    column = add_column(ws, header_fmt, 'sample_mix', column=column,
                        units='amount',
                        col_comment=data_options['sample_mix_comment'] )

    # column = add_column(ws, header_fmt, 'other_phases', column=column)
    column = add_column(ws, header_fmt, 'notes', column=column)

    ws.set_column(0, column-1, col_width)

def write_sheet_rxn(wb, data_options, header_color='F9E2E2', col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)
    ws = wb.add_worksheet('rxn')

    column = 0
    column = add_column(ws, header_fmt, 'index', column=column)
    column = add_column(ws, header_fmt, 'run_id', column=column)
    column = add_column(ws, header_fmt, 'rxn_studied', column=column)

    # column = add_column(ws, header_fmt, 'metastable',
    #                     key_options=data_options['status_opts'], column=column)
    column = add_column(ws, header_fmt, 'flux_type',
                        key_options=data_options['flux_opts'],
                        column=column)
    column = add_column(ws, header_fmt, 'flux_amt', units='amount',
                        column=column, key_options=data_options['flux_amt'])
    # column = add_column(ws, header_fmt, 'contact_geom', column=column)
    column = add_column(ws, header_fmt, 'contact_geom',
                        key_options=data_options['contact_geom'], column=column)

    column = add_column(ws, header_fmt, 'grind_duration', units='time',column=column)
    column = add_column(ws, header_fmt, 'grain_size', units='length',column=column,added_units=['mesh#'])
    column = add_column(ws, header_fmt, 'single_xtal_size', units='weight', column=column)
    column = add_column(ws, header_fmt, 'method',
                        key_options=data_options['rxn_method_opts'],
                        column=column)
    column = add_column(ws, header_fmt, 'rxn_metric_unit',
                        key_options=data_options['rxn_metric_units'],
                        column=column)
    column = add_column(ws, header_fmt, 'rxn_metric_init', column=column)
    column = add_column(ws, header_fmt, 'rxn_metric_final', column=column)
    column = add_column(ws, header_fmt, 'rxn_metric_err', column=column, units='error')

    # Remove rxn_metric_resid
    # column = add_column(ws, header_fmt, 'rxn_metric_resid', column=column)
    column = add_column(ws, header_fmt, 'results', column=column)

    column = add_column(ws, header_fmt, 'rxn_direction_certainty',
                        key_options=data_options['certainty_opts'], column=column)
    # column = add_column(ws, header_fmt, 'completion', column=column, units='fraction')
    column = add_column(ws, header_fmt, 'notes', column=column)

    ws.set_column(0, column-1, col_width)

def write_sheet_soln_phases(wb, exp_chem, data_options,
                            header_color='F9E2E2', col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)

    is_rxn_soln_phase = exp_chem['is_rxn_solution_phase']

    phases = np.array(exp_chem['phases'])
    phase_symbols = np.array(exp_chem['symbols'])
    # print('inwrite',exp_chem['chem_comps'])
    chem_comps = []
    for chem_comp in exp_chem['chem_comps']:
        chem_comps.append(np.array(sort_chem_comp(chem_comp)))

    # print('chem_comps',chem_comps)

    soln_phases = phases[is_rxn_soln_phase]
    soln_phase_symbols = phase_symbols[is_rxn_soln_phase]
    soln_chem_comps = chem_comps

    for soln_phs, soln_symbol, soln_comps in zip(
        soln_phases, soln_phase_symbols, soln_chem_comps):

        ws = wb.add_worksheet(soln_phs)
        column = 0

        # index	run_id	sample_id	source_pubid	notes	table_num	figure_num	page_num


        column = add_column(ws, header_fmt, 'index', column=column)
        column = add_column(ws, header_fmt, 'run_id', column=column)
        column = add_column(ws, header_fmt, 'sample_id', column=column)
        column = add_column(ws, header_fmt, 'formula',  column=column,
                            col_comment=data_options['formula_comment'])
        column = add_column(ws, header_fmt, 'source_pubid', column=column,
                            col_comment=data_options['source_pubid_comment'])
        column = add_column(ws, header_fmt, 'notes', column=column)
        column = add_column(ws, header_fmt, 'table_num', column=column)
        column = add_column(ws, header_fmt, 'figure_num', column=column)
        column = add_column(ws, header_fmt, 'page_num', column=column)

        column = add_column(ws, header_fmt, 'mode',
                            units='mode', column=column)
        column = add_column(ws, header_fmt, 'Total',
                            units='amount', column=column)

        chem_cols = soln_comps
        column = add_chem_headers(ws, chem_cols, header_fmt, data_options,
                          column=column, Nmax=50)

        ws.set_column(0, column-1, col_width)

def write_sheet_eos_phases(wb, exp_chem, data_options, header_color='F9E2E2', col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)

    phases = np.array(exp_chem['phases'])
    symbols = np.array(exp_chem['symbols'])


    for phs, symbol in zip(phases, symbols):

        ws = wb.add_worksheet(phs)
        column = 0

        column = add_column(ws, header_fmt, 'index', column=column)
        column = add_column(ws, header_fmt, 'run_id', column=column)
        # column = add_column(ws, header_fmt, 'lab_id', column=column, col_comment=data_options['eos_lab_id_comment'])
        column = add_column(ws, header_fmt, 'sample_id', column=column)
        column = add_column(ws, header_fmt, 'source_pubid', column=column,
                            col_comment=data_options['source_pubid_comment'])
        column = add_column(ws, header_fmt, 'trust', column=column)
        column = add_column(ws, header_fmt, 'notes', column=column)
        column = add_column(ws, header_fmt, 'table_num', column=column)
        column = add_column(ws, header_fmt, 'figure_num', column=column)
        column = add_column(ws, header_fmt, 'page_num', column=column)
        column = add_column(ws, header_fmt, 'V',
                            units='volume', column=column)
        column = add_column(ws, header_fmt, 'V_err',
                            units='volume', column=column)
        column = add_column(ws, header_fmt, 'a', units='empty',
                            column=column, added_units=['ang'])
        column = add_column(ws, header_fmt, 'a_err', units='empty',
                            column=column, added_units=['ang'])
        column = add_column(ws, header_fmt, 'b', units='empty',
                            column=column, added_units=['ang'])
        column = add_column(ws, header_fmt, 'b_err', units='empty',
                            column=column, added_units=['ang'])
        column = add_column(ws, header_fmt, 'c', units='empty',
                            column=column, added_units=['ang'])
        column = add_column(ws, header_fmt, 'c_err', units='empty',
                            column=column, added_units=['ang'])
        column = add_column(ws, header_fmt, 'alpha', units='angle',
                            column=column)
        column = add_column(ws, header_fmt, 'alpha_err', units='angle',
                            column=column)
        column = add_column(ws, header_fmt, 'beta', units='angle',
                            column=column)
        column = add_column(ws, header_fmt, 'beta_err', units='angle',
                            column=column)
        column = add_column(ws, header_fmt, 'gamma', units='angle',
                            column=column)
        column = add_column(ws, header_fmt, 'gamma_err', units='angle',
                            column=column)

        column = add_column(
            ws, header_fmt, 'xray_source', units=None, column=column,
            key_options=data_options['xray_source_opts'])

        column = add_column(
            ws, header_fmt, 'xray_technique', units=None, column=column,
            key_options=data_options['xray_technique_opts'])
        column = add_column(ws, header_fmt, 'xrd_peak_num', column=column,
                            col_comment=data_options['xrd_peak_num_comment'])

        ws.set_column(0, column-1, col_width)


    # ws = wb.add_worksheet('eos')
    # column = 0

    # column = add_column(ws, header_fmt, 'index', column=column)
    # column = add_column(ws, header_fmt, 'run_id', column=column)
    # column = add_column(ws, header_fmt, 'lab_id', column=column)
    # column = add_column(ws, header_fmt, 'sample_id', column=column)
    # column = add_column(ws, header_fmt, 'source_pubid', column=column)
    # column = add_column(ws, header_fmt, 'trust', column=column)
    # column = add_column(ws, header_fmt, 'notes', column=column)
    # column = add_column(ws, header_fmt, 'table_num', column=column)
    # column = add_column(ws, header_fmt, 'figure_num', column=column)
    # column = add_column(ws, header_fmt, 'page_num', column=column)
    # column = add_column(ws, header_fmt, 'V',
    #                            units='volume', column=column)
    # column = add_column(ws, header_fmt, 'V_err',
    #                            units='volume', column=column)
    # column = add_column(ws, header_fmt, 'a', units='empty',
    #                            column=column, added_units=['ang'])
    # column = add_column(ws, header_fmt, 'a_err', units='empty',
    #                            column=column, added_units=['ang'])
    # column = add_column(ws, header_fmt, 'b', units='empty',
    #                            column=column, added_units=['ang'])
    # column = add_column(ws, header_fmt, 'b_err', units='empty',
    #                            column=column, added_units=['ang'])
    # column = add_column(ws, header_fmt, 'c', units='empty',
    #                            column=column, added_units=['ang'])
    # column = add_column(ws, header_fmt, 'c_err', units='empty',
    #                            column=column, added_units=['ang'])
    # column = add_column(ws, header_fmt, 'alpha', units='angle',
    #                            column=column)
    # column = add_column(ws, header_fmt, 'alpha_err', units='angle',
    #                            column=column)
    # column = add_column(ws, header_fmt, 'beta', units='angle',
    #                            column=column)
    # column = add_column(ws, header_fmt, 'beta_err', units='angle',
    #                            column=column)
    # column = add_column(ws, header_fmt, 'gamma', units='angle',
    #                            column=column)
    # column = add_column(ws, header_fmt, 'gamma_err', units='angle',
    #                            column=column)

    # column = add_column(
    #     ws, header_fmt, 'xray_source', units=None, column=column,
    #     key_options=data_options['xray_source_opts'])

    # column = add_column(
    #     ws, header_fmt, 'xray_technique', units=None, column=column,
    #     key_options=data_options['xray_technique_opts'])
    # column = add_column(ws, header_fmt, 'xrd_peak_num', column=column,
    #                     col_comment=data_options['xrd_peak_num_comment'])

    # ws.set_column(0, column-1, col_width)
    pass

def get_rxn_phases(rxn_str, ignore_missing_space=False, get_rxn_dir=False,
                   sort=False):
    chem_terms = str.split(rxn_str)
    phase_symbols = []
    # print(chem_terms)
    rxn_dir = []
    direc = -1
    for term in chem_terms:
        if term == '=':
            direc = +1

        if term not in ['=','+','-']:
            if ignore_missing_space:
                term = re.sub('^[+=\-]*','',term)
            sym = re.sub('^[0-9]*', '', term)
            sym = sym.split(sep='/')
            phase_symbols.extend(sym)
            rxn_dir.append(direc)

    phase_symbols = np.array(phase_symbols)
    rxn_dir = np.array(rxn_dir)

    if sort:
        phase_symbols = np.sort(phase_symbols)

    if get_rxn_dir:
        return phase_symbols, rxn_dir
    else:
        return phase_symbols

    pass

# FIXME:Validation Code -------------------------------------------------------
def get_input_formats():
    number = '[+-]?\d+(?:\.)?(?:\.\d+)?(?:[eE][+-]?\d+)?'
    coefficient = '\d?(?:\.)?(?:\.\d)?'
    #FIXME: call allowed phases from phase list to ensure proper abbreviations are used
    # pure_phases = pd.read_csv('_phase_data/pure-phases.csv')
    # solution_phases = pd.read_csv('_phase_data/solution-phases.csv')
    # phases=''
    # for phase_symbol in pure_phases['symbol']:
    #     phases+='{}|'.format(phase_symbol)
    # for phase_symbol in solution_phases['symbol']:
    #     phases+='{}|'.format(phase_symbol)
    phases = '[a-zA-Z0-9]*'
    # phases='('+phases[:-1]+')'
    input_format = {}
    input_format['rxn'] = '{coef}?{sym}\s*?(\+\s*{coef}?{sym}\s*)*?\=\s*{coef}?{sym}\s*?(\+\s*{coef}?{sym}\s*)*?'.format(coef=coefficient,sym=phases)
    input_format['P'] = '({0}\s*<->\s*{0}|>\s*{0}|<\s*{0}|~\s*{0}|no_info)'.format(number)
    input_format['P_err'] = '({0}\s*\%|{0}\s*-\s*{0}|>\s*{0}|<\s*{0}|no_info)'.format(number)
    input_format['T'] = '({0}\s*<->\s*{0}|>\s*{0}|<\s*{0}|~\s*{0}|no_info)'.format(number)
    input_format['T_err'] = '({0}\s*\%|{0}\s*-\s*{0}|>\s*{0}|<\s*{0}|no_info)'.format(number)
    input_format['equil_time'] = '({0}\s*-\s*{0}|>\s*{0}|<\s*{0}|no_info)'.format(number)
    input_format['rxn_metric_init'] = '({0}\s*<->\s*{0}|>\s*{0}|<\s*{0}|~\s*{0}|no_info)'.format(number)
    input_format['rxn_metric_final'] = '({0}\s*<->\s*{0}|>\s*{0}|<\s*{0}|~\s*{0}|no_info)'.format(number)
    input_format['rxn_metric_err'] = '({0}\s*<->\s*{0}|>\s*{0}|<\s*{0}|~\s*{0}|no_info)'.format(number)
    input_format['results'] = '{0}\s(\+|\-|\+\?|\-\?|\=\?|\=)(?:\;\s{0}\s(\+|\-|\+\?|\-\?|\=\?|\=))*?|no_info'.format(phases)
    return input_format

def xlsx_to_pandas(filename, path, include_rxn_dir=False):
    path += filename
    # full spreadsheet
    data = pd.read_excel(path,sheet_name=None,skiprows=[None,None,1,1,1,1,1])

    # summary data
    df = pd.concat([data['exp_conditions']['run_id'],
                    data['rxn']['rxn_studied'],
                    data['exp_conditions']['P'],
                    data['exp_conditions']['P_err'],
                    data['exp_conditions']['T'],
                    data['exp_conditions']['T_err'],
                    data['exp_conditions']['equil_time'],
                    data['rxn'][rxn_metric_init],
                    data['rxn'][rxn_metric_final],
                    data['rxn']['rxn_metric_err'],
                    data['rxn']['results']], axis=1)


    if include_rxn_dir:
        direc = []
        for i, rxn in enumerate(df['rxn_studied']):
            direc.append(get_rxn_direction(rxn,df['results'].loc[i]))
        # add to df
        df['rxn_direction'] = pd.Series(direc,index=df.index)
    return df

def run_tests(data):
    input_format = get_input_formats()
    test = ['pass']
    test.append(test_sheet_starter_phase(data, input_format))
    test.append(test_sheet_exp_cond(data, input_format))
    test.append(test_sheet_sample_material(data, input_format))
    test.append(test_sheet_rxn(data, input_format))
    if np.all(np.array(test)=='pass'):
        has_errors = False
        print('EVERYTHING LOOKS GOOD (:-)')
    else:
        has_errors = True
        print('PLEASE GO BACK AND FIX THESE ERRORS')

    return has_errors

def _make_data_plottable(df):
    """
    Fix non-numeric issues to make data plottable
    """

    df_plt = df.copy()

    # from IPython import embed; embed(); import ipdb as pdb; pdb.set_trace()

    rxns = np.unique(df_plt['rxn_studied'])

    numeric_cols = ['P', 'P_err', 'T', 'T_err', 'equil_time', 'rxn_metric_value', 'rxn_metric_err']

    for indcol, col in enumerate(df_plt):
        if col not in numeric_cols:
            continue

        for ind, val in enumerate(df_plt[col]):
            if ('<' in str(val)) or ('>' in str(val)):
                # ignore upper/lower bounds for plotting, instead just take avg value
                tokens = re.split('[\s <>]',val)
                vals = []
                for tok in tokens:
                    if not tok:
                        # remove empty strings
                        continue

                    try:
                        vals.append(float(tok))
                    except:
                        pass

                # Replace all values with their mean
                vals = np.array(vals)
                try:
                    df_plt.iloc[ind,indcol] = np.mean(vals)
                except:
                    from IPython import embed; embed(); import ipdb as pdb; pdb.set_trace()

    # df_plt['trust'] = pd.Series(np.tile(True,trust_cond.shape))
    # df_plt['trust'][trust_cond=='No'] = False
    # df_plt['trust'][trust_cond=='Maybe'] = False

    return df_plt

def read_phase_rxn_data(filepath):
    def get_units(filepath):
        raw_data = pd.read_excel(filepath,sheet_name=None)

        units = {}
        units['P'] = raw_data['exp_conditions']['P'].iloc[0]
        units['P_err'] = raw_data['exp_conditions']['P_err'].iloc[0]
        units['T'] = raw_data['exp_conditions']['T'].iloc[0]
        units['T_err'] = raw_data['exp_conditions']['T_err'].iloc[0]
        units['rxn_metric_err'] = raw_data['rxn']['rxn_metric_err'].iloc[0]
        units['equil_time'] = raw_data['exp_conditions']['equil_time'].iloc[0]
        return units

    data_dict = pd.read_excel(filepath,sheet_name=None,
                              skiprows=[None,None,1,1,1,1,1])

    old_format_errors = []

    if 'rxn_metric_zero' in data_dict['rxn']:
        old_format_errors.append('Your excel file uses an old format and needs minor changes.\nIn the rxn sheet, you have a column titled "rxn_metric_zero".\n\nChange the column name in the "rxn" tab and rerun this validation script:\n\n"rxn_metric_zero" -> "rxn_metric_init"')

    if 'rxn_metric_value' in data_dict['rxn']:
        old_format_errors.append('Your excel file uses an old format and needs minor changes.\nIn the rxn sheet, you have a column titled "rxn_metric_value".\n\nChange the column name in the "rxn" tab and rerun this validation script:\n\n"rxn_metric_value" -> "rxn_metric_final"')

    if 'trust_conditions' not in data_dict['exp_conditions']:
        old_format_errors.append('Your excel file uses an old format and needs minor changes.\nIn the exp_conditions sheet, you are missing the "trust_conditions" column.\n\nInsert a new column to the right of the notes column and label the column header "trust_conditions":\n\n+"trust_conditions"')

    if not not old_format_errors:
        print('==========')
        print('OLD FORMAT ERRORS:')
        print()
        for ind, error in enumerate(old_format_errors):
            print('---\n')
            print(ind, '. ', error)


        print('==========')
        print('Make suggested changes and be sure that these new data columns are properly filled in with the information from publication. Then try resubmitting dataset.')
        print('==========')
        assert False, 'Make suggested changes and try again.'

    units = get_units(filepath)

    df = pd.concat([data_dict['exp_conditions']['run_id'],
                    data_dict['rxn']['rxn_studied'],
                    data_dict['exp_conditions']['trust_conditions'],
                    data_dict['exp_conditions']['P'],
                    data_dict['exp_conditions']['P_err'],
                    data_dict['exp_conditions']['T'],
                    data_dict['exp_conditions']['T_err'],
                    data_dict['exp_conditions']['equil_time'],
                    data_dict['rxn']['rxn_metric_init'],
                    data_dict['rxn']['rxn_metric_final'],
                    data_dict['rxn']['rxn_metric_err'],
                    data_dict['rxn']['rxn_direction_certainty'],
                    data_dict['rxn']['results'],
                    ],axis=1)

    df.loc[df['trust_conditions'].isnull(),'trust_conditions'] = 'Yes'

    direc = []
    for i, (rxn, dir_certainty) in enumerate(zip(
        df['rxn_studied'], df['rxn_direction_certainty'])):

        irxn_dir = get_rxn_direction(
            rxn,df['results'].loc[i], rxn_direction_certainty=dir_certainty)

        direc.append(irxn_dir)
    # add to df
    df['rxn_direction'] = pd.Series(direc,index=df.index)
    # units

    data = OrderedDict()
    data['data_dict'] = data_dict
    data['df'] = df
    data['units'] = units
    data['df_plt'] = _make_data_plottable(df)

    return data

# FIXME: average ranges and take bounds for plotting
# FIXME: add jitter to points that plot of top of each other
def visual_validation(summary_data):
    units = summary_data['units']
    df_plt = summary_data['df_plt']

    # plot bar chart of experiments
    N = df_plt['rxn_studied'].count()
    df_plt['rxn_studied'].value_counts().plot.bar(color='r',alpha=0.7)
    plt.ylabel('number of experiments')
    plt.ylim([0,max(df_plt['rxn_studied'].value_counts())+1])
    plt.title('Total number of experiments = {}'.format(N))
    plt.show()


    # P-T plots and time-exp#
    df_plt.replace(to_replace='no_info', value=np.nan , inplace=True)
    plt.figure(figsize=(6,5))
    plt.errorbar(df_plt['T'],df_plt['P'],xerr=df_plt['T_err'],yerr=df_plt['P_err'],fmt='o', color='r', ecolor='k', capthick=1)
    plt.grid()
    plt.xlabel("T ({})".format(units['T']))
    plt.ylabel("P ({})".format(units['P']))
    plt.title('P-T of all experiments')
    plt.show()

    reactions = df_plt.groupby('rxn_studied')
    if units['T_err']=='%':
        df_plt['T_err'] = df_plt['T_err']*df_plt['T']/100
    if units['P_err']=='%':
        df_plt['P_err'] = df_plt['P_err']*df_plt['P']/100
    for name, reaction in reactions:
        fig, axes = plt.subplots(1, 2, figsize=(10,4))
        fig.suptitle(name)
        results = reaction.groupby('rxn_direction')
        style={}
        style['BOTH']={}
        style['BOTH']['c']='go'
        style['BOTH']['mec']='g'
        style['BIASED']={}
        style['BIASED']['c']='c*'
        style['BIASED']['mec']='c'
        style['NC']={}
        style['NC']['c']='ko'
        style['NC']['mec']='k'
        style['FWD']={}
        style['FWD']['c']='ro'
        style['FWD']['mec']='r'
        style['FWD?']={}
        style['FWD?']['c']='wo'
        style['FWD?']['mec']='r'
        style['REV']={}
        style['REV']['c']='bo'
        style['REV']['mec']='b'
        style['REV?']={}
        style['REV?']['c']='wo'
        style['REV?']['mec']='b'
        for cat, result in results:
            # trust_mask = result['trust_conditions']=='Yes'
            # axes[0].errorbar(result.loc[trust_mask,'T'],
            #                  result.loc[trust_mask,'P'],
            #                  xerr=result.loc[trust_mask,'T_err'],
            #                  yerr=result.loc[trust_mask,'P_err'],
            #                  fmt=style[cat]['c'],
            #                  mec=style[cat]['mec'], ecolor='k',
            #                  capthick=1, label=cat)
            axes[0].errorbar(result['T'],
                             result['P'],
                             xerr=result['T_err'],
                             yerr=result['P_err'],
                             fmt=style[cat]['c'],
                             mec=style[cat]['mec'], ecolor='k',
                             capthick=1, label=cat)
            # axes[0].plot()
        axes[0].set_xlabel("T ({})".format(units['T']))
        axes[0].set_ylabel("P ({})".format(units['P']))
        axes[0].legend()
        try:
            reaction.plot(y='equil_time', ax=axes[1], style='ko', legend=False, grid=True)
        except TypeError:
            print ("Cannot construct equilibrium time plot - no numeric values provided")
        axes[1].set_xlabel("experiment index")
        axes[1].set_ylabel("duration ({})".format(units['equil_time']))
        plt.show()
    pass

def test_and_view(filename, path):
    run_tests(filename, path)
    visual_validation(filename, path)
    pass

def test_sheet_starter_phase(df, input_format):
    test = 'pass'
    return test

def test_sheet_exp_cond(df, input_format):
    test = 'pass'
    for i,Pi in enumerate(df['exp_conditions']['P']):
        if type(Pi)==str:
            if re.fullmatch(r'{}'.format(input_format['P']),Pi) == None:
                test = 'fail'
                print('exp_conditions.P[{}] ({}) is in the incorrect format. must have one of the following forms:\
                \n  P, Pmin <-> Pmax, <Pmax, >Pmin, ~P_approx, "no_info"\n'.format(i,Pi))
    for i,P_erri in enumerate(df['exp_conditions']['P_err']):
        if type(P_erri)==str:
            if re.fullmatch(r'{}'.format(input_format['P_err']),P_erri) == None:
                test = 'fail'
                print('exp_conditions.P_err[{}] ({}) is in the incorrect format. must have one of the following forms:\
                \n  P_err, P_errmin <-> P_errmax, <P_errmax, >P_errmin, ~P_err_approx, "no_info"\n'.format(i,P_erri))
    for i,Ti in enumerate(df['exp_conditions']['T']):
        if type(Ti)==str:
            if re.fullmatch(r'{}'.format(input_format['T']),Ti) == None:
                test = 'fail'
                print('exp_conditions.T[{}] ({}) is in the incorrect format. must have one of the following forms:\
                \n  T, Tmin <-> Tmax, <Tmax, >Tmin, ~T_approx, "no_info"\n'.format(i,Ti))
    for i,T_erri in enumerate(df['exp_conditions']['T_err']):
        if type(T_erri)==str:
            if re.fullmatch(r'{}'.format(input_format['T_err']),T_erri) == None:
                test = 'fail'
                print('exp_conditions.T_err[{}] ({}) is in the incorrect format. must have one of the following forms:\
                \n  T_err, T_errmin <-> T_errmax, <T_errmax, >T_errmin, ~T_err_approx, "no_info"\n'.format(i,T_erri))
    for i,ti in enumerate(df['exp_conditions']['equil_time']):
        if type(ti)==str:
            if re.fullmatch(r'{}'.format(input_format['equil_time']),ti) == None:
                test = 'fail'
                print('exp_conditions.equil_time[{}] ({}) is in the incorrect format. Must have one of the following forms:\
                \n t, tmin <-> tmax, <tmax, >tmin, ~t_approx, "no_info"\n'.format(i,ti))
    return test

def test_sheet_rxn(df, input_format):
    test = 'pass'
    for i,rxni in enumerate(df['rxn']['rxn_studied']):
        if type(rxni)==str:
            irxn_list = [x.strip() for x in rxni.split(';')]
            for rxnij in irxn_list:
                if re.fullmatch(r'{}'.format(input_format['rxn']),rxnij) == None:
                    test = 'fail'
                    print('rxn.rxn[{}] ({}) is in the incorrect format. Must have form:\
                    \n phase (+ phases) = phase (+ phases))\n For multiple reactions, list each reaction separated by "; "'.format(i,rxni))
    for i,valuei in enumerate(df['rxn']['rxn_metric_final']):
        if type(valuei)==str:
            if re.fullmatch(r'{}'.format(input_format['rxn_metric_final']),valuei) ==None:
                test = 'fail'
                print('rxn.rxn_metric_final[{}] ({}) is in the incorrect format. Must have one of the following forms:\
                \n value, value_min <-> value_max, <value_max, >value_min, ~value_approx, "no_info"\n'.format(i,valuei))
    for i,valuei in enumerate(df['rxn']['rxn_metric_init']):
        if type(valuei)==str:
            if re.fullmatch(r'{}'.format(input_format['rxn_metric_init']),valuei) ==None:
                test = 'fail'
                print('rxn.rxn_metric_init[{}] ({}) is in the incorrect format. Must have one of the following forms:\
                \n value, value_min <-> value_max, <value_max, >value_min, ~value_approx, "no_info"\n'.format(i,valuei))
    for i,erri in enumerate(df['rxn']['rxn_metric_err']):
        if type(erri)==str:
            if re.fullmatch(r'{}'.format(input_format['rxn_metric_err']),erri) ==None:
                test = 'fail'
                print('rxn.rxn_metric_err[{}] ({}) is in the incorrect format. Must have one of the following forms:\
                \n value, value_min <-> value_max, <value_max, >value_min, ~value_approx, "no_info"\n'.format(i,erri))
    for i,resulti in enumerate(df['rxn']['results']):
        if type(resulti)==str:
            if re.fullmatch(r'{}'.format(input_format['results']),resulti) ==None:
                test = 'fail'
                print('rxn.result[{}] "{}" is in the incorrect format. Must have the same form as the following examples:\
                \n "Tlc +; Fo +; En -"\
                \n "Tlc -?; Fo -?; En +?"\
                \n where there is a space between the phase and its result (+; +?; =; =?; -?; -) followed by a semicolon and another space. Do not forget the spaces!\n'.format(i,resulti))
    return test

def test_sheet_sample_material(df, input_format):
    test = 'pass'
    return test

# FIXME: add for loop for cases where there are >1 reaction
def get_rxn_direction(rxn_str, results, rxn_direction_certainty=None):
    # get reacting phases and phase-wise reaction directions
    rxn_phases, rxn_dir = get_rxn_phases(rxn_str, get_rxn_dir=True)
    phase_changes = results.split(';')
    rxn_outcome = 'BOTH'

    # split up results into reported phases, whether they increase/decrease/remain
    # constant, and the certainty of the changes
    def parse_results(phase_changes):
        result_phases = []
        result_sign = []
        result_certainty = []

        for term in phase_changes:
            format_fail = False
            split_term = term.split()
            try:
                result_phases.append(str(split_term[-2]))
            except:
                format_fail = True
                result_phases.append(None)

            if format_fail:
                result_sign.append(None)
                result_certainty.append(None)
            else:
                try:
                    change_sign = split_term[-1]
                    if change_sign[-1] =='?':
                        if change_sign[:-1]=='+':
                            result_sign.append(1)
                        elif change_sign[:-1]=='-':
                            result_sign.append(-1)
                        else:
                            result_sign.append(0)
                        result_certainty.extend('?')
                    else:
                        result_certainty.extend('!')
                        if change_sign=='+':
                            result_sign.append(1)
                        elif change_sign=='-':
                            result_sign.append(-1)
                        else:
                            result_sign.append(0)
                except:
                    result_sign.append(None)
                    result_certainty.extend(None)

        return result_phases, result_sign, result_certainty

    result_phases, result_sign, result_certainty = parse_results(phase_changes)

    # Manually set Error result if format invalid
    if (None in result_phases):
        rxn_outcome = 'ERROR'
        return  rxn_outcome
    # assert False
    # get indices of reported result_phases that are involved in the reaction
    indices = [i for i, phase in enumerate(result_phases) if phase in rxn_phases]
    # determine what side of the reaction each of these phases are
    side_of_rxn = []
    for ind in indices:
        ind_result_phs = result_phases[ind]
        ind_rxn_dir = list(rxn_phases).index(ind_result_phs)
        side_of_rxn.append(rxn_dir[ind_rxn_dir])
    # reduce results to the phases involved in the reaction
    phases = np.array(result_phases)[indices]
    direc = np.array(result_sign)[indices]*np.array(side_of_rxn)
    certainty = np.array(result_certainty)[indices]

    # Create 3x2 matrix to identify possible reaction outcome and populate for
    # each phase

    result_matrix = np.zeros((3,2))

    for i in range(len(phases)):
        if direc[i] == -1:
            x = 0
        elif direc[i] == 0:
            x = 1
        else:
            x = 2
        if certainty[i] == '?':
            y = 1
        else:
            y = 0
        result_matrix[x,y] += 1


    # Use result matrix to categorize reaction outcome
    if np.all(result_matrix[0]==0) and np.all(result_matrix[2]==0):
        rxn_outcome = 'NC'
    if np.all(result_matrix[0]==0) and result_matrix[2,0] > 0:
        rxn_outcome = 'FWD'
    if np.all(result_matrix[2]==0) and result_matrix[0,0] > 0:
        rxn_outcome = 'REV'
    if np.all(result_matrix[0]==0) and result_matrix[2,0]==0 and result_matrix[2,1] > 0:
        rxn_outcome = 'FWD?'
    if result_matrix[0,0]==0 and result_matrix[0,1] > 0 and result_matrix[2,0] > 0:
        rxn_outcome = 'FWD?'
    if np.all(result_matrix[2]==0) and result_matrix[0,0]==0 and result_matrix[0,1] > 0:
        rxn_outcome = 'REV?'
    if result_matrix[2,0]==0 and result_matrix[0,0] > 0 and result_matrix[2,1] > 0:
        rxn_outcome = 'REV?'

    # rxn_direction_certainty has final say on the certainty level of the rxn
    if rxn_direction_certainty is not None:
        if rxn_direction_certainty == 'biased':
            rxn_outcome = 'BIASED'
        elif rxn_direction_certainty == 'unresolved':
            rxn_outcome = 'NC'
        elif rxn_direction_certainty == 'probable':
            if rxn_outcome == 'FWD':
                rxn_outcome = 'FWD?'
            elif rxn_outcome == 'REV':
                rxn_outcome = 'REV?'

    return rxn_outcome

def lookup_pure_rxns():
    rxn_papers = pd.read_csv(APP_DATA_DIR+'pure-phase-rxn-papers.csv')
    rxn_papers.dropna(inplace=True)

    pub_rxns = {}
    pub_phases = {}

    for ind, (pubID, rxn) in enumerate(
        zip(rxn_papers['pubID'], rxn_papers['rxn'])):

        phases = get_rxn_phases(rxn)
        # print (ind+2, ' ', pubID, ' : ', dataio.get_rxn_phases(rxn))
        if pubID not in pub_rxns:
            pub_rxns[pubID] = list()
            pub_phases[pubID] = set()

            pub_rxns[pubID].append(rxn)
            pub_phases[pubID].update(phases)

    all_rxns = set()
    all_phases = set()
    for pub in pub_rxns:
        rxns = pub_rxns[pub]
        phases = pub_phases[pub]
        all_rxns.update(rxns)
        all_phases.update(phases)

    # all_rxns = np.unique([pub_rxns[pub] for pub in pub_rxns])
    # all_phases = np.unique([list(pub_phases[pub]) for pub in pub_phases])

    rxn_present = {}
    for rxn in all_rxns:
        rxn_present[rxn] = set()
        for pubID in pub_rxns:
            if rxn in pub_rxns[pubID]:
                rxn_present[rxn].add(pubID)

    phase_present = {}
    for phase in all_phases:
        phase_present[phase] = []
        for pubID in pub_phases:
            if phase in pub_phases[pubID]:
                phase_present[phase].append(pubID)

    phase_in_rxn = {}
    for phase in all_phases:
        phase_in_rxn[phase] = []
        for rxn in all_rxns:
            if phase in rxn:
                phase_in_rxn[phase].append(rxn)

    rxn_data = {}
    rxn_data['pub_rxns'] = pub_rxns
    rxn_data['pub_phases'] = pub_phases
    rxn_data['rxn_present'] = rxn_present
    rxn_data['phase_present'] = phase_present
    rxn_data['phase_in_rxn'] = phase_in_rxn
    rxn_data['all_phases'] = all_phases
    rxn_data['all_rxns'] = all_rxns


    return rxn_data
