"""
This module tests the thermodat package.

"""
from __future__ import absolute_import, print_function, division, with_statement
from builtins import object

from os import path
import numpy as np
import pandas as pd
import yaml
from collections import OrderedDict
import copy

import thermodat

from pandas.util.testing import assert_frame_equal

TESTDIR='test'
TESTDATADIR= TESTDIR+'/'+'test_data'

class TestIO():
    def test_json_storage(self):
        xls_filename = TESTDATADIR+'/phase-rxn-test.xlsx'

        basename, extname = path.splitext(xls_filename)
        json_filename = basename+'.json'
        xls_write_filename = basename+'-write.xlsx'

        thermodat.xls_to_json(xls_filename)
        thermodat.json_to_xls(json_filename, xls_filename=xls_write_filename)

        df_book = pd.read_excel(xls_filename, sheetname=None)
        df_book_write = pd.read_excel(xls_write_filename, sheetname=None)

        [assert_frame_equal(df_book[sheet],
                            df_book_write[sheet]) for sheet in df_book]

    def test_dataframe_compare(self):
        xls_filename = TESTDATADIR+'/phase-rxn-test.xlsx'
        df_book = pd.read_excel(xls_filename, sheetname=None)
        df_book_cp = copy.deepcopy(df_book)

        [assert_frame_equal(df_book[sheet],
                            df_book_cp[sheet]) for sheet in df_book]

        # Force a caught AssertionError by changing a value
        df_book_cp['rxn'].loc[3,'results']='OVERWRITTEN-VALUE'
        try:
            [assert_frame_equal(df_book[sheet],
                                df_book_cp[sheet]) for sheet in df_book]
        except AssertionError: # Catch purposeful error
            pass
        else:
            raise AssertionError(
                'Manual change to DataFrame uncaught by DataFrame equality test.')
        pass



