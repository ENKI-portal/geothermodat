import numpy as np
import pandas as pd
import thermodat
from thermodat import dataio
from thermodat import gui
from thermodat.gui import ComplexWidget

import ipywidgets as widgets
from collections import OrderedDict
from IPython.display import Markdown, Latex, HTML
import subprocess
import os
from pathlib import Path

from abc import ABCMeta, abstractmethod

from thermodat.dataio import APP_DATA_DIR

#================================
class DataTemplateTool(ComplexWidget):
    def __init__(self, local_dir_name='ENKI-data', use_home_dir=True):
        self._init_dir_name(local_dir_name, use_home_dir)
        self._init_widget()
        self._init_events()

    def _init_dir_name(self, local_dir_name, use_home_dir):
        if use_home_dir:
            home_dir = str(Path.home())+'/'
        else:
            home_dir = ''

        data_dir = home_dir+local_dir_name+'/'

        if not os.path.exists(data_dir):
            os.makedirs(data_dir)

        self._dir_name = data_dir

    def _init_widget(self):
        panel = gui.DataTemplateDisplay()

        self._widget = panel._widget
        self._panel = panel

    def _init_events(self):
        panel = self._panel
        panel.ok_button.on_click(self.on_get_template)

    def on_get_template(self, b=None):
        print('enter on get template')
        base_template_filename = (
            APP_DATA_DIR+'phase-rxn-data-base-template.xlsx')
        panel = self._panel

        dir_name = self._dir_name
        filename = dir_name+panel.filename
        phase_names = panel.phase_names
        is_rxn_phase = panel.is_rxn_phase
        phase_abbrevs = panel.phase_abbrevs
        component_names = panel.component_names
        sample_names = panel.sample_names
        sample_ids = panel.sample_ids

        rxn_phases = np.array(phase_names)[
            np.array(panel.is_rxn_phase)].tolist()

        std_sheets, chem_sheet, units_lookup, rxn_phase_sheet = (
            dataio.read_rxn_base_template(base_template_filename))

        data_info = dataio.extract_data_info(
            component_names, rxn_phases,
            std_sheets, chem_sheet, units_lookup, rxn_phase_sheet)

        # phases = ['Liquid', 'Albite', 'Olivine']
        # phase_symbols = ['Liq', 'Ab', 'Ol']
        # sample_ids = ['Liq1','Ab1','Ol1']
        # sample_names = ['LiquidSample1', 'AlbiteSample1','OlivineSample1']

        wb = dataio.write_excel_workbook(filename, data_info)
        dataio.fill_phase_sample_info(phase_names, phase_abbrevs, sample_ids,
                                      sample_names, data_info, wb)

        panel = self._panel

        print_obj = ('The data template file has been written.\n'
                     'It can be found at "'+filename+'".\n'
                     'Download this file to your local computer to begin entering data.'
                     )


        panel._print_output(print_obj)

#================================
