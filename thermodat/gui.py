import numpy as np
import pandas as pd
import thermodat
from thermodat import dataio

# from future import print_function
import ipywidgets as widgets
from collections import OrderedDict
from IPython.display import Markdown, Latex, HTML
import subprocess
import os
from pathlib import Path

from abc import ABCMeta, abstractmethod

from thermodat.dataio import APP_DATA_DIR

#================================
class ComplexWidget(metaclass=ABCMeta):
    def __init__(self, output=None):
        self._output = output
        self._widget = None

    @property
    def widget(self):
        return self._widget

    def display(self):
        display(self.widget)

    def _print_output(self, print_obj):
        try:
            with self._outbox:
                print(print_obj)
        except:
            print(print_obj)

    def _display_output(self, display_obj):
        try:
            with self._outbox:
                display(display_obj)
        except:
            display(display_obj)

class CollectionWidget(ComplexWidget):
    def __init__(self):
        super().__init__()
        self._collection = []
        self._collection_panel = None

    def _delete_action(self, item_ind, item):
        return None

    def _add_action(self, item_ind, item):
        return None

    def _on_delete_item(self, b=None):
        item_ind = b.item_ind

        del_item = self._collection.pop(item_ind)
        del del_item

        widget_collection = []
        for item_ind, item in enumerate(self._collection):
            item._delete_button.item_ind = item_ind
            self._delete_action(item_ind, item)
            widget_collection.append(item.widget)

        self._collection_panel.children = widget_collection

    def _add_item(self, item, delete_button):
        self._collection.append(item)
        self._collection_panel.children += (item.widget,)

        item_ind = len(self._collection)-1

        delete_button.item_ind = item_ind
        delete_button.on_click(self._on_delete_item)

        item._delete_button = delete_button
        self._add_action(item_ind, item)
#================================
class DataTemplateDisplay(ComplexWidget):
    FILE_EXT = '.xlsx'
    def __init__(self):
        super().__init__()
        self._init_widget()
        self._init_events()

    def _init_events(self):
        phase_selector = self._phase_selector
        phase_selector.add_button.on_click(self.on_add_phase)

    def on_add_phase(self, button):
        phase_tabs = self._phase_tabs
        phase_selector = self._phase_selector

        phase_tabs.add_phase(phase_selector.name, phase_selector.abbrev,
                             phase_selector.starter_phase_default,
                             phase_selector.rxn_phase_default)

    def _init_widget(self):
        phase_selector = PhaseSelector()
        chem_panel = ChemPanel()
        phase_tabs = PhaseTabs()
        file_chooser = FileChooser()
        outbox = widgets.Output(layout={'border': '5px solid pink'})

        panel = widgets.VBox(children=[
            phase_selector.widget, phase_tabs.widget, chem_panel.widget,
            file_chooser.widget, outbox])

        self._widget = panel
        self._phase_selector = phase_selector
        self._chem_panel = chem_panel
        self._phase_tabs = phase_tabs
        self._file_chooser = file_chooser
        self._outbox = outbox

    @property
    def phase_names(self):
        phase_tabs = self._phase_tabs
        return phase_tabs.names

    @property
    def is_rxn_phase(self):
        phase_tabs = self._phase_tabs
        return phase_tabs.rxn_phases

    @property
    def phase_abbrevs(self):
        phase_tabs = self._phase_tabs
        return phase_tabs.abbrevs

    @property
    def sample_names(self):
        phase_names = self.phase_names
        sample_names = [name+'Sample1' for name in phase_names]
        return sample_names

    @property
    def sample_ids(self):
        phase_abbrevs = self.phase_abbrevs
        sample_ids = [abbrev+'1' for abbrev in phase_abbrevs]
        return sample_ids

    @property
    def component_names(self):
        chem_panel = self._chem_panel
        return chem_panel.names

    @property
    def filename(self):
        file_chooser = self._file_chooser
        filename = file_chooser.filename + self.FILE_EXT
        return filename

    @property
    def ok_button(self):
        file_chooser = self._file_chooser
        ok_button = file_chooser._ok_button
        return ok_button
#================================
class DataUpdateDisplay(ComplexWidget):
    FILE_EXT = '.xlsx'
    def __init__(self):
        super().__init__()
        self._init_widget()
        self._init_events()

    def _init_events(self):
        # phase_selector = self._phase_selector
        # phase_selector.add_button.on_click(self.on_add_phase)
        pass

    def on_add_phase(self, button):
        # phase_tabs = self._phase_tabs
        # phase_selector = self._phase_selector
        # phase_tabs.add_phase(phase_selector.name, phase_selector.abbrev,
        #                      phase_selector.starter_phase_default,
        #                      phase_selector.rxn_phase_default)
        pass

    def _init_widget(self):
        # phase_selector = PhaseSelector()
        # chem_panel = ChemPanel()
        # phase_tabs = PhaseTabs()
        file_chooser = FileChooser()
        outbox = widgets.Output(layout={'border': '5px solid pink'})

        # panel = widgets.VBox(children=[
        #     phase_selector.widget, phase_tabs.widget, chem_panel.widget,
        #     file_chooser.widget, outbox])

        panel = widgets.VBox(children=[file_chooser.widget, outbox])

        self._widget = panel
        # self._phase_selector = phase_selector
        # self._chem_panel = chem_panel
        # self._phase_tabs = phase_tabs
        self._file_chooser = file_chooser
        self._outbox = outbox

    @property
    def phase_names(self):
        phase_tabs = self._phase_tabs
        return phase_tabs.names

    @property
    def is_rxn_phase(self):
        phase_tabs = self._phase_tabs
        return phase_tabs.rxn_phases

    @property
    def phase_abbrevs(self):
        phase_tabs = self._phase_tabs
        return phase_tabs.abbrevs

    @property
    def sample_names(self):
        phase_names = self.phase_names
        sample_names = [name+'Sample1' for name in phase_names]
        return sample_names

    @property
    def sample_ids(self):
        phase_abbrevs = self.phase_abbrevs
        sample_ids = [abbrev+'1' for abbrev in phase_abbrevs]
        return sample_ids

    @property
    def component_names(self):
        chem_panel = self._chem_panel
        return chem_panel.names

    @property
    def filename(self):
        file_chooser = self._file_chooser
        filename = file_chooser.filename + self.FILE_EXT
        return filename

    @property
    def ok_button(self):
        file_chooser = self._file_chooser
        ok_button = file_chooser._ok_button
        return ok_button
#================================


#================================
class ChemComponent(ComplexWidget):
    def __init__(self):
        super().__init__()
        self._init_widget()
        self._init_events()

    def _init_events(self):
        self._chem_typ_select.observe(self.on_chem_typ_change)
        self._chem_typ_select.observe(self.on_update_name)
        self._as_total_select.observe(self.on_update_name)
        self._std_chem_entry.observe(self.on_update_name)
        self._custom_chem_entry.observe(self.on_update_name)

    def on_update_name(self, change):
        name = self.chem_entry.value
        if name is None:
            name = ''

        as_total_select = self._as_total_select

        if as_total_select.value=='total':
            name = name +'(tot)'

        self.name = name
        self._label.value = name

    def on_chem_typ_change(self, change):
        chem_typ_select = self._chem_typ_select

        std_chem_entry = self._std_chem_entry
        custom_chem_entry = self._custom_chem_entry

        chem_typ = chem_typ_select.value

        if chem_typ == 'oxide':
            # self._set_chem_typ(std_chem_entry)
            self.chem_entry = 'std'
            std_chem_entry.options = self.oxides
            std_chem_entry.value = self.oxides[0]

        elif chem_typ == 'element':
            self.chem_entry = 'std'
            std_chem_entry.options = self.elems
            std_chem_entry.value = self.elems[0]

        elif chem_typ == 'custom':
            self.chem_entry = 'custom'
            custom_chem_entry.value = ''

        else:
            print('not valid')

    def _init_widget(self):
        oxides = self.oxides
        chem_typ = self.chem_typ
        value_init = oxides[0]
        as_total = self.as_total

        delete_button = widgets.Button(
            icon='minus', layout=widgets.Layout(width='50px'))

        label = widgets.Label(
            value=value_init, layout=widgets.Layout(width='200px'))

        chem_typ_select = widgets.Dropdown(options=chem_typ)


        as_total_select = widgets.RadioButtons(options=as_total)

        std_chem_entry = widgets.Dropdown(options=oxides)
        custom_chem_entry = widgets.Text(
            value=value_init, layout=widgets.Layout())

        chem_entry = widgets.Box(children=[std_chem_entry])

        comp_panel = widgets.HBox(children=[
            delete_button, label, chem_typ_select,
            chem_entry, as_total_select])

        self._widget = comp_panel
        self._delete_button = delete_button
        self._label = label
        self._chem_typ_select = chem_typ_select
        self._std_chem_entry = std_chem_entry
        self._as_total_select = as_total_select
        self._custom_chem_entry = custom_chem_entry
        self._chem_entry = chem_entry

    @property
    def oxides(self):
        oxides = ['SiO2','TiO2','Al2O3','Fe2O3','Cr2O3','FeO','MnO',
                  'MgO','NiO','CoO','CaO','Na2O','K2O','P2O5','H2O','CO2']
        return oxides


    @property
    def elems(self):
        elems = [
            'H', 'He', 'Li', 'Be',  'B', 'C', 'N', 'O', 'F', 'Ne',
            'Na','Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca',
            'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu',
            'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y',
            'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
            'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr',
            'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm',
            'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au',
            'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac',
            'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es',
            'Fm', 'Md', 'No', 'Lr','Rf', 'Db', 'Sg' ]
        return elems

    @property
    def chem_typ(self):
        chem_typ = ['oxide','element','custom']
        return chem_typ

    @property
    def as_total(self):
        as_total = ['measured','total']
        return as_total

    @property
    def chem_entry(self):
        return self._chem_entry.children[0]

    @chem_entry.setter
    def chem_entry(self, value):
        if value=='std':
            self._chem_entry.children = [self._std_chem_entry]
        elif value=='custom':
            self._chem_entry.children = [self._custom_chem_entry]
        else:
            assert False, value+' is not a valid chem_entry type.'

    @property
    def name(self):
        return self._label.value

    @name.setter
    def name(self, value):
        self._label.value = value

    @property
    def delete_button(self):
        return self._delete_button

class ChemPanel(CollectionWidget):
    def __init__(self):
        super().__init__()
        self._init_widget()
        self._init_events()

    def _init_events(self):
        self._add_button.on_click(self.on_add_chem_comp)

    def on_add_chem_comp(self, b=None):
        ichem_comp = ChemComponent()
        self._add_item(ichem_comp, ichem_comp._delete_button)

    def _init_widget(self):
        add_button = widgets.Button(
            icon='plus', layout=widgets.Layout(width='50px'))

        title = widgets.Label(
            value='Chemical Components',
            layout=widgets.Layout(width='200px'))

        chem_comps = []
        chem_comp_panel = widgets.VBox(children=[])

        panel = widgets.VBox(children=[title, chem_comp_panel, add_button],
                             layout=widgets.Layout(border='solid gray 1px'))



        self._widget = panel
        self._add_button = add_button
        self._title = title
        self._chem_comps = chem_comps

        self._collection_panel = chem_comp_panel
        self.on_add_chem_comp()

    @property
    def names(self):
        names = []
        for ichem_comp in self._collection:
            names.append(ichem_comp.name)

        return names
#================================
class PhaseSelector(ComplexWidget):
    def __init__(self):
        super().__init__()
        self._load_phase_info()
        self._init_widget()
        self.on_phase_name_change()
        self._init_events()

    def _load_phase_info(self):
        common_phases = pd.read_csv(APP_DATA_DIR+'common-phases.csv')
        pure_phases = pd.read_csv(APP_DATA_DIR+'pure-phases.csv')
        soln_phases = pd.read_csv(APP_DATA_DIR+'solution-phases.csv')

        self._common_phases = common_phases
        self._pure_phases = pure_phases
        self._soln_phases = soln_phases

    def _init_events(self):

        self.on_phase_typ_change()

        self._phase_typ_select.observe(self.on_phase_typ_change)
        self._std_phase_name.observe(self.on_phase_name_change)
        self._custom_phase_name.observe(self.on_phase_name_change)

        # self.on_phase_name_change()

        # self.phase_name_entry.observe(self.on_phase_name_change)

        # self._c_select.observe(self.on_chem_typ_change)
        # self._chem_typ_select.observe(self.on_update_name)
        # self._as_total_select.observe(self.on_update_name)
        # self._std_chem_entry.observe(self.on_update_name)
        # self._custom_chem_entry.observe(self.on_update_name)

    def on_phase_name_change(self, change=None):
        name = self.phase_name_entry.value

        abbrev_lookup = self.abbrev_lookup

        try:
            abbrev = abbrev_lookup[name]
        except:
            abbrev = '<'+name+'>'

        self.name = name
        self.abbrev = abbrev

    def on_phase_typ_change(self, change=None):
        phase_typ_select = self._phase_typ_select
        std_phase_name = self._std_phase_name
        custom_phase_name = self._custom_phase_name

        phase_typ = phase_typ_select.value

        if phase_typ == 'common-phase':
            self.phase_name_entry = 'std'
            std_phase_name.options = self._common_phases['name']
            self.set_abbrev_lookup(self._common_phases['name'],
                                   self._common_phases['symbol'])

        elif phase_typ == 'pure-phase':
            self.phase_name_entry = 'std'
            std_phase_name.options = self._pure_phases['name']
            self.set_abbrev_lookup(self._pure_phases['name'],
                                   self._pure_phases['symbol'])

        elif phase_typ == 'solution-phase':
            self.phase_name_entry = 'std'
            std_phase_name.options = self._soln_phases['name']
            self.set_abbrev_lookup(self._soln_phases['name'],
                                   self._soln_phases['symbol'])

        elif phase_typ == 'custom-phase':
            self.phase_name_entry = 'custom'
            custom_phase_name.value = ''
            self.set_abbrev_lookup(None)

        else:
            print('not valid')

        self.on_phase_name_change()

        # print(self.phase_name_entry.value)
        # print(phase_typ_select.value)

    def set_abbrev_lookup(self, names, abbrevs=None, update_phase=True):
        if names is None:
            lookup = None
        else:
            lookup = {}
            for name, abbrev in zip(names, abbrevs):
                lookup[name] = abbrev

            if update_phase:
                # display(names)
                self.phase_name_entry.value = names[0]

        self._abbrev_lookup = lookup

    def _init_widget(self):
        phase_typ_select = widgets.Dropdown(
            options=self.phase_types, layout=widgets.Layout(width='150px'))

        name_options = self._common_phases['name']
        abbrev_options = self._common_phases['symbol']
        self.set_abbrev_lookup(name_options, abbrev_options, update_phase=False)


        std_phase_name = widgets.Dropdown(
            options=self._common_phases['name'],
            layout=widgets.Layout(width='300px'))

        custom_phase_name = widgets.Text(
            value='', layout=widgets.Layout())


        phase_name_entry = widgets.Box(children=[std_phase_name])

        add_button = widgets.Button(description='Add-Phase')

        panel = widgets.HBox(children=[phase_typ_select, phase_name_entry,
                                       add_button])

        self._widget = panel
        self._add_button = add_button
        self._phase_typ_select = phase_typ_select
        self._phase_name_entry = phase_name_entry
        self._std_phase_name = std_phase_name
        self._custom_phase_name = custom_phase_name

        # self._name_options

    @property
    def phase_types(self):
        phase_types = ['common-phase','pure-phase','solution-phase','custom-phase']
        return phase_types

    @property
    def phase_name_entry(self):
        return self._phase_name_entry.children[0]

    @phase_name_entry.setter
    def phase_name_entry(self, value):
        if value=='std':
            self._phase_name_entry.children = [self._std_phase_name]
        elif value=='custom':
            self._phase_name_entry.children = [self._custom_phase_name]
        else:
            assert False, value+' is not a valid chem_entry type.'

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def abbrev(self):
        return self._abbrev

    @abbrev.setter
    def abbrev(self, value):
        self._abbrev = value

    @property
    def abbrev_lookup(self):
        return self._abbrev_lookup

    @property
    def add_button(self):
        return self._add_button

    @property
    def starter_phase_default(self):
        phase_typ_select = self._phase_typ_select
        phase_typ = phase_typ_select.value

        if phase_typ == 'common-phase':
            starter_phase_default = True

        elif phase_typ == 'pure-phase':
            starter_phase_default = True

        elif phase_typ == 'solution-phase':
            starter_phase_default = False

        elif phase_typ == 'custom-phase':
            starter_phase_default = True

        else:
            print('not valid')

        return starter_phase_default

    @property
    def rxn_phase_default(self):
        phase_typ_select = self._phase_typ_select
        phase_typ = phase_typ_select.value

        if phase_typ == 'common-phase':
            rxn_phase_default = True

        elif phase_typ == 'pure-phase':
            rxn_phase_default = False

        elif phase_typ == 'solution-phase':
            rxn_phase_default = True

        elif phase_typ == 'custom-phase':
            rxn_phase_default = True

        else:
            print('not valid')

        return rxn_phase_default

class PhaseProps(ComplexWidget):
    def __init__(self, name='Test-name', abbrev='abbrev',
                 is_starter_phase=False, is_rxn_phase=False):
        super().__init__()
        self._init_widget(name, abbrev, is_starter_phase, is_rxn_phase)
        self._init_events()

    def _init_events(self):
        # self._del_button.on_click(self.on_add_chem_comp)
        pass


    def _init_widget(self, name, abbrev, is_starter_phase, is_rxn_phase):
        label = widgets.Label(value=name)
        starter_phase_box = widgets.Checkbox(value=is_starter_phase, description='starter_phase')
        rxn_phase_box = widgets.Checkbox(value=is_rxn_phase, description='rxn_phase')
        del_button = widgets.Button(description='Delete Phase')
        box_props = widgets.HBox(children=[starter_phase_box, rxn_phase_box])
        panel = widgets.VBox(
            children=[label, box_props, del_button]
        )
        # layout=widgets.Layout(align_content='flex-start')

        self._name = name
        self._abbrev = abbrev
        self._widget = panel
        self._del_button = del_button

        self._starter_phase_box = starter_phase_box
        self._rxn_phase_box = rxn_phase_box

    @property
    def name(self):
        return self._name

    @property
    def abbrev(self):
        return self._abbrev

    @property
    def del_button(self):
        return self._del_button

    @property
    def is_starter_phase(self):
        return self._starter_phase_box.value

    @property
    def is_rxn_phase(self):
        return self._rxn_phase_box.value

class PhaseTabs(CollectionWidget):
    def __init__(self):
        super().__init__()
        self._init_widget()

    def _init_widget(self):
        panel = widgets.Tab()

        self._widget = panel
        self._collection_panel = panel

    def _delete_action(self, item_ind, item):
        tabs = self._collection_panel
        tabs.set_title(item_ind, item.abbrev)

    def _add_action(self, item_ind, item):
        tabs = self._collection_panel
        tabs.set_title(item_ind, item.abbrev)
        tabs.selected_index = item_ind

    def on_del_phase(self, b=None):
        tabs = self._widget
        ind_del = b.ind
        # print('index value = ', ind_del)

        del_phase_tab = tabs.children.pop(ind_del)
        # del_chem_comp = self._chem_comps.pop(ind_del)
        del del_phase_tab

    def add_phase(self, name, abbrev, is_starter_phase, is_rxn_phase):
        iphase_props = PhaseProps(
            name=name, abbrev=abbrev, is_starter_phase=is_starter_phase,
            is_rxn_phase=is_rxn_phase)
        self._add_item(iphase_props, iphase_props._del_button)

        # tabs = self._collection_panel
        # tab_num = len(tabs.children)
        # tabs.set_title(tab_num-1, abbrev)

    @property
    def names(self):
        names = []
        for item in self._collection:
            names.append(item.name)

        return names

    @property
    def abbrevs(self):
        abbrevs = []
        for item in self._collection:
            abbrevs.append(item.abbrev)

        return abbrevs

    @property
    def starter_phases(self):
        starter_phases = []
        for item in self._collection:
            starter_phases.append(item.is_starter_phase)

        return starter_phases

    @property
    def rxn_phases(self):
        rxn_phases = []
        for item in self._collection:
            rxn_phases.append(item.is_rxn_phase)

        return rxn_phases
#================================
class FileChooser(ComplexWidget):
    def __init__(self):
        super().__init__()
        self._init_widget()

    def _init_widget(self):
        label = widgets.Label(value='Filename:')
        filename_entry = widgets.Text(value='phase-rxn-data')
        ok_button = widgets.Button(description='OK')

        panel = widgets.HBox(children=[label, filename_entry, ok_button])

        self._widget = panel
        self._filename_entry = filename_entry
        self._ok_button = ok_button

    @property
    def filename(self):
        filename = self._filename_entry.value
        return filename
#================================

def _print_output(print_obj, outbox):
    try:
        with outbox:
            print(print_obj)
    except:
        print(print_obj)

def _display_output(display_obj, outbox):
    try:
        with outbox:
            display(display_obj)
    except:
        display(display_obj)

def _get_data_template_dir_name(local_dir_name, use_home_dir):
    if use_home_dir:
        home_dir = str(Path.home())+'/'
    else:
        home_dir = ''

    data_dir = home_dir+local_dir_name+'/'

    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    return data_dir

def create_data_template(local_dir_name='ENKI-data', use_home_dir=True):
    """
    GUI interface for generating excel data template
    """

    data_dir = _get_data_template_dir_name(local_dir_name, use_home_dir)

    outbox = widgets.Output(layout={'border': '5px solid pink'})

    menu_data = set_phase_chem(show_gui=False, outbox=outbox)

    template_data = create_excel_template(menu_data, show_gui=False,
                                          data_dir=data_dir, outbox=outbox)

    # [TK]

    chem_block = create_chem_block()

    Box = widgets.VBox([menu_data['menu'], chem_block['menu'],
                        template_data['menu'], outbox])
    display(Box)

    pass

def validate_submit_data(dirname='my-data'):
    _display_available_data_files(dirname=dirname, ext='.xlsx')


    display(Markdown('### Enter Filepath for Submission'))
    display(Markdown('* NOTE that you can copy/paste any of the available files listed above.'))
    display(Markdown('* *If your current data file is not in the list above*, be sure that you have uploaded it to the ENKI-Portal using the upload button on the main ENKI-Portal File Browser window.'))

    file_menu = select_data_file()
    format_test_menu = run_data_format_test(file_menu)
    visual_validation_menu = run_data_visual_validation(
        file_menu, format_test_menu)

    submit_menu = run_submit_data(file_menu, format_test_menu,
                                  visual_validation_menu)

    Box = widgets.VBox([file_menu['menu'], format_test_menu['menu'],
                        visual_validation_menu['menu'], submit_menu['menu']])
    display(Box)

    pass

def _display_available_data_files(dirname='my-data', ext='.xlsx'):
    filepath = os.path.join(dirname, '*'+ext)
    cmd = 'ls '+filepath
    cmd_output = subprocess.run(cmd, shell=True, encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    display(Markdown('### Available Excel Data Files'))
    print(cmd_output.stdout)
    pass

def select_data_file(show_gui=False):
    file_menu = OrderedDict()

    file_menu['file_title'] = widgets.Label(value='Data File Type:', layout=widgets.Layout(width='150px'))

    file_menu['file_type'] = widgets.Dropdown(
        options=['phase-rxn','eos'], layout=widgets.Layout(width='150px'))

    file_menu['file_name'] = widgets.Text(
        value='', layout=widgets.Layout(width='450px'))

    file_menu['output'] = widgets.Output(description='Load Data')

    def read_data_file(b, file_menu=file_menu):
        # path = dirname + file_menu['file_name'].value
        path = file_menu['file_name'].value
        file_menu['filepath'] = path

        try:
            with file_menu['output']:
                data = dataio.read_phase_rxn_data(path)

        except FileNotFoundError as err:
            with file_menu['output']:
                display(Markdown('**FileNotFoundError**'))
                display(Markdown('* That file cannot be found. Be sure that you have uploaded your file to the ENKI server.'))
                display(Markdown('* You can copy/paste the path for your file from the list above to be sure there are no typos.'))
                raise err

        file_menu['data'] = data

        # summary_data = dataio.get_summary_data(data)
        # file_menu['summary_data'] = summary_data
        with file_menu['output']:
            display(data['df'])

        # display(summary_data['df'])
        pass

    file_menu['load_button'] = widgets.Button(description='Load')
    file_menu['load_button'].on_click(read_data_file)

    HBox = widgets.HBox([file_menu['file_title'], file_menu['file_type'],
                         file_menu['file_name'], file_menu['load_button']])

    VBox = widgets.VBox([HBox, file_menu['output']])

    file_menu['menu'] = VBox

    if show_gui:
        display(file_menu['menu'])

    return file_menu

def run_data_format_test(file_menu, show_gui=False):
    format_test_menu = OrderedDict()

    def run_test(b, file_menu=file_menu, format_test_menu=format_test_menu):
        data = file_menu['data']
        with format_test_menu['output']:
            has_errors = dataio.run_tests(data['data_dict'])
            format_test_menu['has_errors'] = has_errors

        # has_errors = dataio.run_tests(data)
        # format_test_menu['has_errors'] = has_errors

        pass

    format_test_menu['test_button'] = widgets.Button(description='Test Format')
    format_test_menu['test_button'].on_click(run_test)

    format_test_menu['output'] = widgets.Output(description='Test Format')

    Box = widgets.VBox([format_test_menu['test_button'],
                        format_test_menu['output']])

    format_test_menu['menu'] = Box

    if show_gui:
        display(format_test_menu['menu'])

    return format_test_menu

def run_data_visual_validation(file_menu, format_test_menu, show_gui=False):
    visual_validation_menu = OrderedDict()

    def run_validation(b, file_menu=file_menu,
                       format_test_menu=format_test_menu):
        data = file_menu['data']
        with visual_validation_menu['output']:
            display(dataio.visual_validation(data))


        pass

    visual_validation_menu['test_button'] = widgets.Button(description='Plot Data')
    visual_validation_menu['test_button'].on_click(run_validation)

    visual_validation_menu['output'] = widgets.Output(description='Plot Data')

    Box = widgets.VBox([visual_validation_menu['test_button'],
                        visual_validation_menu['output']])

    visual_validation_menu['menu'] = Box

    if show_gui:
        display(visual_validation_menu['menu'])

    return visual_validation_menu

def run_submit_data(file_menu, format_test_menu,
                    visual_validation_menu,
                    destination='/Users/dataczar/data/01-submitted',
                    show_gui=False):

    submit_menu = OrderedDict()

    def get_submit_path(filepath, destination=destination):
        output = subprocess.run('echo $USER', shell=True,
                                encoding='utf-8', stdout=subprocess.PIPE)
        username = output.stdout[:-1]

        basename = os.path.basename(filepath)
        dirname = os.path.dirname(filepath)
        basename_root, basename_ext = os.path.splitext(basename)

        new_basename = basename_root+'_'+username+basename_ext
        submit_path = os.path.join(destination,new_basename)

        return submit_path



    def run_submit(b, file_menu=file_menu,
                   format_test_menu=format_test_menu,
                   visual_validation_menu=visual_validation_menu):
        filepath = file_menu['filepath']
        has_errors = format_test_menu['has_errors']
        submit_path = get_submit_path(filepath)

        if has_errors:

            display(Markdown('**There are still errors present in this data file.**'))
            display(Markdown('Please correct them as indicated above and try validating again.'))
            display(Markdown('*If you believe this warning is incorrect, please notify Aaron Wolf so this potential bug can be investigated and fixed.*'))


        else:
            cmd = 'chmod 770 '+filepath+'; cp '+filepath+' '+submit_path
            # print(cmd)

            cmd_output = subprocess.run(cmd, shell=True, encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            # display(cmd_output)
            if cmd_output.returncode==0:
                display(Markdown('**Submission Successful.** Thank you for your contribution!'))
                display(Markdown('**Please [CLICK HERE](https://gitlab.com/ENKI-portal/geothermodat/issues) to go to the gitlab issues page and close out the issue for this publication** This is crucial for tracking our progress.'))
                display(Markdown("**Now that you're on a roll, which paper will you tackle next?** *Be sure to assign yourself to the corresponding issue so that we don't duplicate efforts*"))
            else:
                display(Markdown('**Submission Failed.** This is a problem with the ENKI server. Please contact Aaron Wolf or Mark Ghiorso so that we can fix it ASAP. *Be sure to include the error messages below.*'))

                print('---')
                print('cmd:')
                print(cmd)
                print('---')
                print('stdout:')
                print(cmd_output.stdout)
                print('---')
                print('stderr:')
                print(cmd_output.stderr)
                print('---')

        pass

    submit_menu['submit_button'] = widgets.Button(description='Submit')
    submit_menu['submit_button'].on_click(run_submit)

    submit_menu['menu'] = widgets.VBox([submit_menu['submit_button']])

    if show_gui:
        display(submit_menu['menu'])

    return submit_menu

def select_publication():
    rxn_data = dataio.lookup_pure_rxns()
    rxn_data['phase_present']

    menu_data = OrderedDict()

    menu_data['rxn_data'] = rxn_data

    pure_phase_data = pd.read_csv(APP_DATA_DIR+'pure-phases.csv')
    all_names = pure_phase_data['name']
    all_symbols = pure_phase_data['symbol']

    # valid_pure_phases = {}
    valid_names = []
    valid_symbols = []
    for symbol, name in zip(all_symbols, all_names):
        if symbol in rxn_data['phase_in_rxn']:
            # valid_pure_phases[symbol] = name
            valid_symbols.append(symbol)
            valid_names.append(name)

    valid_names = np.array(valid_names)
    valid_symbols = np.array(valid_symbols)

    # menu_data['valid_pure_phases'] = valid_pure_phases
    menu_data['valid_names'] = valid_names
    menu_data['valid_symbols'] = valid_symbols

    menu_data['all_names'] = all_names
    menu_data['all_symbols'] = all_symbols

    menu_data['phs_dropdown'] = widgets.Dropdown(
        options=valid_names, layout=widgets.Layout(width='250px'))

    rxn_list = rxn_data['phase_in_rxn'][valid_symbols[0]]
    menu_data['rxn_dropdown'] = widgets.Dropdown(
        options=rxn_list, layout=widgets.Layout(width='350px'))

    def on_change_phase(change):
        phase_name = menu_data['phs_dropdown'].value
        rxn_data = menu_data['rxn_data']
        valid_names = menu_data['valid_names']
        valid_symbols = menu_data['valid_symbols']

        ind = np.where(valid_names==phase_name)[0][0]
        symbol = valid_symbols[ind]

        phase_in_rxn = menu_data['rxn_data']['phase_in_rxn']
        rxn_opts = phase_in_rxn[symbol]
        menu_data['rxn_dropdown'].options = rxn_opts
        menu_data['rxn_dropdown'].value = rxn_opts[0]

        on_change_rxn(0)

    def on_change_rxn(change):
        rxn = menu_data['rxn_dropdown'].value
        rxn_data = menu_data['rxn_data']
        pub_set = list(rxn_data['rxn_present'][rxn])
        pub_displays = []
        for pub in pub_set:
            pub_displays.append(get_pub_display(pub))

        menu_data['pub_display_panel'].children = pub_displays

    def get_pub_display(pub):
        label = widgets.Label(value=pub, layout=widgets.Layout(width='200px'))
        download_button = widgets.Button(description='Download')
        pub_display = widgets.HBox(children=[label,download_button])

        def copy_pdf(b, pub=pub):
            filename = pub+'.pdf'
            filepath = '/Users/Shared/CommunityFiles/pdfs/'+filename
            sys_cmd = 'cp -R ' + filepath + ' pdfs/'+filename
            try:
                os.system(sys_cmd)
            except:
                print('File not found at ', filepath)


        download_button.on_click(copy_pdf)
        return pub_display



    menu_data['phs_dropdown'].observe(on_change_phase)
    menu_data['rxn_dropdown'].observe(on_change_rxn)

    phs_rxn_select = widgets.HBox([menu_data['phs_dropdown'],
                                   menu_data['rxn_dropdown']])

    pub_label = widgets.Label(value='Publications:',
                              layout=widgets.Layout(width='200px'))
    pub_display_panel = widgets.VBox(children = [])
    menu_data['pub_display_panel'] = pub_display_panel
    Box = widgets.VBox(children=[phs_rxn_select, pub_label, pub_display_panel])


    on_change_rxn(0)
    display(Box)

    pass

def download_Berman1988():
    pub = 'Berman1988'
    label = widgets.Label(value=pub, layout=widgets.Layout(width='200px'))
    download_button = widgets.Button(description='Download')
    pub_display = widgets.HBox(children=[label,download_button])

    def copy_pdf(b, pub=pub):
        filename = pub+'.pdf'
        filepath = '/Users/Shared/CommunityFiles/pdfs/'+filename
        sys_cmd = 'cp -R ' + filepath + ' ./pdfs'
        print(sys_cmd)
        try:
            os.system('mkdir - p pdfs')
            os.system(sys_cmd)
        except:
            print('File not found at ', filepath)

    download_button.on_click(copy_pdf)
    display(pub_display)
    pass

def get_phase_info():
    common_phs = pd.read_csv(APP_DATA_DIR+'common-phases.csv')
    pure_phs = pd.read_csv(APP_DATA_DIR+'pure-phases.csv')
    soln_phs = pd.read_csv(APP_DATA_DIR+'solution-phases.csv')


    phase_info = {'common': common_phs, 'pure':pure_phs, 'solution':soln_phs}
    return phase_info

def set_phase_chem(show_gui=True, outbox=None):
    menu_data = {}

    menu_data['all_phase_info'] = get_phase_info()

    def on_delete_phase(b):
        # , phaseID=0
        # print('enter func = ', menu_data['phase_tabs']._titles)
        ind = menu_data['phase_tabs'].selected_index
        menu_data['phase_tabs'].selected_index = None
        # ind = np.where(np.array(menu_data['phaseIDs'])==phaseID)[0][0]
        # print(menu_data['phasenames'])
        # print(ind)
        # print(menu_data['phase_tabs'])
        tab_list = list(menu_data['phase_tabs'].children)
        # print(menu_data['phase_tabs'])

        menu_data['phasenames'].pop(ind)
        menu_data['phases'].pop(ind)
        # menu_data['phaseIDs'].pop(ind)
        tab_list.pop(ind)
        menu_data['phase_tabs'].children = tab_list


        for ind, phase in enumerate(menu_data['phasenames']):
            menu_data['phase_tabs'].set_title(ind, phase)

        menu_data['phase_tabs'].set_title(len(menu_data['phasenames']),None)

        Nphase = len(menu_data['phasenames'])
        # print('Nphase = ', Nphase)
        titles = menu_data['phase_tabs']._titles
        # print('pre = ',titles)
        del titles[str(Nphase)]
        # print('post delete = ',titles)
        menu_data['phase_tabs']._titles = titles

        # print('post set titles = ',menu_data['phase_tabs']._titles)

        # print(len(menu_data['phasenames']))
        # print(len(menu_data['phases']))
        # print(len(menu_data['phaseIDs']))
        # print(len(menu_data['phase_tabs'].children))

        if len(titles) >0:
            menu_data['phase_tabs'].selected_index = np.minimum(ind,Nphase-1)

    def on_add_phase(b):
        Nphase_init = len(menu_data['phase_tabs'].children)
        phase = menu_data['phasename_input'].value
        phase_info = menu_data['phase_info']
        typ = menu_data['phase_typ_selector'].value

        if phase_info is None:
            phase_symbol = phase
        else:
            phs_index = menu_data['phasename_input'].index
            phase_symbol = phase_info['symbol'][phs_index]


        phases_temp = list(menu_data['phase_tabs'].children)
        phase_chem = _new_phase_chem(phase, phase_symbol, typ)
        # menu_data['phaseID_last'] += 1
        # phaseID = menu_data['phaseID_last']
        # menu_data['phaseIDs'].append(phaseID)


        # phase_chem['delete_phase'].on_click(
        #     lambda b, phaseID=phaseID:on_delete_phase(b, phaseID=phaseID))
        phase_chem['delete_phase'].on_click(on_delete_phase)

        # chem_comp = _get_chem_comp()
        # phases_temp.append(chem_comp['panel'])
        phases_temp.append(phase_chem['Box'])
        menu_data['phase_tabs'].children = phases_temp
        menu_data['phasenames'].append(phase)


        ###  TK  ###
        menu_data['phases'].append(phase_chem)

        menu_data['phase_tabs'].set_title(Nphase_init, phase_symbol)

    phase_types = ['common-phase','pure-phase','solution-phase','custom-phase']
    menu_data['phase_typ_selector'] = widgets.Dropdown(
        options=phase_types, layout=widgets.Layout(width='150px'))

    def on_change_phase_typ(change):
        all_phase_info = menu_data['all_phase_info']
        typ = menu_data['phase_typ_selector'].value
        if typ=='common-phase':
            menu_data['phase_info'] = all_phase_info['common']
            menu_data['phasename_dropdown'].options = menu_data['phase_info']['name']
            menu_data['phasename_input'] = menu_data['phasename_dropdown']
        elif typ=='pure-phase':
            menu_data['phase_info'] = all_phase_info['pure']
            menu_data['phasename_dropdown'].options = menu_data['phase_info']['name']
            menu_data['phasename_input'] = menu_data['phasename_dropdown']
        elif typ=='solution-phase':
            menu_data['phase_info'] = all_phase_info['solution']
            menu_data['phasename_dropdown'].options = menu_data['phase_info']['name']
            menu_data['phasename_input'] = menu_data['phasename_dropdown']
        elif typ=='custom-phase':
            menu_data['phasename_input'] = menu_data['phasename_text']
            menu_data['phase_info'] = None
        else:
            print('not valid!')

        opts = list(menu_data['add_phase'].children)
        #print(len(opts))
        opts.pop(1)
        opts.insert(1, menu_data['phasename_input'])
        menu_data['add_phase'].children = opts

    menu_data['phasenames'] = []
    menu_data['phases'] = []
    # menu_data['phaseIDs'] = []
    # menu_data['phaseID_last'] = -1


    # children = [widgets.Text(description=name) for name in tab_contents]
    # children = [_get_chem_comp() for name in menu_data['phases']]
    menu_data['phase_tabs'] = widgets.Tab()
    menu_data['phase_tabs'].children = []
    # for i, phs in enumerate(menu_data['phases']):
    #     tab.set_title(i, phs)

    phasename_dropdown = widgets.Dropdown(
        options=menu_data['all_phase_info']['common']['name'], layout=widgets.Layout(width='300px'))

    menu_data['phase_info'] = menu_data['all_phase_info']['common']
    phasename_text = widgets.Text(value='')

    add_phase_button = widgets.Button(description='Add-Phase')
    menu_data['phasename_dropdown'] = phasename_dropdown
    menu_data['phasename_text'] = phasename_text
    menu_data['phasename_input'] = phasename_dropdown
    menu_data['add_phase_button'] = add_phase_button


    add_phase_button.on_click(on_add_phase)
    menu_data['phase_typ_selector'].observe(on_change_phase_typ)

    add_phase = widgets.HBox(
        children=[menu_data['phase_typ_selector'],phasename_dropdown,add_phase_button])
    menu_data['add_phase'] = add_phase
    menu = widgets.VBox(
        children=[add_phase, menu_data['phase_tabs']])
    menu_data['menu'] = menu

    if show_gui:
        display(menu)

    return menu_data

def _get_chem_comp():
    chem_comp = {}
    def set_chem_dropdown(options):
        chem_comp['chem'].options = options
        chem_comp['chem'].value = options[0]

    def on_chem_typ_change(change):
        chem_pos = 3
        chem_typ = chem_comp['chem_typ'].value
        members = list(chem_comp['panel'].children)
        members.pop(chem_pos)
        if chem_typ == 'oxide':
            chem_comp['chem']=chem_comp['std-chem']
            set_chem_dropdown(chem_comp['oxides'])
        elif chem_typ == 'element':
            chem_comp['chem']=chem_comp['std-chem']
            set_chem_dropdown(chem_comp['elems'])
        elif chem_typ == 'custom':
            chem_comp['chem']=chem_comp['custom-chem']
            chem_comp['chem'].value = ''
            # members.insert(chem_pos, chem_comp['custom-chem'])
            # print('kill')
        else:
            print('not valid')

        members.insert(chem_pos, chem_comp['chem'])
        chem_comp['panel'].children = members
        on_chem_select(None)

    def on_chem_select(change):
        chem_select = chem_comp['chem'].value
        report_as = chem_comp['as_total'].value
        # print(report_as)

        chem_lbl = chem_select
        if report_as=='total':
            chem_lbl = chem_lbl +'(tot)'

        chem_comp['label'].value = chem_lbl
        chem_comp['comp_str'] = chem_lbl


    oxides = ['SiO2','MgO','Al2O3','H2O','CO2','FeO','Fe2O3','CaO',
              'Na2O','K2O','TiO2','MnO','NiO','Cr2O3','P2O5',]
    elems = ['Sc','Ti','V','Zn',
             'Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd',
             'La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy',
             'Ho','Er','Tm','Yb','Lu','Hf','Ta']
    chem_typ = ['oxide','element','custom']
    # major_elems = ['Fe','Al','Si','Mg']
    # trace_elems = ['Zn','U']
    report_as = ['measured','total']


    chem_comp['oxides'] = oxides
    chem_comp['elems'] = elems
    # chem_comp['trace-elems'] = trace_elems

    chem_comp['delete'] = widgets.Button(
        icon='minus', layout=widgets.Layout(width='50px'))

    chem_comp['label'] = widgets.Label(
        value=oxides[0], layout=widgets.Layout(width='200px'))
    chem_comp['custom-chem'] = widgets.Text(
        value=oxides[0], layout=widgets.Layout(),
    )
    chem_comp['std-chem'] = widgets.Dropdown(options=oxides)

    chem_comp['comp_str'] = chem_comp['label'].value
    chem_comp['chem_typ'] = widgets.Dropdown(options=chem_typ)
    chem_comp['chem'] = chem_comp['std-chem']
    # chem_comp['as_total'] = widgets.Checkbox(
    #     value=False, description='as total')
    chem_comp['as_total'] = widgets.RadioButtons(
        options=['measured','total'])


    comp_panel = widgets.HBox(children=[
        chem_comp['delete'], chem_comp['label'],
        chem_comp['chem_typ'], chem_comp['chem'], chem_comp['as_total']
        ] )




    chem_comp['chem_typ'].observe(on_chem_typ_change)
    chem_comp['chem'].observe(on_chem_select)
    chem_comp['custom-chem'].observe(on_chem_select)
    chem_comp['as_total'].observe(on_chem_select)

    chem_comp['panel'] = comp_panel

    return chem_comp

def create_chem_block():
    label = widgets.Label(value='Measured Chemistry', layout=widgets.Layout(width='200px'))
    chem_meas = _new_chem_meas()
    add_chem_button = widgets.Button(
        icon='plus', layout=widgets.Layout(width='40px'))

    menu = widgets.VBox(
        children=[label, chem_meas['Box'], add_chem_button])

    chem_block = {}
    chem_block['menu'] = menu
    chem_block['chem_meas'] = chem_meas

    return chem_block

def _new_chem_meas():
    chem_meas = {}
    # phase_chem = {}

    def on_delete_chem_comp(b, chem_meas=chem_meas, compID=0):
        ind = np.where(np.array(chem_meas['compIDs'])==compID)[0][0]
        # print('matching ind = ',ind)
        # print(chem_meas['compIDs'])
        del chem_meas['compIDs'][ind]
        chem_meas['comp_list'].pop(ind)
        box_list = list(chem_meas['Box'].children)
        box_list.pop(ind+2)
        chem_meas['Box'].children = box_list

    def on_add_chem_comp(b, chem_meas=chem_meas):
        chem_comp_Box = list(chem_meas['Box'].children)
        new_chem_comp = _get_chem_comp()
        chem_meas['compID_last'] += 1
        compID = chem_meas['compID_last']
        # print(chem_meas['compIDs'])
        chem_meas['compIDs'].append(compID)

        new_chem_comp['delete'].on_click(
            lambda b, compID=compID:on_delete_chem_comp(b, compID=compID))

        chem_comp_Box.insert(-1, new_chem_comp['panel'])
        chem_meas['comp_list'].append(new_chem_comp)
        chem_meas['Box'].children = chem_comp_Box


    add_chem_comp = widgets.Button(
        icon='plus', layout=widgets.Layout(width='40px'))
    # delete_phase = widgets.Button(
    #     description='Delete', layout=widgets.Layout(margin='0 0 0 200px'))
    delete_phase = widgets.Button(
        description='Delete Phase')

    # [TK]
    add_chem_comp.on_click(on_add_chem_comp)

    # is_starter_phase_default = True
    # if typ=='pure-phase':
    #     is_rxn_soln_phase_default = False
    # else:
    #     is_rxn_soln_phase_default = True

    # chem_meas['is_starter_phase'] = widgets.ToggleButton(
    #     description='starter_phase', value=is_starter_phase_default,
    #     tooltip='This defines the initial chemistry components of this starter phase '+
    #     '(used in the sample mix OR to track pure phase reactions/EOS behavior).')
    # chem_meas['is_rxn_solution_phase'] = widgets.ToggleButton(
    #     description='rxn_solution_phase', value=is_rxn_soln_phase_default,
    #     tooltip='This defines the final chemistry components of this reaction solution phase.')


    # chem_meas['set_phase_appearance'] = widgets.HBox(
    #     [chem_meas['is_starter_phase'],
    #      chem_meas['is_rxn_solution_phase']],
    #     layout=widgets.Layout(align_items='flex-start')
    # )

    chem_meas['add_chem_comp'] = add_chem_comp
    chem_meas['delete_phase'] = delete_phase

    # chem_meas['name'] = phase
    # chem_meas['symbol'] = phase_symbol
    chem_meas['Box'] = widgets.VBox()
    # [TK] modify_phase_chem
    chem_meas['modify_chem_meas'] = widgets.HBox(
        [chem_meas['add_chem_comp'],chem_meas['delete_phase']],
        layout=widgets.Layout(justify_content='space-between'))

    # [TK]
    # chem_meas['Box'].children = [
    #     widgets.HTML(value="<b>"+"PHASE"+"</b>",layout=widgets.Layout(width='250px')),
    #     chem_meas['set_phase_appearance'], chem_meas['modify_chem_meas'],]
    chem_meas['comp_list'] = []
    chem_meas['compIDs'] = []
    chem_meas['compID_last'] = -1

    # [TK]
    on_add_chem_comp(None)

    # chem_comp = _get_chem_comp()
    # chem_meas['comp_list'] = [chem_comp]
    # chem_meas['Box'].children = [
    #    chem_comp['panel'], chem_meas['add_chem_comp']]
    return chem_meas

def _new_phase_chem(phase, phase_symbol, typ):
    phase_chem = {}

    def on_add_chem_comp(b):
        chem_comp_Box = list(phase_chem['Box'].children)
        new_chem_comp = _get_chem_comp()
        phase_chem['compID_last'] += 1
        compID = phase_chem['compID_last']
        # print(phase_chem['compIDs'])
        phase_chem['compIDs'].append(compID)

        new_chem_comp['delete'].on_click(
            lambda b, compID=compID:on_delete_chem_comp(b, compID=compID))

        chem_comp_Box.insert(-1, new_chem_comp['panel'])
        phase_chem['comp_list'].append(new_chem_comp)
        phase_chem['Box'].children = chem_comp_Box

    def on_delete_chem_comp(b, compID=0):
        ind = np.where(np.array(phase_chem['compIDs'])==compID)[0][0]
        # print('matching ind = ',ind)
        # print(phase_chem['compIDs'])
        del phase_chem['compIDs'][ind]
        phase_chem['comp_list'].pop(ind)
        box_list = list(phase_chem['Box'].children)
        box_list.pop(ind+2)
        phase_chem['Box'].children = box_list

    add_chem_comp = widgets.Button(
        icon='plus', layout=widgets.Layout(width='40px'))
    # delete_phase = widgets.Button(
    #     description='Delete', layout=widgets.Layout(margin='0 0 0 200px'))
    delete_phase = widgets.Button(
        description='Delete Phase')

    # [TK]
    # add_chem_comp.on_click(on_add_chem_comp)

    is_starter_phase_default = True
    if typ=='pure-phase':
        is_rxn_soln_phase_default = False
    else:
        is_rxn_soln_phase_default = True

    phase_chem['is_starter_phase'] = widgets.ToggleButton(
        description='starter_phase', value=is_starter_phase_default,
        tooltip='This defines the initial chemistry components of this starter phase '+
        '(used in the sample mix OR to track pure phase reactions/EOS behavior).')
    phase_chem['is_rxn_phase'] = widgets.ToggleButton(
        description='rxn_phase', value=is_rxn_soln_phase_default,
        tooltip='This defines the final chemistry components of this reaction solution phase.')


    phase_chem['set_phase_appearance'] = widgets.HBox(
        [phase_chem['is_starter_phase'],
         phase_chem['is_rxn_phase']],
        layout=widgets.Layout(align_items='flex-start')
    )

    phase_chem['add_chem_comp'] = add_chem_comp
    phase_chem['delete_phase'] = delete_phase

    phase_chem['name'] = phase
    phase_chem['symbol'] = phase_symbol
    phase_chem['Box'] = widgets.VBox()
    phase_chem['modify_phase_chem'] = widgets.HBox(
        [phase_chem['add_chem_comp'],phase_chem['delete_phase']],
        layout=widgets.Layout(justify_content='space-between'))

    phase_chem['Box'].children = [
        widgets.HTML(value="<b>"+phase+"</b>",layout=widgets.Layout(width='250px')),
        phase_chem['set_phase_appearance'], phase_chem['modify_phase_chem'],]
    phase_chem['comp_list'] = []
    phase_chem['compIDs'] = []
    phase_chem['compID_last'] = -1

    # [TK]
    # on_add_chem_comp(None)

    # chem_comp = _get_chem_comp()
    # phase_chem['comp_list'] = [chem_comp]
    # phase_chem['Box'].children = [
    #    chem_comp['panel'], phase_chem['add_chem_comp']]
    return phase_chem

def _extract_exp_chem(menu_data):
    exp_chem = {}

    exp_chem['phases'] = []
    exp_chem['symbols'] = []
    exp_chem['chem_comps'] = []
    exp_chem['starter_chem_comps'] = set([])
    exp_chem['is_starter_phase'] = []
    exp_chem['is_rxn_phase'] = []

    for phasenm, phase in zip(menu_data['phasenames'],
                              menu_data['phases']):
        isymbol = phase['symbol']
        #print(len(phase['comp_list']))
        #print(phase['comp_list'][0]['comp_str'])
        #print('=======')

        # comp_list = phase['comp_list']
        # print(comp_list)
        ichem_comps = [
            comp['comp_str'] for comp in phase['comp_list']]


        istarter = phase['is_starter_phase'].value
        irxn_soln = phase['is_rxn_phase'].value

        # print(istarter)
        exp_chem['phases'].append(phasenm)
        exp_chem['symbols'].append(isymbol)
        exp_chem['chem_comps'].append(ichem_comps)
        if istarter:
            exp_chem['starter_chem_comps'] = exp_chem['starter_chem_comps'].union(set(ichem_comps))
        exp_chem['starter_chem_comps'] = exp_chem['starter_chem_comps']

        exp_chem['is_starter_phase'].append(istarter)
        exp_chem['is_rxn_phase'].append(irxn_soln)

    return exp_chem

def create_excel_template(menu_data, show_gui=True, data_dir=None,
                          outbox=None):
    template_data = {}

    filetype_dropdown = widgets.Dropdown(description='Filetype:',
                                         options=['phase-rxn','eos'])

    template_data['filetype_dropdown'] = filetype_dropdown
    filename_box = widgets.Text(description='Filename:',
                                value='template')

    template_data['filename_box'] = filename_box
    ok_button = widgets.Button(description='OK')

    template_data['ok_button'] = ok_button

    menu = widgets.HBox(
        children=[filetype_dropdown, filename_box, ok_button])


    def on_change_filetype(change):
        filetype = template_data['filetype_dropdown'].value
        template_data['filename_box'].value = filetype+'-data'

    def on_create_template(change, data_dir=data_dir, outbox=outbox):
        if data_dir is None:
            data_dir = 'my-data/'

        filetype = template_data['filetype_dropdown'].value
        filename = data_dir+template_data['filename_box'].value + '.xlsx'
        exp_chem = _extract_exp_chem(menu_data)

        print_obj = ('The data template file has been written.\n'
                     'It can be found at "'+filename+'".\n'
                     'Download this file to your local computer to begin entering data.'
                     )

        if filetype=='phase-rxn':
            dataio.write_phase_rxn_template(filename, exp_chem)
            _print_output(print_obj, outbox)
        elif filetype=='eos':
            dataio.write_eos_template(filename, exp_chem)
            _print_output(print_obj, outbox)
        else:
            _print_output('There is an error, that choice is not valid.',
                          outbox)

    template_data['filetype_dropdown'].observe(on_change_filetype)
    template_data['ok_button'].on_click(on_create_template)
    on_change_filetype(None)

    template_data['menu'] = menu

    if show_gui:
        display(menu)

    return template_data
