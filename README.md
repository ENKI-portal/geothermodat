# geothermodat - An open database of geological thermodynamic data for model calibration

## Installation
* Download package by cloning repository (using git clone)
* Install python package:
    * `python setup.py develop`

*  `sudo pip` to install on ENKI compute server
* `sudo pip install .`
