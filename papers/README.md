# Paper Database for all contributing publications
* NOTE: papers are stored using git-lfs (large file storage). See below for details.
* BibTeX file stores unique bibtex key for every publication
    * Doubles as unique paper ID
* pdf directory stores pdf of original paper
* data archive should use paperID that agrees with this archive


# Git-LFS
* See git-lfs tutorial at:  https://about.gitlab.com/2017/01/30/getting-started-with-git-lfs-tutorial/
* You need to install git-lfs on your local computer

# Paper To-Do list

