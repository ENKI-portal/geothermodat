# README for Geothermodat Documentation


## Required installations

- numpydoc
- recommonmark (allows .md and .rst files in the same project)

## Equations 
Equations use Mathjax and must be written in LaTeX.

In Markdown files, they must be written like this:

- **Standalone equations** begin with double dollar signs $$ and end with double dollar signs $$

<pre>$$...$$</pre>

- **Inline equations** begin with two backslashes and an open parenthesis `\\(` and end with two backslashes and a close parenthesis `\\)`

``\\(...\\)``

In HTML code, they must be written like this:
- **Standalone equations** are the same as for Markdown.

- **Inline equations** begin with one backslash and an open parenthesis `\(` and end with one backslash and a close parenthesis `\)`

``\(...\)``

