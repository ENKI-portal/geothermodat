# Submit Data  

To populate the Geothermodat database, the published data must first be digitized. This task involves manually entering the data into Excel spreadsheets.  

The data are organized into these categories: 
- Equation of state
- Phase reaction
- Heat capacity
- Aqueous reaction

Each category requires one spreadsheet per paper. 

## Repository structure  
- `/papers/pdfs` folder contains:
  - PDF copies of all papers from which data are contributed to the respository. 
  - A `.bib` file that contains each paper's BibTex key. 

- `/data` folder contains subfolders for the Excel templates. The subfolders also contain the completed Excel files (in general, named according to author and year of publication).  


## Filling out the spreadsheets 

### 1. Add a PDF copy of the published paper
Place a PDF copy of the published paper into the `/papers/pdfs` folder.
### 2. Obtain the paper's reference information

1. Make sure that [JabRef](http://www.jabref.org) (bibliography manager) is installed. 

2. In JabRef, open the `data-sources.bib` file, located in the  `/papers/pdfs` folder.  

3. Drag a copy of the paper's citation file onto JabRef.  
The publication ID number (or BibTeX number) and other reference information appears on the Reference and General tabs in JabRef. You need this information to complete the spreadsheets.


### 2. Complete the required spreadsheets   
Complete the required spreadsheets: 
- For equation of state data, fill out an Equation of State spreadsheet. [➔  Go to the instructions](EOSinstructions.html).  
- For phase reaction data, fill out a Phase Reaction spreadsheet. [➔ Go to the instructions](PhaseRxnInstructions.html).  

