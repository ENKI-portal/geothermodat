# Complete a spreadsheet for Equation of State  

## Overview  
The Equation of State spreadsheet contains the following tabs:
- [reference](#reference-tab) - Basic reference information about the published paper
- [notes](#notes-tab) - Notes regarding any special decisions you made in transcribing the data
- [starter_phases](#starter-phases-tab) - Details of the phases at the start of the experiment
- [experimental_conditions](#experimental-conditions-tab) - Details of the experimental setup
- [\<Phase\> tabs](#phase-tabs) - Details of the phases in the experiment results

## General setup
1. In the `data` directory, open these files:  
   -  `vocab_key.xlsx`: Vocabulary spreadsheet, which contains allowable terminology for most entries in the spreadsheets  
   - `/eos/eos-template.xlsx`: Equation of State template file
2. Make a copy of the template file, and change the copy's filename to the paper's publication ID number followed by `-eos`; for example, `Holdaway1971-eos.xlsx`.

2. Set up the phase tabs: Using the placeholder **\<Phase\>** tab, create a tab for each phase that occurs in the experimental results. Change the header text (**\<Phase\>**) to the name of the phase. For example, if the results involve three phases—andalusite, kyanite, and sillimanite—add an **And** tab, a **Ky** tab, and a **Sil** tab. Make sure to use only names allowed by the Vocabulary spreadsheet. 

## Fill out the tabs

### reference tab
The **reference** tab contains the basic reference information about the published paper. 

✦ Fill out the columns. You can copy most of this information from the file you opened in JabRef. 

- **pub_id** - The publication ID number for the paper  
- **authors** - Names of author or authors   
- **date** - Year of publication  
- **title** - Title of paper  
- **journal** - Journal where published  
- **volume** - Volume number  
- **pages** - Page numbers of published paper  
- **doi** - Document Object Identifier for the paper  
- **notes** - Enter any notes related to information you entered on this tab.  
 
 
### notes tab
✦ On the **notes** tab, enter any notes such as special choices you made in recording the data. These notes help to make your work reproducible. Use whatever format you wish. This tab is to be read by humans rather than computers. 

### starter_phases tab

The **starter_phases** tab describes the phases at the start of the experiment. Each row represents an experiment.  

1. Set up the chemistry columns.  

   For each element or oxide in the experiment, you need four values: composition, error, method, and standard. Therefore, you need to set up four columns for each composition reported:  
   - *composition*   
   - *composition*\_err  
   - *composition*\_method  
   - *composition*\_std  

   Use the placeholder headers to set up the columns, adding additional sets as needed. (Delete any not needed.)  

<table style="border: 1px solid black;margin-left:30px; padding-bottom:15px;">
<tbody>
  <tr style="border: 1px solid black;margin-left:30px;background-color:#eee">
    <th style="padding:10px;">Placeholder headers</th>
    <th style="padding:10px;">Usage</th> 
    <th style="padding:10px;">Example</th>
</tr>
 
  <tr style="border: 1px solid black;margin-left:30px;">
    <td valign="top"style="padding:10px; width:30%"><strong>&#60;oxide&#62;<br/>
    &#60;oxide&#62;_err <br/> 
    &#60;oxide&#62;_method<br/>  
    &#60;oxide&#62;_std<br/></strong>  
   </td>
    <td valign="top"style="padding:10px">Use these if the phase is an oxide. Replace <span style="font-family:courier">&#60;oxide&#62;</span> with the oxide name.</td>   
    <td valign="top"style="padding:10px"><strong>Fe2O3</strong><br/> 
    <strong>Fe2O3_err</strong><br/> 
    <strong>Fe2O3_method</strong><br/> 
    <strong>Fe2O3_std</strong>
    </td>
    </tr>
    <tr style="border: 1px solid black;margin-left:30px">
    <td valign="top" style="padding:10px; width:30%">
    <strong>&#60;oxide&#62;(tot)</strong><br/>
    <strong>&#60;oxide&#62;(tot)_err</strong><br/>
    <strong>&#60;oxide&#62;(tot)_method</strong><br/>
    <strong>&#60;oxide&#62;(tot)_std<br/></strong></td>
    <td valign="top" style="padding:10px;">Use these headers if the measurement used a method that reported an element as total oxide equivalent. Replace <span style="font-family:courier">&#60;oxide&#62;</span> with the oxide name. </td><td valign="top"style="padding:10px">
    <strong>Fe2O3(tot)</strong><br/>
    <strong>Fe2O3(tot)_err</strong><br/>
    <strong>Fe2O3(tot)_method</strong><br/> 
    <strong>Fe2O3(tot)_std </strong><br/>
    * This experiment measured iron atoms, and the authors reported the results as total feric iron.<br/>
    </td>
  </tr>
   <tr style="border: 1px solid black;margin-left:30px">
    <td valign="top" style="padding:10px; width:30%">
    <strong>&#60;element&#62;</strong><br/>  
    <strong>&#60;element&#62;_err</strong><br/>  
    <strong>&#60;element&#62;_method</strong><br/>  
    <strong>&#60;element&#62;_std</strong>  
</td> 
    <td valign="top" style="padding:10px">Use these if the phase is an element. Replace <span style="font-family:courier">&#60;element&#62;</span> with the element name. 
    </td>
    <td valign="top" style="padding:10px">
    <strong>SIO2</strong><br/>  
    <strong>SiO2_err</strong><br/>  
    <strong>SiO2_method</strong><br/>  
    <strong>SiO2_std<strong>
  </td>
  </tr>
  </tbody>
</table>
 
2. On Row 2, add subheads for unit (usually `wt%`) to the first two columns of each set. Refer to the Vocabulary spreadsheet for allowable entries.

3. Fill out the columns: 

##### Sample information  
- **sample_id**, **sample_name** - In these columns, provide a unique identifier and name for each sample. Use whatever convention you wish, but try for something meaningful. For example, you might use `Sil` (for sample_id) and `Sillimanite` (for sample_name) for a sillimanite sample.  If the paper involves multiple sillimanite samples, you might use `Sil1` and `Sillimanite1`, `Sil2` and `Sillimanite2`, `Sil3` and `Sillimanite3`, and so on.   

##### Reference information
- **source_pubid** - The publication ID number for the paper in which the sample originated.  
- **notes** - Enter any notes related to information you entered on that row. 
- **table_num** - If the sample appears in a table, enter the table number. Otherwise, leave this field blank.   
- **figure_num** -  If the sample appears in a figure, enter the figure number. Otherwise, leave this field blank. 
- **page_num** - Enter the page number on which the sample is described. Use ` <-> ` to indicate a range of pages. For example, for pages 6 to 7, enter `6 <-> 7`.   

##### Phase information
- **phase** - Make sure to enter the correct abbreviation listed in the Vocabulary spreadsheet (**pure_phases_vocab** tab). For example, enter `And` for *Andalusite*.    
- **synthetic** - Enter `Yes` if the sample is synthetically created, or `No` if it is natural.  
- **grain_size**  
- **contaminant_phases** - If the sample is not pure but contains another phase, indicate what that other phase is. First, in Row 2, enter a unit of measurement. Then, in the working row, enter a value and the contaminant phase. For example, for a sillimanite sample that is 30% fibrous sillimanite, enter `vol%` (volume percent) in Row 2, and enter `30 fSil` in the working row. Make sure to use only entries allowed by the Vocabulary spreadsheet.  


  
##### Chemistry columns
*Note: You set up these columns in a previous step.*

- *composition* - Enter the compositional measurement.  
- *composition*\_**err** - Enter the error in the amount of the composition. You can use decimal places as well as scientific notations.  
- *composition*\_**method** - Enter the method used to measure the composition (for example, `AAS`, which indicates Atomic Absorption Spectroscopy).  
- *composition*\_**std** - Enter the standard used in the method.  
-  **Total** - Enter the total sum of the compositional values, for a sum close to 100%.   

### experimental_conditions tab  

The **experimental_conditions** tab contains the details of the experimental setup. If any of the columns do not apply, leave its fields blank. Except where noted, use only entries from the Vocabulary spreadsheet.

1. Enter a unit of measurement on Row 2 where appropriate. 
2. Fill out the columns:  

##### Reference information
- **index** - Enter numbers to identify all the experiments. If there are, for example, five experiments, fill out five rows. The first experiment is `1`; the second is `2`, and so on. (This column and all its entries must be identical to the corresponding **index** columns on the phase tabs.)
- **run_id** - Enter a meaningful name for each experimental run. For example, if there are two silliminate experiments, enter `Sil1_0` and `Sil2_0`. (This column and all its entries must be identical to the corresponding **run_id** columns on the [phase tabs](#phase-tabs).)  
- **lab_id** -  
- **source_pubid** - The publication ID number for the paper in which the sample originated.  
- **table_num** - If the experiment appears in a table, enter the table number. 
- **figure_num** - If the experiment appears in a figure, enter the figure number.  
- **page_num** - Enter the page number on which the experiment is described. Use ` <-> ` to indicate a range of pages. For example, for pages 6 to 7, enter `6 <-> 7`.   

##### Equipment used  
- **device**   
- **container**   
- **capsule_width**   
- **capsule_length**   

##### Procedural details  
- **annealed**   
- **aneal_time**   
- **equil_time** - Equilibration time of the experiment  
- **press_medium**   
- **peak_oil_press**   

##### Pressure and temperature conditions  
- **P_meas_method**   
- **T_meas_method**   
- **T_meas_heat_loc**   
- **T_meas_samp_loc**   
- **P** - Pressure 
- **P_err**   
- **T** - Temperature  
- **T_err**   

##### Oxidation state conditions
- **fO2**   
- **fO2_err** 	 

##### Confidence  
- **trust** - Enter `Yes`, `No`, or `Maybe` to indicate whether you have confidence in this experiment. 
- **notes** - <span style="background-color: cyan">Important:</span>  If you entered `No` or `Maybe` for **trust**, enter an explanation here. Also, enter any other notes related to information you entered on this row.  

### \<Phase> tabs
The phase tabs describe the results of the experiments.

1. On each phase tab, enter the same information for the **index** and **run_id** columns as it appears on the **experimental_conditions** tab. Make sure that all these columns on all these tabs are identical.

2. On each phase tab, fill out the remaining columns. Fill out only those rows that involve this phase. Leave rows for other phases blank.  
   
   
##### Sample information
- **sample_id** - Same sample_id as on the **starter_phases** tab. 
- **source_pubid** - The publication ID number for the paper in which the sample originated.  

##### Notes  
- **notes** - Any notes related to information you entered on this row  

##### Unit cell dimensions
- **V** - Volume measured 
- **V_err** -  Error of the volume measured
- **a** - Length of the *a* direction of the unit cell in the crystal  
- **a_err** - Error in the measurement of *a*  
- **b** - Length of the *b* direction of the unit cell in the crystal  
- **b_err** - Error in the measurement of *b*  
- **c** -  Length of the *c* direction of the unit cell in the crystal 
- **c_err** - Error in the measurement of *c*	
- **alpha**   
- **alpha_err**   
- **beta**   
- **beta_err**   
- **gamma**   
- **gamma_err**   

##### XRD data

- **xrd_peak_num**   
- **d(h,k,l)**   
- **d(h,k,l)\_err**   

##### Refraction data  
- **refract_idx_alpha** - Measurement of the refractive index in alpha direction 
- **refract_idx_alpha_err** - Error in the measurement of the refractive index in alpha direction  
- **refract_idx_beta** - Measurement of the refractive index in beta direction 
- **refract_idx_beta_err** - Error in the measurement of the refractive index in beta direction.  
- **refract_idx_gamma** - Measurement of the refractive index in gamma direction 
- **refract_idx_gamma_err** - Error in the measurement of the refractive index in gamma direction 




