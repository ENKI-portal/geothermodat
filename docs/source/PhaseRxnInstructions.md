# Complete a spreadsheet for Phase Reaction

## Overview  
The Phase Reaction spreadsheet contains the following tabs:

- [reference](#reference-tab) - Basic reference information about the published paper
- [notes](#notes-tab) - Notes regarding any special decisions you made in transcribing the data
- [starter_phases](#starter-phases-tab) - Description of the samples available at the start of the experiments  
- [experimental_conditions](#experimental-conditions-tab) - Details of the experimental setup
 - [sample_material](#sample-material-tab) - Description of the mixture of the various starter phases used in the experiment 
 - [rxn](#rxn-tab) - Details about the results of the experiments    
- [\<Phase> tabs](#phase-melt-if-needed-and-fluid-if-needed-tabs)  - (If needed) Additional details on the phases in the experimental results   
- [melt(if needed)](#phase-meltif-needed-and-fluidif-needed-tabs)    
- [fluid(if needed)](#phase-meltif-needed-and-fluidif-needed-tabs)  

## General setup  
1. In the `data` directory, open these files:  
   -  `vocab_key.xlsx`: Vocabulary spreadsheet, which contains allowable terminology for most entries in the spreadsheets  
   - `phase-rxn/phase-rxn-template.xlsx`: Phase Reaction template file
2. Make a copy of the template file, and change the copy's filename to the paper's publication ID number followed by `-phase-rxn`; for example, `Holdaway1971-phase-rxn.xlsx`.  

3. If needed, set up the [phase tabs](#phase-melt-if-needed-and-fluid-if-needed-tabs) (using the **\<Phase\>** tab placeholder), the same as for the [EOS spreadsheet](EOSinstructions.html).  

## Fill out the tabs

### reference tab
✦ Fill out the **reference** tab the same as for the paper's [EOS spreadsheet](EOSinstructions.html). 

### notes tab
✦ On the **notes** tab, enter any notes such as special choices you made in recording the data. These notes help to make your work reproducible. Use whatever format you wish. This tab is to be read by humans rather than computers.  

### starter_phases tab  
✦ Fill out the **starter_phases** tab the same as for the paper's [EOS spreadsheet](EOSinstructions.html). Typically, all rows in this tab will be identical to those in its EOS counterpart. Add additional rows for new phases if needed.

This tab describes all the samples available for use at the start of the experiments.

### experimental_conditions tab
This tab describes the conditions of the experiments as well as the results. Each row in this tab represents an experiment. Except where noted, make sure to use only allowable entries from the Vocabulary spreadsheet.  
1. Wherever needed, enter a unit on Row 2.
2. Fill out the columns:   

##### Reference information

- **index** - Enter numbers to identify all the experiments. If there are, for example, 60 experiments, fill out 60 rows. The first experiment is `1`; the second is `2`, and so on. (This column and all its entries must be identical to the **index** column on the **sample_material** and **rxn** tabs.)
- **run_id** - Enter a meaningful name for each experimental run. For example, use the name that appears in the table in the paper. (This column and all its entries must be identical to the **run_id** column on the **sample_material** and **rxn**  tabs.)  
- **lab_id** -  
- **source_pubid** -  The publication ID number for the paper in which the sample originated.
- **table_num** - If the experiment appears in a table, enter the table number. 
- **figure_num** - If the experiment appears in a figure, enter the figure number.  
- **page_num** - Enter the page number on which the experiment is described. Use ` <-> ` to indicate a range of pages. For example, for pages 6 to 7, enter `6 <-> 7`.   

##### Equipment used  

- **device**   
- **container**   
- **capsule_width**   
- **capsule_length**   

##### Procedural details
- **annealed**   
- **aneal_time**   
- **equil_time** - Equilibration time of the experiment 
- **press_medium**   
- **peak_oil_press**   

##### Pressure and temperature conditions
- **P_meas_method**   
- **T_meas_method**   
- **T_meas_heat_loc**   
- **T_meas_samp_loc**  
- **P** - Pressure 
- **P_err**  
- **T** - Temperature 
- **T_err**   

##### Oxidation state conditions

- **fO2**   
- **fO2_err** 	

##### Confidence

- **trust** - Enter `Yes`, `No`, or `Maybe` to indicate whether you have confidence in this experiment.  
- **notes** - <span style="background-color: cyan">Important:</span> If you entered `No` or `Maybe` for **trust**, enter an explanation here. Also, enter any other notes related to information you entered on this row.  

### sample\_material tab  
This tab details the mixture of the samples (which are defined on the **starter_phases** tab) used in the experiment. That is, it describes the starter phases mixed together and heated. Each row in this tab represents an experiment, the same experiment represented on the same row in the **experimental_conditions** and **rxn** tabs.  
1. Set up the chemistry columns.  

   For each element or oxide in the experiment, you need four values: composition, error, method, and standard. Therefore, you need to set up four columns for each composition reported:
   - *composition*   
   - *composition*\_err  
   - *composition*\_method  
   - *composition*\_std  

   Use the placeholder headers to set up the columns, adding additional sets as needed. (Delete any not needed.) 
  

<table style="border: 1px solid black;margin-left:30px">
<tbody>
  <tr style="border: 1px solid black;margin-left:30px;background-color:#eee">
    <th style="padding:10px;">Placeholder headers</th>
    <th style="padding:10px;">Usage</th> 
    <th style="padding:10px;">Example</th>
</tr>
 
  <tr style="border: 1px solid black;margin-left:30px; padding:10px;">
    <td valign="top"style="padding:10px; width:30%"><strong>&#60;oxide&#62;<br/>
    &#60;oxide&#62;_err <br/> 
    &#60;oxide&#62;_method<br/>  
    &#60;oxide&#62;_std<br/></strong>  
   </td>
    <td valign="top"style="padding:10px">Use these if the phase is an oxide. Replace <span style="font-family:courier">&#60;oxide&#62;</span> with the oxide name.</td>   
    <td valign="top" style="padding:10px"><strong>Fe2O3</strong><br/> 
    <strong>Fe2O3_err</strong><br/> 
    <strong>Fe2O3_method</strong><br/> 
    <strong>Fe2O3_std</strong>
    </td>
    </tr>
    <tr style="border: 1px solid black;margin-left:30px">
    <td valign="top" style="padding:10px; width:30%">
    <strong>&#60;oxide&#62;(tot)</strong><br/>
    <strong>&#60;oxide&#62;(tot)_err</strong><br/>
    <strong>&#60;oxide&#62;(tot)_method</strong><br/>
    <strong>&#60;oxide&#62;(tot)_std<br/></strong></td>
    <td valign="top" style="padding:10px;">Use these headers if the measurement used a method that reported an element as total oxide equivalent. Replace <span style="font-family:courier">&#60;oxide&#62;</span> with the oxide name. </td><td valign="top" style="padding:10px">
    <strong>Fe2O3(tot)</strong><br/>
    <strong>Fe2O3(tot)_err</strong><br/>
    <strong>Fe2O3(tot)_method</strong><br/> 
    <strong>Fe2O3(tot)_std </strong><br/>
    * This experiment measured iron atoms, and the authors reported the results as total feric iron.<br/>
    </td>
  </tr>
   <tr style="border: 1px solid black;margin-left:30px">
    <td valign="top" style="padding:10px; width:30%">
    <strong>&#60;element&#62;/strong><br/>  
    <strong>&#60;element&#62;_err</strong><br/>  
    <strong>&#60;element&#62;_method</strong><br/>  
    <strong>&#60;element&#62;_std</strong></td> 
    <td valign="top" style="padding:10px">Use these if the phase is an element. Replace <span style="font-family:courier">&#60;element&#62;</span> with the element name. 
    </td>
    <td valign="top" style="padding:10px">
    <strong>SIO2</strong><br/>  
    <strong>SiO2_err</strong><br/>  
    <strong>SiO2_method</strong><br/>  
    <strong>SiO2_std<strong>
    </td>
  </tr>
  </tbody>
</table> 

2. On Row 2, add subheads for unit (usually `wt%`) to the first two columns (*composition* and *composition*\_err columns) of each set. Refer to the Vocabulary spreadsheet for allowable entries.

3. Fill out the columns:  
##### Reference information  
- **index**, **run_id** - Fill out these columns the same as for the **experimental_conditions** tab. Each row represents an experiment, and the rows must agree across these tabs.  
- **source_pubid** - The publication ID number for the paper in which the sample originated.   
- **sample_mix** - A mixture of the sample; for example, `And0 + Ky0` or `And0 + Sil3 + Crn0`. If the amount (in wt %) is important, include that as well.  
- **contact_geom**  
- **contaminant phases**
-  **notes** - Enter any notes related to information you entered on that row.  

##### Chemistry columns
*Note: You set up these columns in a previous step.*
- *composition* - Enter the compositional measurement.  
- *composition*\_**err** - Enter the error in the amount of the composition. You can use decimal places as well as scientific notations.  
- *composition*\_**method** - Enter the method used to measure the composition (for example, `AAS`, which indicates Atomic Absorption Spectroscopy).  
- *composition*\_**std** - Enter the standard used in the method.  
-  **Total** - Enter the total sum of the compositional values, for a sum close to 100%.   

### rxn tab

The **rxn** tab details the results of the experiments.  

✦ Fill out the columns:

##### Reference information
- **index**, **run_id** -  Fill out these columns the same as the **experimental_conditions** and **sample_material** tabs. Each row represents an experiment, and the rows must agree across these tabs.   

##### Notes  
- Enter any notes related to information you entered on that row.

##### Flux information
- **flux_type** - The type of flux used, if any.    
- **flux_amt** -  The amount of flux used, if any.

##### Reaction information

- **rxn_studied** - The reaction that is studied. Make sure to use only valid phase names from the Vocabulary spreadsheet (**pure_phases_vocab** tab and **solution_phases_vocab** tab). No nicknames! Also, the entry must be a balanced stoichiometric equation.   
- **method** - The method used to determine the results. For example, `weightdiff` (from the Vocabulary spreadsheet) indicates that the authors measured the difference in crystal weights. If the method produces results that are quantitative (such as the  `weightdiff` method), make sure to enter the specifics in the reaction metrics columns.

##### Reaction metrics
   Use these columns to record metrics for results that involve quantitative data (as opposed to qualitative, observational data). These metrics help to determine in which direction the reaction moves. They also help to remove our uncertainty and to make interpretation of the data reproducible.  
   
  <table style="border: 1px solid black;margin-left:30px;margin-bottom:30px;">
  <tr style="border: 1px solid black;margin-left:30px;background-color:#eee">
    <th valign="top"style="padding:10px; width:20%">Column</th>
    <th valign="top"style="padding:10px; width:30%">Usage</th>
    <th valign="top"style="padding:10px; width:25%">Example 1<br />
    <hr>
      Method used: weightdiff (Change in crystal weight)</th>
       <th valign="top"style="padding:10px; width:25%">Example 2<br />
          <hr>
      Method used: xrd-peak-ratio (Ratio of <em>x</em>-ray diffraction peaks)</th> 
  </tr>  
   <tr style="border: 1px solid black;margin-left:30px;">
    <td valign="top"style="padding:10px;"><strong>rxn_metric_unit</strong></td>
    <td valign="top"style="padding:10px;">Unit used to measure the phase reaction</td>
    <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">micrograms</span></td>
    <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">%</span>
    </td>
  </tr>
  
  <tr style="border: 1px solid black;margin-left:30px;">
    <td valign="top"style="padding:10px;"><strong>rxn_metric_zero</strong></td>
    <td valign="top"style="padding:10px;">Zero point of whatever the experiment uses to determine the direction of the phase reaction.</td>
    <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">0</span></td>
    <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">0.465</span></td>
  </tr>
  <tr style="border: 1px solid black;margin-left:30px;">
    <td valign="top" style="padding:10px;"><strong>rxn_metric_err</strong>
      </th>
    <td valign="top"style="padding:10px;">Error needed to identify the direction of the phase reaction.</td>
    <td valign="top"style="padding:10px;"><span style="background-color:#eee;font-family:monospace;">5.8</span> <br />
      * Indicates an allowable error of 5.8 micrograms<br />
      </td>
      <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">20</span><br>
      * Indicates an allowable error of 20%
      </td>
  </tr>
  <tr style="border: 1px solid black;margin-left:30px;">
    <td valign="top" style="padding:10px;"><strong>rxn_metric</strong></th>
    <td valign="top"style="padding:10px">Reaction metric value. How much change there is.</td>
    <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">-6.3</span><br />
      * Indicates a change of -6.3 micrograms
</td>
      <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">3.13</span><br />* Indicates a change of 3.13%</td>
  </tr>
  <tr style="border: 1px solid black;margin-left:30px;">
    <td valign="top" style="padding:10px;"><strong>rxn_metric_resid</strong></th>
    <td valign="top"style="padding:10px;"><p>Reaction metric residual. Compares the reaction metric value to the zero value and determines, within the error, if a direction is apparent. This value is expressed in sigma (the number of error bars away from zero).<br />
    Use this equation to calculate the residual:<br />
      <ul><li>In a simple linear metric (such as weight difference): <br />m-(z/e)<br />
      where <em>m</em> is measurement, <em>z</em> is the zero value, and <em>e</em> is the error.<br /></li>
      <li>In a non-linear metric (such as ratio of x-ray diffraction peaks): \(\log(m/z)/(0.01\cdot\epsilon)\) <br />
      where <em>m</em> is measurement, <em>z</em> is the zero value, and \(\epsilon\) is the error in terms of percent.</li></ul>
     </td>
    <td valign="top"style="padding:10px;"><span style="background-color:#eee;font-family:monospace;">-1.086206897 </span></td>
    <td valign="top"style="padding:10px"><span style="background-color:#eee;font-family:monospace;">9.53375439</span></td>
  </tr>
</table>

##### Results
- **results** - Based on the reaction metrics you entered, describes the experiment's effect on each phase: no change, questionable, increase, or decrease. The results are measured in sigma. (Make sure to use only phase names from the Vocabulary spreadsheet.)

   * No Change (`NC`) -  If the change is less than 1 sigma in either direction, there is no change. Enter `NC`.
    * Questionable (`-?` or `+?`) - A result between 1 and 2 sigma (more than one error bar width but less than two) is considered suggestive but inconclusive. Enter the phase name, a space, and `-?` or `+?`. For example, if Andalusite descreased 1.6 according to the metrics, enter `And -?`. If it increased 1.6, enter `And +?`.  
   * Increase (`+`) - If a phase change is greater than 2 sigma, enter the phase name, a space, and a plus sign. For example, if Andalusite increased more than 2, enter `And +`.  
   * Decrease (`-`) - If a phase change is less than 2 sigma, enter the phase name, a space, and a minus sign. For example, if Andalusite decreased more than 2, enter `And -`.  

 _Multiple phases_    
 If more than one phase is involved, list them all. For example, `Ky + And -` indicates that Kyalite increased and Andalusite decreased.  

- **metastable** -   
- **completion** - (optional) How complete the reaction is, expressed as a percentage. For example, `20` indicates 20% complete.     

## \<Phase\>, melt(if needed), and fluid(if needed) tabs

Use these compositional tabs if you need to describe the bulk composition; for example, if the experiment uses solution phases and involves an unusual mixture.

If the experiments simply mix pure phases, you can leave these tabs blank.

