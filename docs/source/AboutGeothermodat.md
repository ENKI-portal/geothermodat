# About Geothermodat  
 
Geothermodat, part of the ENKI project, is an open database of geological thermodynamic data for model calibration. It is the database used by ThermoEngine (formerly "PhaseObjC").

Creating the Geothermodat database involves digitizing the data from the published literature. The data are input manually into Excel spreadsheets, <a href="SubmitDataViaExcel.html">according to specific guidelines</a>. The `dataio` module then converts the Excel spreadsheets into JSON files for storage in the database. The `dataio`  module can also convert the JSON files back to Excel format for editing if necessary.  

Future plans include replacing the Excel input process with a user-friendly interface.