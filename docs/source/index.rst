
.. Geothermodat documentation master file, created by
   sphinx-quickstart on Wed Jun 14 13:48:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Geothermodat documentation
=========================================

Welcome to the Geothermodat documentation. Here you can find information on using the Geothermodat database as well as contributing data.



Contents
--------

.. toctree::
   :maxdepth: 2

   AboutGeothermodat.md
   SubmitDataViaExcel.md 
   dataio
   test_thermodat
   


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
