#!/usr/bin/env python
""" file: setup.py
    modified: Aaron S. Wolf, University of Michigan
    date: Aug 2, 2017, revised Aug 2, 2017

    description: Distutils installer script for thermodat.
"""
from setuptools import find_packages, setup
setup(
    name="thermodat",
    version="0.1",
    description="Thermodynamic data storage & management tool in Python",
    author="Aaron S. Wolf",
    author_email='aswolf@umich.edu',
    platforms=["any"],  # or more specific, e.g. "win32", "cygwin", "osx"
    license="MIT",
    url="https://gitlab.com/ENKI-portal/geothermodat",
    # packages=[
    #       'thermodat',
    # ],
    packages=find_packages(),
)
