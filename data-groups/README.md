# datagroups
* This directory defines useful data groupings, representing interesting chemical subsystems, or regions of PT space (e.g. simple lower mantle model)
* NOTE: that all raw data is stored in data directory, not here
* This merely defines which raw data files belong to each group
