# Thermodynamic data
* All the raw data are stored within this directory
* data types include:
    * eos high P- and/or T- experiments
        * cold-compression 
        * heated-compression 
        * thermal expansion 
    * heat capacity experiments
    * solubility experiments
    * phase rxn experiments
        * solid phase reversals
        * melt coexistence
* Each paper is stored in a separate directory 
* data stored as csv files
* helper scripts are provided to automatically convert data to/from excel workbooks
* Upon submitting new data, the pubID should be auto generated and stored in a bibtex database
