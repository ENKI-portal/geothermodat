import numpy as np
import pandas as pd
import thermodat
from thermodat import dataio

# from future import print_function
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
from collections import OrderedDict


def get_phase_info():
    common_phs = pd.read_csv('phase_data/common-phases.csv')
    pure_phs = pd.read_csv('phase_data/pure-phases.csv')
    soln_phs = pd.read_csv('phase_data/solution-phases.csv')


    phase_info = {'common': common_phs, 'pure':pure_phs, 'solution':soln_phs}
    return phase_info

def set_phase_chem():
    menu_data = {}

    menu_data['all_phase_info'] = get_phase_info()

    def on_delete_phase(b):
        # , phaseID=0
        # print('enter func = ', menu_data['phase_tabs']._titles)
        ind = menu_data['phase_tabs'].selected_index
        menu_data['phase_tabs'].selected_index = None
        # ind = np.where(np.array(menu_data['phaseIDs'])==phaseID)[0][0]
        # print(menu_data['phasenames'])
        # print(ind)
        # print(menu_data['phase_tabs'])
        tab_list = list(menu_data['phase_tabs'].children)
        # print(menu_data['phase_tabs'])

        menu_data['phasenames'].pop(ind)
        menu_data['phases'].pop(ind)
        # menu_data['phaseIDs'].pop(ind)
        tab_list.pop(ind)
        menu_data['phase_tabs'].children = tab_list


        for ind, phase in enumerate(menu_data['phasenames']):
            menu_data['phase_tabs'].set_title(ind, phase)

        menu_data['phase_tabs'].set_title(len(menu_data['phasenames']),None)

        Nphase = len(menu_data['phasenames'])
        # print('Nphase = ', Nphase)
        titles = menu_data['phase_tabs']._titles
        # print('pre = ',titles)
        del titles[str(Nphase)]
        # print('post delete = ',titles)
        menu_data['phase_tabs']._titles = titles

        # print('post set titles = ',menu_data['phase_tabs']._titles)

        # print(len(menu_data['phasenames']))
        # print(len(menu_data['phases']))
        # print(len(menu_data['phaseIDs']))
        # print(len(menu_data['phase_tabs'].children))

        if len(titles) >0:
            menu_data['phase_tabs'].selected_index = np.minimum(ind,Nphase-1)

    def on_add_phase(b):
        Nphase_init = len(menu_data['phase_tabs'].children)
        phase = menu_data['phasename_input'].value
        phase_info = menu_data['phase_info']

        if phase_info is None:
            phase_abbrev = phase
        else:
            phs_index = menu_data['phasename_input'].index
            phase_abbrev = phase_info['abbrev'][phs_index]

        phases_temp = list(menu_data['phase_tabs'].children)
        phase_chem = _new_phase_chem(phase, phase_abbrev)
        # menu_data['phaseID_last'] += 1
        # phaseID = menu_data['phaseID_last']
        # menu_data['phaseIDs'].append(phaseID)


        # phase_chem['delete_phase'].on_click(
        #     lambda b, phaseID=phaseID:on_delete_phase(b, phaseID=phaseID))
        phase_chem['delete_phase'].on_click(on_delete_phase)

        # chem_comp = _get_chem_comp()
        # phases_temp.append(chem_comp['panel'])
        phases_temp.append(phase_chem['Box'])
        menu_data['phase_tabs'].children = phases_temp
        menu_data['phasenames'].append(phase)
        menu_data['phases'].append(phase_chem)

        menu_data['phase_tabs'].set_title(Nphase_init, phase_abbrev)

    phase_types = ['common-phase','pure-phase','solution-phase','custom-phase']
    menu_data['phase_typ_selector'] = widgets.Dropdown(
        options=phase_types, layout=widgets.Layout(width='150px'))

    def on_change_phase_typ(change):
        all_phase_info = menu_data['all_phase_info']
        typ = menu_data['phase_typ_selector'].value
        if typ=='common-phase':
            menu_data['phase_info'] = all_phase_info['common']
            menu_data['phasename_dropdown'].options = menu_data['phase_info']['name']
            menu_data['phasename_input'] = menu_data['phasename_dropdown']
        elif typ=='pure-phase':
            menu_data['phase_info'] = all_phase_info['pure']
            menu_data['phasename_dropdown'].options = menu_data['phase_info']['name']
            menu_data['phasename_input'] = menu_data['phasename_dropdown']
        elif typ=='solution-phase':
            menu_data['phase_info'] = all_phase_info['solution']
            menu_data['phasename_dropdown'].options = menu_data['phase_info']['name']
            menu_data['phasename_input'] = menu_data['phasename_dropdown']
        elif typ=='custom-phase':
            menu_data['phasename_input'] = menu_data['phasename_text']
            menu_data['phase_info'] = None
        else:
            print('not valid!')

        opts = list(menu_data['add_phase'].children)
        #print(len(opts))
        opts.pop(1)
        opts.insert(1, menu_data['phasename_input'])
        menu_data['add_phase'].children = opts


    menu_data['phasenames'] = []
    menu_data['phases'] = []
    # menu_data['phaseIDs'] = []
    # menu_data['phaseID_last'] = -1


    # children = [widgets.Text(description=name) for name in tab_contents]
    # children = [_get_chem_comp() for name in menu_data['phases']]
    menu_data['phase_tabs'] = widgets.Tab()
    menu_data['phase_tabs'].children = []
    # for i, phs in enumerate(menu_data['phases']):
    #     tab.set_title(i, phs)

    phasename_dropdown = widgets.Dropdown(
        options=menu_data['all_phase_info']['common']['name'], layout=widgets.Layout(width='300px'))

    menu_data['phase_info'] = menu_data['all_phase_info']['common']
    phasename_text = widgets.Text(value='')

    add_phase_button = widgets.Button(description='Add-Phase')
    menu_data['phasename_dropdown'] = phasename_dropdown
    menu_data['phasename_text'] = phasename_text
    menu_data['phasename_input'] = phasename_dropdown
    menu_data['add_phase_button'] = add_phase_button


    add_phase_button.on_click(on_add_phase)
    menu_data['phase_typ_selector'].observe(on_change_phase_typ)

    add_phase = widgets.HBox(
        children=[menu_data['phase_typ_selector'],phasename_dropdown,add_phase_button])
    menu_data['add_phase'] = add_phase
    menu = widgets.VBox(
        children=[add_phase, menu_data['phase_tabs']])
    menu_data['menu'] = menu

    display(menu)

    return menu_data

def _get_chem_comp():
    chem_comp = {}
    def set_chem_dropdown(options):
        chem_comp['chem'].options = options
        chem_comp['chem'].value = options[0]

    def on_chem_typ_change(change):
        chem_pos = 3
        chem_typ = chem_comp['chem_typ'].value
        members = list(chem_comp['panel'].children)
        members.pop(chem_pos)
        if chem_typ == 'oxide':
            chem_comp['chem']=chem_comp['std-chem']
            set_chem_dropdown(chem_comp['oxides'])
        elif chem_typ == 'element':
            chem_comp['chem']=chem_comp['std-chem']
            set_chem_dropdown(chem_comp['elems'])
        elif chem_typ == 'custom':
            chem_comp['chem']=chem_comp['custom-chem']
            chem_comp['chem'].value = ''
            # members.insert(chem_pos, chem_comp['custom-chem'])
            # print('kill')
        else:
            print('not valid')

        members.insert(chem_pos, chem_comp['chem'])
        chem_comp['panel'].children = members
        on_chem_select(None)

    def on_chem_select(change):
        chem_select = chem_comp['chem'].value
        report_as = chem_comp['as_total'].value
        # print(report_as)

        chem_lbl = chem_select
        if report_as=='total':
            chem_lbl = chem_lbl +'(tot)'

        chem_comp['label'].value = chem_lbl
        chem_comp['comp_str'] = chem_lbl


    oxides = ['SiO2','MgO','Al2O3','H2O','CO2','FeO','Fe2O3','CaO',
              'Na2O','K2O','TiO2','MnO','NiO','Cr2O3','P2O5',]
    elems = ['Sc','Ti','V','Zn',
             'Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd',
             'La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy',
             'Ho','Er','Tm','Yb','Lu','Hf','Ta']
    chem_typ = ['oxide','element','custom']
    # major_elems = ['Fe','Al','Si','Mg']
    # trace_elems = ['Zn','U']
    report_as = ['measured','total']


    chem_comp['oxides'] = oxides
    chem_comp['elems'] = elems
    # chem_comp['trace-elems'] = trace_elems

    chem_comp['delete'] = widgets.Button(
        icon='minus', layout=widgets.Layout(width='50px'))

    chem_comp['label'] = widgets.Label(
        value=oxides[0], layout=widgets.Layout(width='200px'))
    chem_comp['custom-chem'] = widgets.Text(
        value=oxides[0], layout=widgets.Layout(),
    )
    chem_comp['std-chem'] = widgets.Dropdown(options=oxides)

    chem_comp['comp_str'] = chem_comp['label'].value
    chem_comp['chem_typ'] = widgets.Dropdown(options=chem_typ)
    chem_comp['chem'] = chem_comp['std-chem']
    # chem_comp['as_total'] = widgets.Checkbox(
    #     value=False, description='as total')
    chem_comp['as_total'] = widgets.RadioButtons(
        options=['measured','total'])


    comp_panel = widgets.HBox(children=[
        chem_comp['delete'], chem_comp['label'],
        chem_comp['chem_typ'], chem_comp['chem'], chem_comp['as_total']
        ] )




    chem_comp['chem_typ'].observe(on_chem_typ_change)
    chem_comp['chem'].observe(on_chem_select)
    chem_comp['custom-chem'].observe(on_chem_select)
    chem_comp['as_total'].observe(on_chem_select)

    chem_comp['panel'] = comp_panel

    return chem_comp

def _new_phase_chem(phase, phase_abbrev):
    phase_chem = {}

    def on_add_chem_comp(b):
        chem_comp_Box = list(phase_chem['Box'].children)
        new_chem_comp = _get_chem_comp()
        phase_chem['compID_last'] += 1
        compID = phase_chem['compID_last']
        # print(phase_chem['compIDs'])
        phase_chem['compIDs'].append(compID)

        new_chem_comp['delete'].on_click(
            lambda b, compID=compID:on_delete_chem_comp(b, compID=compID))

        chem_comp_Box.insert(-1, new_chem_comp['panel'])
        phase_chem['comp_list'].append(new_chem_comp)
        phase_chem['Box'].children = chem_comp_Box

    def on_delete_chem_comp(b, compID=0):
        ind = np.where(np.array(phase_chem['compIDs'])==compID)[0][0]
        # print('matching ind = ',ind)
        # print(phase_chem['compIDs'])
        del phase_chem['compIDs'][ind]
        phase_chem['comp_list'].pop(ind)
        box_list = list(phase_chem['Box'].children)
        box_list.pop(ind)
        phase_chem['Box'].children = box_list

    add_chem_comp = widgets.Button(
        icon='plus', layout=widgets.Layout(width='40px'))
    # delete_phase = widgets.Button(
    #     description='Delete', layout=widgets.Layout(margin='0 0 0 200px'))
    delete_phase = widgets.Button(
        description='Delete Phase')

    add_chem_comp.on_click(on_add_chem_comp)

    phase_chem['is_starter_phase'] = widgets.ToggleButton(
        description='starter_phase', value=True,
        tooltip='This defines the initial chemistry components of this starter phase '+
        '(used in the sample mix OR to track pure phase reactions/EOS behavior).')
    phase_chem['is_rxn_solution_phase'] = widgets.ToggleButton(
        description='rxn_solution_phase', value=False,
        tooltip='This defines the final chemistry components of this reaction solution phase.')


    phase_chem['set_phase_appearance'] = widgets.HBox(
        [widgets.HTML(value='Choose one or both:'), phase_chem['is_starter_phase'],
         phase_chem['is_rxn_solution_phase']],
        layout=widgets.Layout(align_items='flex-start')
    )

    phase_chem['add_chem_comp'] = add_chem_comp
    phase_chem['delete_phase'] = delete_phase

    phase_chem['name'] = phase
    phase_chem['abbrev'] = phase_abbrev
    phase_chem['Box'] = widgets.VBox()
    phase_chem['modify_phase_chem'] = widgets.HBox(
        [phase_chem['add_chem_comp'],phase_chem['delete_phase']],
        layout=widgets.Layout(justify_content='space-between'))

    phase_chem['Box'].children = [
        widgets.HTML(value="<b>"+phase+"</b>",layout=widgets.Layout(width='250px')),
        phase_chem['set_phase_appearance'], phase_chem['modify_phase_chem'],]
    phase_chem['comp_list'] = []
    phase_chem['compIDs'] = []
    phase_chem['compID_last'] = -1

    on_add_chem_comp(None)
    # chem_comp = _get_chem_comp()
    # phase_chem['comp_list'] = [chem_comp]
    # phase_chem['Box'].children = [
    #    chem_comp['panel'], phase_chem['add_chem_comp']]
    return phase_chem

def extract_exp_chem(menu_data):
    exp_chem = {}

    exp_chem['phases'] = []
    exp_chem['abbrevs'] = []
    exp_chem['chem_comps'] = []
    exp_chem['starter_chem_comps'] = set([])
    exp_chem['is_starter_phase'] = []
    exp_chem['is_rxn_solution_phase'] = []

    for phasenm, phase in zip(menu_data['phasenames'],
                              menu_data['phases']):
        iabbrev = phase['abbrev']
        #print(len(phase['comp_list']))
        #print(phase['comp_list'][0]['comp_str'])
        #print('=======')

        # comp_list = phase['comp_list']
        # print(comp_list)
        ichem_comps = [comp['comp_str'] for comp in phase['comp_list']]
        istarter = phase['is_starter_phase'].value
        irxn_soln = phase['is_rxn_solution_phase'].value

        # print(istarter)
        exp_chem['phases'].append(phasenm)
        exp_chem['abbrevs'].append(iabbrev)
        exp_chem['chem_comps'].append(ichem_comps)
        if istarter:
            exp_chem['starter_chem_comps'] = exp_chem['starter_chem_comps'].union(set(ichem_comps))
        exp_chem['is_starter_phase'].append(istarter)
        exp_chem['is_rxn_solution_phase'].append(irxn_soln)

    return exp_chem

def create_excel_template(menu_data):
    exp_chem = extract_exp_chem(menu_data)
    template_data = {}

    filetype_dropdown = widgets.Dropdown(description='Filetype:',
                                         options=['phase-rxn','eos'])

    template_data['filetype_dropdown'] = filetype_dropdown
    filename_box = widgets.Text(description='Filename:',
                                value='template')

    template_data['filename_box'] = filename_box
    ok_button = widgets.Button(description='OK')

    template_data['ok_button'] = ok_button

    make_template_gui = widgets.HBox(
        children=[filetype_dropdown, filename_box, ok_button])


    def on_change_filetype(change):
        filetype = template_data['filetype_dropdown'].value
        template_data['filename_box'].value = filetype+'-template'

    def on_create_template(change):
        filetype = template_data['filetype_dropdown'].value
        filename = template_data['filename_box'].value + '.xlsx'

        if filetype=='phase-rxn':
            dataio.write_phase_rxn_template(filename, exp_chem)
        elif filetype=='eos':
            dataio.write_eos_template(filename, exp_chem)
        else:
            assert False, 'Not a valid choice.'

        pass


    template_data['filetype_dropdown'].observe(on_change_filetype)
    template_data['ok_button'].on_click(on_create_template)
    on_change_filetype(None)

    display(make_template_gui)
