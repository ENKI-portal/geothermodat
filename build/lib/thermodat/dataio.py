"""
This module handles input/ouput for thermodynamic databases.

"""
import json
import numpy as np
import pandas as pd
from os import path
import matplotlib.pyplot as plt

import openpyxl
from openpyxl.styles.borders import Border, Side

import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from collections import OrderedDict

__all__ = ['extract_table_data', 'extract_unstructured_data', 'read_json_data',
           'read_xls_data', 'json_to_xls', 'xls_to_json']


def _extract_data_units(df, unitless=True):
    if unitless:
        units = None
    else:
        fields = list(df.columns)
        units_list = list(df.iloc[0])
        units = OrderedDict()
        for field, unit in zip(fields, units_list):
            units[field] = unit

        df.drop(df.index[0], inplace=True) # drop units row after saving

    return units

def _extract_data_values(df):
    data_str = df.to_json(orient='records')
    json_str = '{"data": '+data_str+'}'
    data_tbl=json.loads(json_str, object_pairs_hook=OrderedDict)['data']
    return data_tbl

def extract_table_data(sheet, df_book, unitless=True):
    df = df_book[sheet].copy()

    fields = list(df.columns)
    units = _extract_data_units(df, unitless=unitless)
    data_tbl = _extract_data_values(df)

    data_dict = OrderedDict()
    data_dict['name'] = sheet
    data_dict['fields'] = fields
    data_dict['units'] = units
    data_dict['data'] = data_tbl
    return data_dict

def extract_unstructured_data(sheet, df_book):
    df = df_book[sheet].copy()

    data_str = df.to_json(orient='records')
    json_str = '{"data": '+data_str+'}'
    data_tbl=json.loads(json_str, object_pairs_hook=OrderedDict)['data']

    data_dict = OrderedDict()
    data_dict['name'] = sheet
    data_dict['data'] = data_tbl
    return data_dict

def read_json_data(filename):
    with open(filename,'r') as f:
        json_dat = json.load(f, object_pairs_hook=OrderedDict)

    return json_dat

def read_xls_data(filename):
    df_book = pd.read_excel(filename, sheetname=None)
    sheetnames = list(df_book.keys())

    workbook_data = OrderedDict()
    for sheet in sheetnames:
        if sheet=='reference':
            data = extract_table_data(sheet, df_book, unitless=True)
        elif sheet=='notes':
            data = extract_unstructured_data(sheet, df_book)
        else:
            data = extract_table_data(sheet, df_book, unitless=False)

        workbook_data[sheet] = data

    return workbook_data

def json_to_xls(filename, xls_filename=None):

    if xls_filename is None:
        basename, extname = path.splitext(filename)
        xls_filename = basename+'.xlsx'

    json_data = read_json_data(filename)

    writer = pd.ExcelWriter(xls_filename)
    sheets = list(json_data.keys())
    for sheet in sheets:
        data = json_data[sheet]
        df = pd.DataFrame(data['data'])
        if 'fields' in data:
            fields = data['fields']
            units = data['units']
            if units is not None:
                # df = pd.concat([pd.DataFrame(units), df])
                df.loc[-1] = [units[key] for key in units]
                df.index = df.index + 1  # shifting index
                df.sort_index(inplace=True)


        df.to_excel(writer,sheet_name=sheet,index=False)
        worksheet = writer.sheets[sheet]
        # from IPython import embed; embed(); import ipbd; ipdb.set_trace()
        # worksheet.set_column(col_range,col_width)
        # worksheet.column_dimensions['A'].width=col_width
        adjust_worksheet_column_widths(worksheet)
        if sheet == 'reference':
            header_rows = 1
        elif sheet == 'notes':
            header_rows = 0
        else:
            header_rows = 2
        adjust_worksheet_row_colors(worksheet, header_rows=header_rows)

    writer.close()
    pass

def adjust_worksheet_column_widths(worksheet, fac=1.1):
    def as_text(value):
        if value is None:
            return ""
        return str(value)

    for column_cells in worksheet.columns:
        length = max(len(as_text(cell.value)) for cell in column_cells)
        length = np.ceil(fac*length)
        worksheet.column_dimensions[column_cells[0].column].width = length

    pass

def adjust_worksheet_row_colors(worksheet, header_rows=2):
    # redFill = PatternFill(start_color='FFFF0000', end_color='FFFF0000',
    #                       fill_type='solid')
    # ws['A1'].style = redFill
    #F9E2E2
    # my_red = openpyxl.styles.colors.Color(rgb='00FF0000')
    thin_border = Border(left=Side(style='thin'), right=Side(style='thin'),
                         top=Side(style='thin'), bottom=Side(style='thin'))


    my_red = openpyxl.styles.colors.Color(rgb='00F9E2E2')
    my_fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=my_red)
    for row_ind, row_cells in enumerate(worksheet.rows):
        if row_ind >= header_rows:
            break
        # worksheet.column_dimensions[column_cells[0].column].width = length
        for cell in row_cells:
            cell.fill = my_fill
            cell.border = thin_border

    pass

def xls_to_json(filename, json_filename=None):
    if json_filename is None:
        basename, extname = path.splitext(filename)
        json_filename = basename+'.json'

    workbook_data = read_xls_data(filename)
    s = json.dumps(workbook_data, indent=4)
    with open(json_filename,'w') as f:
        f.write(s)

    pass


#####################################################
def get_comp_list(type='major_elems'):
    if type=='major_oxides':
        comp = ['Al2O3', 'CO2', 'CaO', 'FeO', 'Fe2O3', 'H2O', 'K2O', 'MgO', 'MnO',
                'MnO2', 'Na2O', 'P2O5','SO3', 'SiO2', 'TiO2', 'Ti2O3']
    elif type=='all_oxides':
        comp = ['Al2O3', 'As2O3', 'Au2O', 'B2O3', 'BaO', 'BeO', 'CO2', 'CaO',
                'CeO2', 'Ce2O3', 'CoO', 'Cr2O3', 'Cs2O', 'CuO', 'Dy2O3', 'Er2O3',
                'EuO', 'Eu2O3', 'FeO', 'Fe2O3', 'Ga2O3', 'Gd2O3', 'GeO2', 'H2O',
                'HfO2', 'Ho2O3', 'K2O', 'La2O3', 'Li2O', 'Lu2O3', 'MgO', 'MnO',
                'MnO2', 'Mn3O4', 'MoO3', 'Na2O', 'Nb2O5', 'Nd2O3', 'NiO', 'P2O5',
                'PbO', 'Pr2O3', 'Rb2O', 'SO3', 'Sb2O3', 'Sc2O3', 'SiO2', 'Sm2O3',
                'SnO2', 'SrO', 'Ta2O5', 'Tb2O3', 'ThO2', 'TiO2', 'Ti2O3', 'Tm2O3',
                'UO2', 'U3O8', 'V2O5', 'WO3', 'Y2O3', 'Yb2O3', 'ZnO', 'ZrO2']
    elif type=='major_elems':
        comp = ['Si', 'Al', 'Ca', 'Mg', 'Na', 'K', 'Ti', 'Fe', 'Mn', 'P',
                'Cu', 'Zn', 'O']
    elif type=='minor_elems':
        comp = ['Ag','As', 'B', 'Ba', 'Bi', 'Cd', 'Co', 'Cr', 'Hg', 'Li',
                'Mn', 'Mo', 'Ni', 'Pb', 'Sb', 'Se', 'Sn', 'Sr', 'V', 'Zn']
    elif type=='all_elems':
        comp = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg',
                'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V',
                'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se',
                'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh',
                'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba',
                'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy',
                'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir',
                'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra',
                'Ac', 'Th', 'Pa', 'U']
    else:
        raise LookupError(type + ' is not a valid comp type. Try again.')

    return comp

def get_header_format(wb, header_color='F9E2E2'):
    header_fmt = wb.add_format({'bold': True, 'bg_color': header_color})
    return header_fmt

def get_unit_list(units):
    if units=='length':
        unit_list = ['cm','mm','micron']
    elif units=='weight':
        unit_list = ['g','mg','microgram']
    elif units=='amount':
        unit_list = ['wt%','mol%','vol%','log10molal','wtppm','wtppb','molppm',
                     'molppb','molal']
    elif units=='pressure':
        unit_list = ['kbar','bar','GPa','Pa','TPa','Mbar']
    elif units=='temperature':
        unit_list = ['K','C','F']
    elif units=='fO2':
        unit_list = ['log10bar','fO2-IW','fO2-NNO','fO2-MMO','fO2-QFM','fO2-MW',
                     'fO2-GCH','fO2-HM','fO2-Cu+Cu2O','fO2-CCO']
    elif units=='time':
        unit_list = ['hr','s','min','day']
    elif units=='fraction':
        unit_list = ['%','decimal']
    else:
        unit_list = ['-']

    return unit_list

def add_column(ws, header_fmt, title, units=None, key_options=None,
               col_comment=None, Nrows=300, column=0):

    if key_options is not None:
        add_column_options(ws, key_options, Nrows=Nrows, column=column)

    column = add_column_header(ws, header_fmt, title, units=units,
                               col_comment=col_comment, column=column)
    return column

def add_column_options(ws, key_options, Nrows=300, column=0):
    for ientry in range(Nrows):
        # ws.write(ientry+2, column, header_fmt)
        unit_cell = xl_rowcol_to_cell(ientry+2, column)
        ws.data_validation(unit_cell, {'validate': 'list',
                                       'source': key_options})

    pass

def add_column_header(ws, header_fmt, title, units=None, col_comment=None,
                      unit_cond=None, column=0):
    ws.write(0, column, title, header_fmt)

    if col_comment is not None:
        ws.write_comment(xl_rowcol_to_cell(0, column), col_comment,
                         {'y_scale':3.0})

    if units is None:
        ws.write(1, column, None, header_fmt)
    else:
        unit_list = get_unit_list(units)
        if unit_cond is None:
            ws.write(1, column, unit_list[0], header_fmt)
        else:
            unit_formula = unit_cond.format(unit_list[0])
            ws.write_formula(1, column, unit_formula, header_fmt)
        if len(unit_list)>1:
            ws.write(1, column, unit_list[0], header_fmt)
            unit_cell = xl_rowcol_to_cell(1, column)
            ws.data_validation(unit_cell, {'validate': 'list',
                                           'source': unit_list})

    return column+1

def add_chem_headers(ws, chem_cols, header_fmt, column=0, Nmax=50):

    for ichem in chem_cols :

        # icomp_cell = chem_sheet+'!'+xl_rowcol_to_cell(1+irow, 1)
        # itot_cell = chem_sheet+'!'+xl_rowcol_to_cell(1+irow, 2)

        # ititle = [
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)",'+
        #      '{0}))').format(icomp_cell,itot_cell),
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)_err",'+
        #      '{0}&"_err"))').format(icomp_cell,itot_cell),
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)_method",'+
        #      '{0}&"_method"))').format(icomp_cell,itot_cell),
        #     ('=IF(ISBLANK({0}),"-", IF({1}="total",{0}&"(tot)_std",'+
        #      '{0}&"_std"))').format(icomp_cell, itot_cell)
        #     ]
        ititle = [
            (ichem),
            (ichem+'_err'),
            (ichem+'_method'),
            (ichem+'_std'),
        ]
        # unit_cond = '=IF(ISBLANK('+icomp_cell+'),"",{0})'
        column = add_column_header(ws, header_fmt, ititle[0], units='amount', column=column)
        column = add_column_header(ws, header_fmt, ititle[1], units='amount', column=column)
        column = add_column_header(ws, header_fmt, ititle[2], units='amount', column=column)
        column = add_column_header(ws, header_fmt, ititle[3], units='amount', column=column)

    return column+1

#####################################################

def write_eos_template(filename, exp_chem):
    data_options = {}
    data_options['rxn_method_opts']  = [
        'xrd-peak-ratio','weightdiff','Optical','no-info']
    data_options['rxn_metric_units'] = ['-','microgram','area%']
    data_options['status_opts'] = ['Yes','No','Maybe']
    data_options['flux_opts'] = ['water','none','molten-salt','no-info']
    data_options['contact_geom'] = ['mixed-grains','planar','single-xtal']
    data_options['press_medium'] = ['water','Ar-gas','Ne-gas',
                                    'He-gas','no-info']
    data_options['P_meas_method'] = ['resist','tube-guage',
                                     'xrd-vol-eos','no-info']
    data_options['T_meas_samp_loc'] = ['touching','adjacent','separate',
                                       'no-info']
    data_options['T_meas_heat_loc'] = ['center','axial','off-axis','aligned',
                                       'no-info']

    data_options['fO2_comment'] = (
        "log10bar: Absolute units log10(bars)\n"+
        "fO2-IW: Iron-wustite buffer\n"+
        "fO2-NNO: Nickel-nickel oxide buffer\n"+
        "fO2-MMO: MnO-Mn3O4 buffer\n"+
        "fO2-QFM: Quartz-fayalite-magnetite buffer\n"+
        "fO2-MW: Magnetite-wustite buffer\n"+
        "fO2-GCH: Graphite-methane buffer\n"+
        "fO2-HM: Hematite-magnetite buffer\n"+
        "fO2-Cu+Cu2O: Copper-copper oxide buffer\n"+
        "fO2-CCO: Graphite-carbon monoxide buffer\n"+
        "no_info: No information provided")
    data_options['device_comment'] = (
        'no_info: No information provided\n'+
        'ambient: Ambient conditions measurement\n'+
        '1-atm: One atmosphere furnace\n'+
        'Belt: Belt apparatus\n'+
        'CSPV: Cold-seal pressure vessel\n'+
        'IHPV: Internally heated pressure vessel\n'+
        'MA: Multi-anvil apparatus\n'+
        'PC: Piston-cylinder apparatus\n'+
        'RQAC: Rapid quench autoclave\n'+
        'LHDAC: Laser heated diamond anvil cell\n'+
        'Gas: Gas-medium pressure apparatus\n'+
        'DAC300: 300K DAC')

    data_options['device'] = ['no_info', 'ambient', '1-atm', 'Belt', 'CSPV',
        'IHPV', 'MA', 'PC', 'RQAC', 'LHDAC', 'Gas', 'DAC300']


    wb = xlsxwriter.Workbook(filename)

    write_sheet_ref(wb, data_options)
    write_sheet_notes(wb, data_options)
    # write_sheet_chem(wb)
    write_sheet_start_phs(wb, exp_chem, data_options)
    write_sheet_exp_cond(wb, data_options)
    write_sheet_samp_material(wb, data_options)
    write_sheet_rxn(wb, data_options)
    # write_sheet_phase(wb)
    # write_sheet_fluid(wb)
    # write_sheet_melt(wb)
    wb.close()

def write_phase_rxn_template(filename, exp_chem):
    data_options = {}
    data_options['rxn_method_opts']  = [
        'xrd-peak-ratio','weightdiff','Optical','no-info']
    data_options['rxn_metric_units'] = ['-','microgram','area%']
    data_options['status_opts'] = ['Yes','No','Maybe']
    data_options['flux_opts'] = ['water','none','molten-salt','no-info']
    data_options['contact_geom'] = ['mixed-grains','planar','single-xtal']
    data_options['press_medium'] = ['water','Ar-gas','Ne-gas',
                                    'He-gas','no-info']
    data_options['P_meas_method'] = ['resist','tube-guage',
                                     'xrd-vol-eos','no-info']
    data_options['T_meas_samp_loc'] = ['touching','adjacent','separate',
                                       'no-info']
    data_options['T_meas_heat_loc'] = ['center','axial','off-axis','aligned',
                                       'no-info']

    data_options['fO2_comment'] = (
        "log10bar: Absolute units log10(bars)\n"+
        "fO2-IW: Iron-wustite buffer\n"+
        "fO2-NNO: Nickel-nickel oxide buffer\n"+
        "fO2-MMO: MnO-Mn3O4 buffer\n"+
        "fO2-QFM: Quartz-fayalite-magnetite buffer\n"+
        "fO2-MW: Magnetite-wustite buffer\n"+
        "fO2-GCH: Graphite-methane buffer\n"+
        "fO2-HM: Hematite-magnetite buffer\n"+
        "fO2-Cu+Cu2O: Copper-copper oxide buffer\n"+
        "fO2-CCO: Graphite-carbon monoxide buffer\n"+
        "no_info: No information provided")
    data_options['device_comment'] = (
        'no_info: No information provided\n'+
        'ambient: Ambient conditions measurement\n'+
        '1-atm: One atmosphere furnace\n'+
        'Belt: Belt apparatus\n'+
        'CSPV: Cold-seal pressure vessel\n'+
        'IHPV: Internally heated pressure vessel\n'+
        'MA: Multi-anvil apparatus\n'+
        'PC: Piston-cylinder apparatus\n'+
        'RQAC: Rapid quench autoclave\n'+
        'LHDAC: Laser heated diamond anvil cell\n'+
        'Gas: Gas-medium pressure apparatus\n'+
        'DAC300: 300K DAC')

    data_options['device'] = ['no_info', 'ambient', '1-atm', 'Belt', 'CSPV',
        'IHPV', 'MA', 'PC', 'RQAC', 'LHDAC', 'Gas', 'DAC300']


    wb = xlsxwriter.Workbook(filename)

    write_sheet_ref(wb, data_options)
    write_sheet_notes(wb, data_options)
    # write_sheet_chem(wb)
    write_sheet_start_phs(wb, exp_chem, data_options)
    write_sheet_exp_cond(wb, data_options)
    write_sheet_samp_material(wb, data_options)
    write_sheet_rxn(wb, data_options)
    # write_sheet_phase(wb)
    # write_sheet_fluid(wb)
    # write_sheet_melt(wb)
    wb.close()

def write_sheet_ref(wb, data_options, header_color='F9E2E2', col_width=20):
    header_fmt = get_header_format(wb, header_color=header_color)

    ws = wb.add_worksheet('reference')

    column = 0
    column = add_column_header(ws, header_fmt, 'pub_id', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'authors', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'date', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'title', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'journal', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'volume', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'pages', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'doi', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'notes', units=None, column=column)

    ws.set_column(0, column-1, col_width)
    pass

def write_sheet_notes(wb, data_options, header_color='F9E2E2', col_width=20):
    header_fmt = get_header_format(wb, header_color=header_color)

    ws = wb.add_worksheet('notes')
    ws.write('A1', 'notes', header_fmt)
    pass

def write_sheet_start_phs(wb, exp_chem, data_options,
                          header_color='F9E2E2', col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)

    ws = wb.add_worksheet('starter_phase')

    column = 0
    column = add_column_header(ws, header_fmt, 'sample_id', units=None, column=column)
    column, sample_name_col = (add_column_header(ws, header_fmt, 'sample_name', units=None, column=column), column)
    column, phase_col = (add_column_header(ws, header_fmt, 'phase', units=None, column=column), column)
    column = add_column_header(ws, header_fmt, 'source_pubid', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'notes', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'table_num', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'figure_num', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'page_num', units=None, column=column)

    column = add_column_header(ws, header_fmt, 'synthetic', units=None, column=column)
    column = add_column_header(ws, header_fmt, 'contaminant_phases', units='amount', column=column)
    column = add_column_header(ws, header_fmt, 'Total', units='amount', column=column)

    chem_cols = exp_chem['starter_chem_comps']
    column = add_chem_headers(ws, chem_cols, header_fmt, column=column, Nmax=50)
    # column = add_column_header(ws, header_fmt, 'Total', units='amount', column=column)
    ws.set_column(0, column-1, col_width)

    # write rows
    phases = np.array(exp_chem['phases'])
    is_starter = np.array(exp_chem['is_starter_phase'])
    # print(is_starter)
    starter_phases = phases[is_starter]
    # print(starter_phases)

    irows=2
    for phs in starter_phases:
        ws.write(irows, sample_name_col, phs)
        ws.write(irows, phase_col, phs)
        irows += 1

    pass

def write_sheet_exp_cond(wb, data_options, header_color='F9E2E2',
                         col_width=20):
    header_fmt = get_header_format(wb, header_color=header_color)
    ws = wb.add_worksheet('exp_conditions')

    column = 0
    column = add_column_header(ws, header_fmt, 'index', column=column)
    column = add_column_header(ws, header_fmt, 'run_id', column=column)
    column = add_column_header(ws, header_fmt, 'lab_id', column=column)
    column = add_column_header(ws, header_fmt, 'source_pubid', column=column)
    column = add_column_header(ws, header_fmt, 'notes', column=column)
    column = add_column_header(ws, header_fmt, 'table_num', column=column)
    column = add_column_header(ws, header_fmt, 'figure_num', column=column)
    column = add_column_header(ws, header_fmt, 'page_num', column=column)
    column = add_column(ws, header_fmt, 'device', column=column,
                        key_options=data_options['device'],
                        col_comment=data_options['device_comment'])
    column = add_column_header(ws, header_fmt, 'container', column=column)
    column = add_column_header(ws, header_fmt, 'container_width',
                               units='length', column=column)
    column = add_column_header(ws, header_fmt, 'container_length',
                               units='length', column=column)
    column = add_column_header(ws, header_fmt, 'press_medium', column=column)
    column = add_column_header(ws, header_fmt, 'P_meas_method', column=column)
    column = add_column_header(ws, header_fmt, 'T_meas_method', column=column)
    column = add_column_header(ws, header_fmt, 'T_meas_samp_loc', column=column)
    column = add_column_header(ws, header_fmt, 'P', units='pressure',column=column)
    column = add_column_header(ws, header_fmt, 'P_err', units='pressure', column=column)
    column = add_column_header(ws, header_fmt, 'T', units='temperature', column=column)
    column = add_column_header(ws, header_fmt, 'T_err', units='temperature', column=column)
    column = add_column_header(ws, header_fmt, 'fO2', units='fO2', column=column, col_comment=data_options['fO2_comment'])
    column = add_column_header(ws, header_fmt, 'equil_time', units='time', column=column)

    ws.set_column(0, column-1, col_width)
    # adjust_worksheet_column_widths(ws, fac=1.1)
    pass

def write_sheet_samp_material(wb, data_options, header_color='F9E2E2',
                              col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)
    ws = wb.add_worksheet('sample_material')

    column = 0
    column = add_column_header(ws, header_fmt, 'index', column=column)
    column = add_column_header(ws, header_fmt, 'run_id', column=column)
    column = add_column_header(ws, header_fmt, 'source_pubid', column=column)
    column = add_column_header(ws, header_fmt, 'sample_mix', column=column)
    column = add_column_header(ws, header_fmt, 'other_phases', column=column)
    column = add_column_header(ws, header_fmt, 'notes', column=column)

    ws.set_column(0, column-1, col_width)

def write_sheet_rxn(wb, data_options,header_color='F9E2E2', col_width=20):

    header_fmt = get_header_format(wb, header_color=header_color)
    ws = wb.add_worksheet('rxn')

    column = 0
    column = add_column_header(ws, header_fmt, 'index', column=column)
    column = add_column_header(ws, header_fmt, 'run_id', column=column)
    column = add_column_header(ws, header_fmt, 'rxn_studied', column=column)


    column = add_column(ws, header_fmt, 'metastable',
                        key_options=data_options['status_opts'], column=column)
    column = add_column(ws, header_fmt, 'trust',
                        key_options=data_options['status_opts'], column=column)
    column = add_column_header(ws, header_fmt, 'notes', column=column)
    column = add_column_header(ws, header_fmt, 'flux_amt', units='amount',column=column)
    column = add_column(ws, header_fmt, 'flux_type',
                        key_options=data_options['flux_opts'], column=column)
    column = add_column_header(ws, header_fmt, 'contact_geom', column=column)
    column = add_column_header(ws, header_fmt, 'grain_size', units='length',column=column)
    column = add_column_header(ws, header_fmt, 'single_xtal_size', units='weight', column=column)
    column = add_column(ws, header_fmt, 'method',
                        key_options=data_options['rxn_method_opts'],
                        column=column)
    column = add_column(ws, header_fmt, 'rxn_metric_unit',
                        key_options=data_options['rxn_metric_units'],
                        column=column)
    column = add_column_header(ws, header_fmt, 'rxn_metric_zero', column=column)
    column = add_column_header(ws, header_fmt, 'rxn_metric_err', column=column)
    column = add_column_header(ws, header_fmt, 'rxn_metric', column=column)
    column = add_column_header(ws, header_fmt, 'rxn_metric_resid', column=column)
    column = add_column_header(ws, header_fmt, 'results', column=column)
    column = add_column_header(ws, header_fmt, 'completion', column=column)

    ws.set_column(0, column-1, col_width)
